package com.Carthago.conformite.clients.repositories;

import com.Carthago.conformite.clients.entities.beneficiaireEffectif;
import org.springframework.data.jpa.repository.JpaRepository;

public interface beneficiareEffectifRepository extends JpaRepository<beneficiaireEffectif,Long> {
}
