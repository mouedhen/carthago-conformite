package com.Carthago.conformite.risque.services;

import com.Carthago.conformite.clients.entities.personnePhysique;
import com.Carthago.conformite.risque.entities.risque;
import com.Carthago.conformite.risque.entities.secteur;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public interface secteurService {
    ArrayList<secteur> findAllListeSecteur();
    ArrayList<secteur> findAllListeProfession();
}
