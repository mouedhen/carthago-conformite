package com.Carthago.conformite.clients.serviceImp;

import com.Carthago.conformite.clients.entities.representantLegal;

import com.Carthago.conformite.clients.entities.revenu;
import com.Carthago.conformite.clients.repositories.revenuRepository;
import com.Carthago.conformite.clients.services.revenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service("revenuServiceImpl")
public class revenuServiceImp implements revenuService {

    @Autowired
    revenuRepository RLRepository;


    /**
     * C'est l'implementation de la methode qui permet d'enregistrer cet objet
     */
    @Override
    public void saveRevenu(revenu R) {
        RLRepository.save(R);
    }


    /**
     * C'est l'implementation de la methode qui permet
     * d'enregitrer les modifications exercées sur cet objet
     */
    @Override
    public void updateRevenu
    (long codeC,revenu newRevenu) {

        revenu revenu;
        Optional<revenu> OldRevenu
            =RLRepository.findById(codeC);

        if(OldRevenu.isPresent())
        {
            revenu=OldRevenu.get();
            RLRepository.save(revenu);
        }
    }

    /**
     * C'est l'implementation de la methode qui permet de lister tous ces objets
     */
    @Override
    public List<revenu> listRevenu()
    {
        return RLRepository.findAll();
    }




    /**
     * C'est l'implementation de la methode qui permet de supprimer cet objet
     */
    @Override
    public  void removeRevenu(long code){
        RLRepository.deleteById(code);
    }



    /**
     * C'est l'implementation de la methode qui permet de trouver cet objet par son id
     */
    @Override
    public Optional<revenu> findRevenu(long code){
        return RLRepository.findById(code);
    }
}

