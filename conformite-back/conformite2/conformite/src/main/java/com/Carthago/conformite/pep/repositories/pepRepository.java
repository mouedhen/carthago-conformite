package com.Carthago.conformite.pep.repositories;

import com.Carthago.conformite.pep.entities.pep;
import org.springframework.data.jpa.repository.JpaRepository;

public interface pepRepository extends JpaRepository<pep,Long> {
}
