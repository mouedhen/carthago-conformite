package com.Carthago.conformite.clients.serviceImp;

import com.Carthago.conformite.clients.entities.personneMorale;
import com.Carthago.conformite.clients.repositories.personneMoraleRepository;
import com.Carthago.conformite.clients.services.personneMoraleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service("personneMoraleServiceImpl")
public class personneMoraleServiceImp implements personneMoraleService {

    @Autowired
    personneMoraleRepository PMRepository;


    /**
     * C'est l'implementation de la methode qui permet d'enregistrer cet objet
     */
    @Override
    public void savePersonneMorale(personneMorale PM) {
        PMRepository.save(PM);
    }


    /**
     * C'est l'implementation de la methode qui permet
     * d'enregitrer les modifications exercées sur cet objet
     */
    @Override
    public void updatePersonneMorale(long codeC,personneMorale PM) {


           PMRepository.save(PM);

    }


    /**
     * C'est l'implementation de la methode qui permet de lister tous ces objets
     */
    @Override
    public List<personneMorale> listPersonneMorale(){
        return PMRepository.findAll();
    }


    /**
     * C'est l'implementation de la methode qui permet de supprimer cet objet
     */
    @Override
    public  void removePersonneMorale(long CodeC){
        PMRepository.deleteById(CodeC);
    }

    /**
     * C'est l'implementation de la methode qui permet de trouver cet objet par son id
     */
    @Override
    public Optional<personneMorale> findPersonneMorale(long CodeC){
        return PMRepository.findById(CodeC);
    }
}

