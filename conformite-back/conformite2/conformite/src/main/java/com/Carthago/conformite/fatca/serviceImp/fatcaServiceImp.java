package com.Carthago.conformite.fatca.serviceImp;


import com.Carthago.conformite.fatca.entities.fatca;
import com.Carthago.conformite.fatca.repositories.fatcaRepository;
import com.Carthago.conformite.fatca.services.fatcaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service("fatcaServiceImpl")
public class fatcaServiceImp implements fatcaService {

    @Autowired
    fatcaRepository FRepository;

    /**
     * enregistrer fatca
     * @param F
     */
    @Override
    public void saveFatca(fatca F) {
        FRepository.save(F);
    }

    /**
     * enregistrer les modificiations
     * @param code
     * @param new_F
     */
    @Override
    public void updateFatca(long code,fatca new_F) {
        fatca F;
        Optional<fatca> OldF= FRepository.findById(code);

        if(OldF.isPresent())
        {
            F=OldF.get();
               /* par.setLibelleC(new_Par.getLibelleC());
                par.setSalle(new_Par.getSalle());
                par.setEnseignant(new_Par.getEnseignant());*/
            FRepository.save(F);
        }}


    /**
     * lister fatca
     * @return
     */
    @Override
    public List<fatca> listFatca(){
        return FRepository.findAll();
    }

    /**
     * supprimer fatca
     * @param Code
     */
    @Override
    public void removeFatca(long Code){
        FRepository.deleteById(Code);
    }


    /**
     * trouver fatca par son id
     * @param Code
     * @return
     */
    @Override
    public Optional<fatca> findFatca(long Code){
        return FRepository.findById(Code);

    }

}
