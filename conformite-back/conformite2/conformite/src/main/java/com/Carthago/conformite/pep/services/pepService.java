package com.Carthago.conformite.pep.services;

import com.Carthago.conformite.pep.entities.pep;

import java.util.List;
import java.util.Optional;

public interface pepService {
    /**
     * Cette Methode permet d'enregistrer pep
     */
    void savePep(pep pep);

    /**
     * Cette Methode permet d'enregistrer les modifications de pep.
     */
    void updatePep(long code, pep pep);

    /**
     * Cette Methode permet de lister les peps.
     */
    List<pep> listPep();

    /**
     * Cette Methode permet de supprimer pep.
     */
    void removePep(long Code);

    /**
     * Cette Methode permet de trouver pep par son id .
     */
    Optional<pep> findPep(long Code);

}
