package com.Carthago.conformite.pep.serviceImp;

import com.Carthago.conformite.pep.entities.pep;
import com.Carthago.conformite.pep.repositories.pepRepository;
import com.Carthago.conformite.pep.services.pepService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service("pepServiceImpl")
public class pepServiceImp implements pepService {

    @Autowired
    pepRepository PRepository;

    /**
     * c'est l'implementation de la fonction qui permet d'enregistrer pep
     */
    @Override
    public void savePep (pep P) {
        PRepository.save(P);
    }

    /**
     * c'est l'implementation de la fonction
     * qui permet d'enregistrer les modifications du pep
     */
    @Override
    public void updatePep(long code,pep new_P) {
        pep p;
        Optional<pep> OldP= PRepository.findById(code);


        if(OldP.isPresent())
        {
            p=OldP.get();
            PRepository.save(p);
        }}

    /**
     * c'est l'implementation de la fonction qui permet de lister les peps
     */
    @Override
    public List<pep> listPep(){
        return PRepository.findAll();
    }


    /**
     * c'est l'implementation de la fonction qui permet de supprimer  pep
     */
    @Override
    public void removePep(long code){
        PRepository.deleteById(code);
    }

    /**
     * c'est l'implementation de la fonction qui permet de trouver
     * pep  avec son id
     */
    @Override
    public Optional<pep> findPep(long code){
        return PRepository.findById(code);
    }


}

