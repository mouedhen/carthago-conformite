package com.Carthago.conformite.clients.controllers;

import com.Carthago.conformite.clients.entities.persNat;
import com.Carthago.conformite.clients.services.persNatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value="/persNat",method= {RequestMethod.GET})


public class persNatController {

    @Autowired
    persNatService PService;


    /**
     * Cette methode permet la creation de persNat
     */
    @PostMapping("/Create")
    public String createPersNat
    (@Validated @RequestBody persNat P)
    {
        PService.savePersNat(P);
        return "PersNat cree";
    }


    /**
     * Cette methode de lister les persNats
     */
    @GetMapping("/GetAll")
    public List<persNat> getAllPersNat()
    {
        return PService.listPersNat();

    }

    /**
     * Cette methode permet de trouver PersNatpar son id
     */
    @GetMapping("/Get/{id}")
    public Optional<persNat> findPersNat
    (@PathVariable Long id)
    {

        return PService.findPersNat(id);
    }

    /**
     * Cette methode permet de modifier PersNat
     */
    @PutMapping("/Update/{id}")
    public String UpdatePersBe
    (@Validated @RequestBody persNat P,@PathVariable Long id)
    {
        PService.updatePersNat(id,P);
        return "La mise a jour a ete faite avec succees";
    }


    /**
     * Cette methode permet la suppression PersNat par son id
     */
    @DeleteMapping(value="/Delete/{id}")
    public String deletePersNat(@PathVariable String id)
    {
        PService.removePersNat (Long.parseLong(id));
        return "PersNat supprimee avec succee";
    }




}
