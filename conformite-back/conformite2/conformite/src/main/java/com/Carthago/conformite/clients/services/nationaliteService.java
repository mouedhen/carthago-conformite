package com.Carthago.conformite.clients.services;

import com.Carthago.conformite.clients.entities.nationalite;

import java.util.List;
import java.util.Optional;

public interface nationaliteService {
    /**
     * Cette Methode permet d'enregistrer une Nationalite.
     */
    void saveNationalite(nationalite N);


    /**
     * Cette Methode permet d'enregistrer les modifications exercées sur une Nationalite.
     */
    void updateNationalite(long codeNationalite, nationalite N);


    /**
     * Cette Methode permet de lister toutes les Nationalites.
     */
    List<nationalite> listNationalite();


    /**
     * Cette Methode permet de supprimer une Nationalite.
     */
    void removeNationalite(long codeNationalite);


    /**
     * Cette Methode permet de trouver une Nationalite par son CodeNationalite.
     */
    public Optional<nationalite> findNationalite(long codeNationalite);



}
