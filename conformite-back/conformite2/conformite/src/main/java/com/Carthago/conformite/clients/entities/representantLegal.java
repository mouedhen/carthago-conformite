package com.Carthago.conformite.clients.entities;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
public class representantLegal extends contact {

    private String fonctionOrganisme="" ;//indique si le representant est administrateur ou mandataire

    //getter et setter


    public String getFonctionOrganisme() {
        return fonctionOrganisme;
    }

    public void setFonctionOrganisme(String fonctionOrganisme) {
        this.fonctionOrganisme = fonctionOrganisme;
    }

    public List<personneMorale> getPersonneMoraleList() {
        return personneMoraleList;
    }

    public void setPersonneMoraleList
        (List<personneMorale> personneMoraleList) {
        this.personneMoraleList = personneMoraleList;
    }

    //constructeur
    public representantLegal(Long codeClient) {
        super(codeClient);
    }

    //relation Many to Many avec personne Morale
    @ManyToMany
    @JoinTable( name = "RepPers",
        joinColumns = @JoinColumn( name = "codeRep" ),
        inverseJoinColumns = @JoinColumn( name = "codeClient" ) )
    private List<personneMorale> personneMoraleList = new ArrayList<>();
}
