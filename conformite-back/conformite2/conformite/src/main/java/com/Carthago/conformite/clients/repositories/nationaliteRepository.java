package com.Carthago.conformite.clients.repositories;

import com.Carthago.conformite.clients.entities.nationalite;
import org.springframework.data.jpa.repository.JpaRepository;

public interface nationaliteRepository extends JpaRepository<nationalite,Long> {

    nationalite findByNationalite (String N) ;
}
