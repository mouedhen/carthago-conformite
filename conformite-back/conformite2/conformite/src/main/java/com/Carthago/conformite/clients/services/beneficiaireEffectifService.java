package com.Carthago.conformite.clients.services;

import com.Carthago.conformite.clients.entities.beneficiaireEffectif;

import java.util.List;
import java.util.Optional;

public interface beneficiaireEffectifService {

    /**
     * Cette Methode permet d'enregistrer le beneficiaire effectif.
     */
    void saveBeneficiaireEffectif(beneficiaireEffectif BF);


    /**
     * Cette Methode permet d'enregistrer les modifications exercées sur le  beneficiaire effectif.
     */
    void updateBeneficiaireEffectif(long codeClient,beneficiaireEffectif BF);


    /**
     * Cette Methode permet de lister tous les beneficiaires effectifs.
     */
    List<beneficiaireEffectif> listBeneficiaireEffectif();


    /**
     * Cette Methode permet de supprimer un beneficiaire effectif.
     */
    void removeBeneficiaireEffectif(long codeClient);


    /**
     * Cette Methode permet de trouver un beneficiaire effectif par son CodeClient.
     */
    public Optional<beneficiaireEffectif> findBeneficiaireEffectif(long codeClient);



}
