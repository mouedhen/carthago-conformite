package com.Carthago.conformite.clients.entities;

import com.Carthago.conformite.fatca.entities.fatca;
import com.Carthago.conformite.pep.entities.pep;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import java.sql.Date;

@AllArgsConstructor
@NoArgsConstructor
@Entity
public class contact {

    //declaration des variables
    private static final long serialVersionUID=1L;
    @Id
    private  Long codeClient; //Code du client
    private  String paysResidence=""; //Pays residence du client
    private  String email=""; //Email du client
    private  String numTelephone=""; //Numero du telephone du client
    private  boolean codeTin=false ; //true si le client a un code TIN
    private String Adresse=""; //adresse du client
    private  String revue=""; // periodicite de revue
    private Date derniereDate ; //date de la derniere mise à jour
    private String niveauRisque ; //niveau du risque de chaque client



    public Long getCodeClient() {
        return codeClient;
    }

    public void setCodeClient(Long codeClient) {
        this.codeClient = codeClient;
    }

    public String getPaysResidence() {
        return paysResidence;
    }

    public void setPaysResidence(String paysResidence) {
        this.paysResidence = paysResidence;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNumTelephone() {
        return numTelephone;
    }

    public void setNumTelephone(String numTelephone) {
        this.numTelephone = numTelephone;
    }

    public boolean isCodeTin() {
        return codeTin;
    }

    public void setCodeTin(boolean codeTin) {
        this.codeTin = codeTin;
    }

    public pep getPep() {
        return pep;
    }

    public void setPep(pep pep) {
        this.pep = pep;
    }

    public String getAdresse() {
        return Adresse;
    }

    public void setAdresse(String adresse) {
        Adresse = adresse;
    }

    public fatca getFatca() {
        return fatca;
    }

    public void setFatca(fatca fatca) {
        this.fatca = fatca;
    }

    public String getRevue() {
        return revue;
    }

    public void setRevue(String revue) {
        this.revue = revue;
    }

    public Date getDerniereDate() {
        return derniereDate;
    }

    public void setDerniereDate(Date derniereDate) {
        this.derniereDate = derniereDate;
    }

    public String getNiveauRisque() {
        return niveauRisque;
    }

    public void setNiveauRisque(String niveauRisque) {
        this.niveauRisque = niveauRisque;
    }

    //constructeur
    public contact(Long codeClient) {
        this.codeClient = codeClient;
    }

    //Relations
    @OneToOne(mappedBy ="contact",cascade = CascadeType.ALL)
    @JsonManagedReference
    private pep pep ;
    //relation client et fatca
    @OneToOne(mappedBy ="contact1",cascade = CascadeType.ALL)
    private fatca fatca;
}
