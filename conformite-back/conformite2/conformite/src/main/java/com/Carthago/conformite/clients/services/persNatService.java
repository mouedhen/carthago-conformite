package com.Carthago.conformite.clients.services;

import com.Carthago.conformite.clients.entities.persNat;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public interface persNatService {

    /**
     * Cette Methode permet d'enregistrer PersNat.
     */
    void savePersNat(persNat PN);


    /**
     * Cette Methode permet d'enregistrer les modifications exercées sur persNat.
     */
    void updatePersNat(long codePersBe, persNat PN);


    /**
     * Cette Methode permet de lister tous les persNats.
     */
    List<persNat> listPersNat();


    /**
     * Cette Methode permet de supprimer persNat.
     */
    void removePersNat(long codePersNat);


    /**
     * Cette Methode permet de trouver persNat par son CodePersNat.
     */
    public Optional<persNat> findPersNat(long codePersNat);

    /**
     * cette methode permet de lister les codes
     */
    ArrayList<Long> listPNCode(long id);
}
