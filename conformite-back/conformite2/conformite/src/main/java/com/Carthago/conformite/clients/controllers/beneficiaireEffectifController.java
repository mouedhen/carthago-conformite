package com.Carthago.conformite.clients.controllers;

import com.Carthago.conformite.clients.entities.beneficiaireEffectif;
import com.Carthago.conformite.clients.services.beneficiaireEffectifService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value="/beneficiaireEffectif",method= {RequestMethod.GET})

public class beneficiaireEffectifController  {
    @Autowired
    beneficiaireEffectifService BEService;


    /**
     * Cette methode permet la creation du beneficiaire effectif
     */
    @PostMapping("/Create")
    public String createBeneficiaireEffectif
    (@Validated @RequestBody beneficiaireEffectif BE)
    {
        BEService.saveBeneficiaireEffectif(BE);
        return "BeneficiaireEffectif cree";
    }


    /**
     * Cette methode de lister les beneficiaires effectifs
     */
    @GetMapping("/GetAll")
    public List<beneficiaireEffectif> getAllBeneficiaireEffectif()
    {
        return BEService.listBeneficiaireEffectif();

    }

    /**
     * Cette methode permet de trouver un beneficiaire effectif par son id
     */
    @GetMapping("/Get/{id}")
    public Optional<beneficiaireEffectif> findBeneficiaireEffectif
    (@PathVariable Long id)
    {

        return BEService.findBeneficiaireEffectif(id);
    }

    /**
     * Cette methode permet de modifier un beneficiaire effectif
     */
    @PutMapping("/Update/{id}")
    public String UpdateBeneficiaireEffectif
    (@Validated @RequestBody beneficiaireEffectif BF, @PathVariable Long id)
    {
        BEService.updateBeneficiaireEffectif(id,BF);
        return"La mise a jour a ete faite avec succees";
    }


    /**
     * Cette methode permet la suppression d'un beneficiaire effectif par son id
     */
    @DeleteMapping(value="/Delete/{id}")
    public String deleteBeneficiaireEffectif(@PathVariable String id)
    {
        BEService.removeBeneficiaireEffectif(Long.parseLong(id));
        return "BeneficiaireEffectif supprimee avec succee";
    }
}


