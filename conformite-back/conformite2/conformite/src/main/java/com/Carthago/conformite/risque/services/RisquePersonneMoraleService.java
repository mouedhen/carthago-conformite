package com.Carthago.conformite.risque.services;

import com.Carthago.conformite.clients.entities.personneMorale;
import com.Carthago.conformite.risque.entities.risque;
import com.Carthago.conformite.risque.entities.risquePersonneMorale;
import org.springframework.web.bind.annotation.PathVariable;

public interface RisquePersonneMoraleService {

    int critereGeo(String pays);
    int risqueGeographique(long id,personneMorale P);
    void saveRisque(risquePersonneMorale R);
    int risqueNature (personneMorale p);
     int risqueSecteur(personneMorale p);
}
