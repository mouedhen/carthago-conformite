package com.Carthago.conformite.clients.serviceImp;

import com.Carthago.conformite.clients.entities.personnePhysique;
import com.Carthago.conformite.clients.repositories.personnePhysiqueRepository;
import com.Carthago.conformite.clients.services.personnePhysiqueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service("personnePhysiqueServiceImpl")

public class personnePhysiqueServiceImp implements personnePhysiqueService {

    @Autowired
    personnePhysiqueRepository PPRepository;


    /**
     * C'est l'implementation de la methode qui permet d'enregistrer cet objet
     */
    @Override
    public void savePersonnePhysique(personnePhysique PP) {
        PPRepository.save(PP);
    }


    /**
     * C'est l'implementation de la methode qui permet
     * d'enregitrer les modifications exercées sur cet objet
     */
    @Override
    public void updatePersonnePhysique
    (personnePhysique personnePhysique) {

        PPRepository.save(personnePhysique);
    }

    /**
     * C'est l'implementation de la methode qui permet de lister tous ces objets
     */
    @Override
    public List<personnePhysique> listPersonnePhysique(){
        return PPRepository.findAll();
    }

    /**
     * C'est l'implementation de la methode qui permet de supprimer cet objet
     */
    @Override
    public  void removePersonnePhysique(long CodeC){
        PPRepository.deleteById(CodeC);
    }

    /**
     * C'est l'implementation de la methode qui permet de trouver cet objet par son id
     */
    @Override
    public Optional<personnePhysique> findPersonnePhysique(long CodeC){
        return PPRepository.findById(CodeC);
    }
}

