package com.Carthago.conformite.risque.serviceImp;

import com.Carthago.conformite.clients.entities.personnePhysique;
import com.Carthago.conformite.clients.services.persNatService;
import com.Carthago.conformite.pep.entities.pep;
import com.Carthago.conformite.pep.services.pepService;
import com.Carthago.conformite.risque.entities.listeCorruption;
import com.Carthago.conformite.risque.entities.pays;
import com.Carthago.conformite.risque.entities.risque;
import com.Carthago.conformite.risque.entities.secteur;
import com.Carthago.conformite.risque.repositories.risqueRepository;
import com.Carthago.conformite.risque.services.paysService;
import com.Carthago.conformite.risque.services.risqueService;
import com.Carthago.conformite.risque.services.secteurService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service("risqueServiceImpl")
public class risqueFatcaServiceImp implements risqueService {

    @Autowired
    risqueRepository ParRepository;

    @Autowired
    paysService na;

    @Autowired
    secteurService s;

    @Autowired
    pepService pe;

    @Autowired
    persNatService pns;

    @Override
    public void saveRisque(risque Par) {
        ParRepository.save(Par);
    }
    @Override
    public void updateRisque(risque new_Par) {
        ParRepository.save(new_Par);
    }
    @Override
    public List<risque> listRisque(){
        return ParRepository.findAll();
    }
    @Override
    public void removeRisque(long CodeC){
        ParRepository.deleteById(CodeC);
    }
    @Override
    public Optional<risque> findRisque(long CodeC){
        return ParRepository.findById(CodeC);

    }

    //fonctionne qui calcule le risque pour une personne physique :
    @Override
    public int risqueGeographique(personnePhysique p){
        int r=0;
        int res=0;
        //trouver le risque des nationalites :
        ArrayList<Long> liste_nat=pns.listPNCode(p.getCodeClient());
        res=critereGeoNat(liste_nat);


        //trouver le risque des pays PEP
        r=r+ critereGeo(p.getPaysNaissance());
        r=r+ critereGeo(p.getPaysTravail());
        r=r+ critereGeo(p.getPaysResidence());
        if(p.getStatutPep()==true)
        {
            Optional<pep> t=pe.findPep(p.getCodeClient());
            r=r+critereGeo(t.get().getPaysPep());
        }
        r=r+res;

        return r;
    }


    //fonctionne qui calcule le risque d'un critere geographique
    @Override
    public  int critereGeo(String pays)
    {
        int note_p=0;
        Boolean t=false;
        int i = 0;

        List<pays> l=na.findAllListeGrise();
        List<pays> l1=na.findAllListeNoire();
        List<pays> l2=na.findAllListePaysGeurre();
        List<listeCorruption> l3=na.findAllListeCorruption();
        List<pays> l4=na.findAllListeEconomieInformelle();
        List<pays> l5=na.findAllListePaysProducteursDrogue();

        while ( i < l.size() && t==false) {
            if(l.get(i).getPays().equalsIgnoreCase(pays))
            {
                t=true;
                note_p=note_p+4;
            }
            else{
                i++;
            }
        }
        i=0;
        t=false;
        while ( i < l1.size() && t==false) {
            if(l1.get(i).getPays().equalsIgnoreCase(pays))
            {
                t=true;
                note_p=note_p+5;
            }
            else{
                i++;
            }
        }
        t=false;
        i=0;
        while ( i < l2.size() && t==false) {
            if(l2.get(i).getPays().equalsIgnoreCase(pays))
            {
                t=true;
                note_p=note_p+4;
            }
            else{
                i++;
            }
        }
        t=false;
        i=0;
        while ( i < l3.size() && t==false) {
            if((l3.get(i).getPays().equalsIgnoreCase(pays))&&(l3.get(i).getRang()>70))
            {
                t=true;
                note_p=note_p+4;
            }
            else{
                i++;
            }
        }
        t=false;
        i=0;
        while ( i < l4.size() && t==false) {
            if(l4.get(i).getPays().equalsIgnoreCase(pays))
            {
                t=true;
                note_p=note_p+4;
            }
            else{
                i++;
            }
        }
        t=false;
        i=0;
        while ( i < l5.size() && t==false) {
            if(l5.get(i).getPays().equalsIgnoreCase(pays))
            {
                t=true;
                note_p=note_p+3;
            }
            else{
                i++;
            }
        }
        return note_p;
    }

    //fonctionne qui calcule le risque d'un critere geographique pour les nationnalites
    @Override
    public  int critereGeoNat(ArrayList<Long> liste_nat)
    {int note_p=0;
        Boolean t=false;
        int i = 0;

        List<pays> l=na.findAllListeGrise();
        List<pays> l1=na.findAllListeNoire();
        List<pays> l2=na.findAllListePaysGeurre();
        List<listeCorruption> l3=na.findAllListeCorruption();
        List<pays> l4=na.findAllListeEconomieInformelle();
        List<pays> l5=na.findAllListePaysProducteursDrogue();
        for (int j=0;j<liste_nat.size();j++) {

            while (i < l.size() && t == false) {
                if (l.get(i).getCodePays() == liste_nat.get(j)) {
                    t = true;
                    note_p = note_p + 4;
                } else {
                    i++;
                }
            }
            i=0;
            t=false;
            while ( i < l1.size() && t==false) {
                if(l1.get(i).getCodePays() == liste_nat.get(j))
                {
                    t=true;
                    note_p=note_p+5;
                }
                else{
                    i++;
                }
            }
            t=false;
            i=0;
            while ( i < l2.size() && t==false) {
                if(l2.get(i).getCodePays() == liste_nat.get(j))
                {
                    t=true;
                    note_p=note_p+4;
                }
                else{
                    i++;
                }
            }
            t=false;
            i=0;
            while ( i < l3.size() && t==false) {
                if((l3.get(i).getCodePays() == liste_nat.get(j))&&(l3.get(i).getRang()>70))
                {
                    t=true;
                    note_p=note_p+4;
                }
                else{
                    i++;
                }
            }

            t=false;
            i=0;
            while ( i < l4.size() && t==false) {
                if(l4.get(i).getCodePays() == liste_nat.get(j))
                {
                    t=true;
                    note_p=note_p+4;
                }
                else{
                    i++;
                } }

            t=false;
            i=0;
            while ( i < l5.size() && t==false) {
                if(l5.get(i).getCodePays() == liste_nat.get(j))
                {
                    t=true;
                    note_p=note_p+3;
                }
                else{
                    i++;
                } } }
        return note_p;
    }

    //fonctionne qui calcule le risque  du secteurpour une personne physique :
    /*@Override
    public int risqueSecteur(personnePhysique p){

        int note_p=0;
        Boolean t=false;
        int i = 0;
        List<secteur> l=s.findAllListeSecteur();
        while ( i < l.size() && t==false) {
            if(l.get(i).getNom().equalsIgnoreCase(p.getSecteurTravail()))
            {
                if(l.get(i))
                {t=true;
                    note_p=note_p+4;}




            }
            else{
                i++;
            }
        }
        return note_p;
    }*/

}

