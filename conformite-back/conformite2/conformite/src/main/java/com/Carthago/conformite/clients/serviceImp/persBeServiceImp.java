package com.Carthago.conformite.clients.serviceImp;


import com.Carthago.conformite.clients.entities.RepPers;
import com.Carthago.conformite.clients.entities.persBe;
import com.Carthago.conformite.clients.repositories.persBeRepository;
import com.Carthago.conformite.clients.services.persBeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service("PersBeServiceImpl")
public class persBeServiceImp  implements persBeService {

    @Autowired
    persBeRepository repository;


    /**
     * Retourne une liste contenant tous les codes de rep d'une personne Morale
     * @param id
     * @return
     */
    @Override
    public ArrayList<Long> listPersBeCode(long id) {
        List<persBe> l=repository.findAll();
        Long l1;

        ArrayList<Long>liste=new ArrayList<Long>();
        for(int i=0;i<l.size();i++)

        {
            if(l.get(i).getCodeClient()==id)
            {

                l1=(l.get(i).getCodeBe());
                liste.add(l1);

            }
        }
        return liste;
    }

    @Override
    public List<persBe> findAll ()
    {
        return repository.findAll();
    }
}
