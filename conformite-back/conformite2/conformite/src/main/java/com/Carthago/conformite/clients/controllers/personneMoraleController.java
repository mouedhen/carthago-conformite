package com.Carthago.conformite.clients.controllers;

import com.Carthago.conformite.clients.entities.RepPers;
import com.Carthago.conformite.clients.entities.personneMorale;
import com.Carthago.conformite.clients.entities.personnePhysique;
import com.Carthago.conformite.clients.services.RepPersService;
import com.Carthago.conformite.clients.services.personneMoraleService;
import com.Carthago.conformite.fatca.entities.fatca;
import com.Carthago.conformite.fatca.services.fatcaService;
import com.Carthago.conformite.risque.entities.risque;
import com.Carthago.conformite.risque.entities.risquePersonneMorale;
import com.Carthago.conformite.risque.services.RisquePersonneMoraleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value="/personneMorale",method= {RequestMethod.GET})

public class personneMoraleController {

    @Autowired
    personneMoraleService PService;

    @Autowired
    fatcaService fService;


    @Autowired
    RisquePersonneMoraleService RisqueService;


    /**
     * Cette methode permet la creation de personneMorale
     */
    @PostMapping("/Create")
    public String createPersonneMorale
    (@Validated @RequestBody personneMorale P)
    {

        PService.savePersonneMorale(P);
        return "PersonneMorale cree";
    }


    /**
     * Cette methode de lister les personneMorale
     */
    @GetMapping("/GetAll")
    public List<personneMorale> getAllPersonneMorale()
    {
        List<personneMorale> l=PService.listPersonneMorale();
        List<personneMorale> p = new ArrayList<personneMorale>();
        personneMorale pers;
        for(int i=0;i<l.size();i++)
        {
            pers=new personneMorale();
            pers.setCodeClient(l.get(i).getCodeClient());
            pers.setStatutFatca(l.get(i).getStatutFatca());
            pers.setRevue((l.get(i).getRevue()));
            pers.setNiveauRisque((l.get(i).getNiveauRisque()));
            pers.setFormeJuridique((l.get(i).getFormeJuridique()));
            pers.setNature((l.get(i).getNature()));
            p.add(pers);
        }
        return p;

    }

    /**
     * Cette methode permet de trouver PersonneMoralepar son id
     */
    @GetMapping("/Get/{id}")
    public Optional<personneMorale>findPersonneMorale
    (@PathVariable Long id)
    { Optional<personneMorale> p= PService.findPersonneMorale(id);
        Optional<personneMorale> pers= Optional.of(new personneMorale());
        pers.get().setCodeClient(p.get().getCodeClient());
        pers.get().setStatutFatca(p.get().getStatutFatca());
        pers.get().setRevue((p.get().getRevue()));
        pers.get().setNiveauRisque((p.get().getNiveauRisque()));
        pers.get().setFormeJuridique((p.get().getFormeJuridique()));
        pers.get().setNature((p.get().getNature()));


        return pers;

    }

    /**
     * Cette methode permet de modifier PersonneMorale
     */
    @PutMapping("/Update/{id}")
    public int UpdatePersonneMorale
    (@PathVariable long id,@Validated @RequestBody  personneMorale P)
    {
        //set matricule fiscale
        P.setMatriculeFiscale(id);
        //detection statut fatca
        fatca f = new fatca();
        f.setCodeFatca(id);
        if((P.getGiin()==true )
            ||(P.getUs()==true)
            || (P.isCodeTin()==true)
            ||(P.getPaysConstruction().equalsIgnoreCase("Etat_Unis"))
            ||(P.getPaysResidence().equalsIgnoreCase("Etat_Unis"))||(P.getActionnaireUs()==true))
        {
            P.setStatutFatca(true);
            f.setStatut(true);
            fService.saveFatca(f);

        }

        else {
            P.setStatutFatca(false);
            fService.saveFatca(f);

        }

        //Calcul du risque
        int n=RisqueService.risqueGeographique(id,P);
        n=n+RisqueService.risqueNature(P);
        n=n+RisqueService.risqueSecteur(P);
        risquePersonneMorale r=new risquePersonneMorale();
        r.setCodeRisque(id);
        if(n>10)
        {
            r.setStatut("E");
            P.setNiveauRisque("Eleve");
            P.setRevue("Chaque trois mois");}
        else{
            r.setStatut("F");
            P.setNiveauRisque("Faible");
            P.setRevue("Chaque annee");
        }
        r.setValeur(n);
        RisqueService.saveRisque(r);
        PService.updatePersonneMorale(id,P);
        return n;
    }


    /**
     * Cette methode permet la suppression PersonneMorale par son id
     */
    @DeleteMapping(value="/Delete/{id}")
    public String deletePersonneMorale(@PathVariable String id)
    {
        PService.removePersonneMorale (Long.parseLong(id));
        return "PersNat supprimee avec succee";
    }






}

