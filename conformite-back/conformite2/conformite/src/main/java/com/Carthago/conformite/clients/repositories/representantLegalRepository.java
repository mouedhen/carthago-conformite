package com.Carthago.conformite.clients.repositories;

import com.Carthago.conformite.clients.entities.representantLegal;
import org.springframework.data.jpa.repository.JpaRepository;

public interface representantLegalRepository extends JpaRepository<representantLegal,Long> {
}
