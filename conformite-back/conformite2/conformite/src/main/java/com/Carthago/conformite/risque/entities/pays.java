package com.Carthago.conformite.risque.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
public class pays {
    //declaration des variables
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private  Long codePays;
    private String pays;




    //GETTERS AND SETTERS
    public Long getCodePays() {
        return codePays;
    }

    public void setCodePays(Long codePays) {
        this.codePays = codePays;
    }

    public String getPays() {
        return pays;
    }

    public void setPays(String pays) {
        this.pays = pays;
    }
}
