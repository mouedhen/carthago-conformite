package com.Carthago.conformite.clients.repositories;

import com.Carthago.conformite.clients.entities.RepPers;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

public interface RepPersRepository  extends JpaRepository<RepPers,Long> {

   List<RepPers> findAllByCodeClient (Long codeClient);

   RepPers findByCodeClient(@PathVariable Long codeClient);
}
