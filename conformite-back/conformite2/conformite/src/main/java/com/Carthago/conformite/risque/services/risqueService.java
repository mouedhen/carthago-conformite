package com.Carthago.conformite.risque.services;

import com.Carthago.conformite.clients.entities.personnePhysique;
import com.Carthago.conformite.risque.entities.risque;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public interface risqueService {
    void saveRisque(risque Nat);
    void updateRisque(risque Nat);
    List<risque> listRisque();
    void removeRisque(long CodeC);
    Optional<risque> findRisque(long CodeC);
    int critereGeo(String pays);
    int risqueGeographique(personnePhysique p);
    int critereGeoNat(ArrayList<Long> liste_nat);
    //int risqueSecteur(personnePhysique p);
}
