package com.Carthago.conformite.fatca.services;

import com.Carthago.conformite.fatca.entities.fatca;

import java.util.List;
import java.util.Optional;

public interface fatcaService {

    /**
     * Permet d'enregitrer le statut Fatca
     * @param F
     */
    void saveFatca(fatca F);

    /**
     * Permet d'enregistrer les modification
     * @param code
     * @param F
     */
    void updateFatca(long code, fatca F);

    /**
     * permet des lister les fatca
     * @return
     */
    List<fatca> listFatca();

    /**
     * permer de supprimer Fatca
     * @param Code
     */
    void removeFatca(long Code);

    /**
     * permet de trouver fatca par son id
     * @param Code
     * @return
     */
    Optional<fatca> findFatca(long Code);

}
