package com.Carthago.conformite.clients.controllers;

import com.Carthago.conformite.clients.entities.personnePhysique;
import com.Carthago.conformite.clients.services.personnePhysiqueService;
import com.Carthago.conformite.fatca.entities.fatca;
import com.Carthago.conformite.fatca.services.fatcaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value="/personnePhysique",method= {RequestMethod.GET})

public class personnePhysiqueController {

    @Autowired
    personnePhysiqueService PService;

    @Autowired
    fatcaService fService;


    /**
     * Cette methode permet la creation de personne Physique
     */
    @PostMapping("/Create")
    public String createPersonnePhysique
    (@Validated @RequestBody personnePhysique P)
    {
        PService.savePersonnePhysique(P);
        return "Personne Physique cree";
    }


    /**
     * Cette methode de lister les personne physique
     */
    @GetMapping("/GetAll")
    public List<personnePhysique> getAllPersonnePhysique()
    {
        List<personnePhysique> l=PService.listPersonnePhysique();
        List<personnePhysique> p = new ArrayList<personnePhysique>();
        personnePhysique pers;
        for(int i=0;i<l.size();i++)
        {
            pers=new personnePhysique();
            pers.setCodeClient(l.get(i).getCodeClient());
            pers.setNom(l.get(i).getNom());
            pers.setPrenom((l.get(i).getPrenom()));
            pers.setStatutFatca((l.get(i).getStatutFatca()));
            p.add(pers);
        }
        return p;
    }

    /**
     * Cette methode permet de trouver Personne physique par son id
     */
    @GetMapping("/Get/{id}")
    public Optional<personnePhysique>findPersonnePhysique
    (@PathVariable Long id)
    { Optional<personnePhysique> p= PService.findPersonnePhysique(id);
        Optional<personnePhysique> pers= Optional.of(new personnePhysique());
      pers.get().setCodeClient(p.get().getCodeClient());
      pers.get().setNom(p.get().getNom());
      pers.get().setPrenom(p.get().getPrenom());
      return pers;

    }

    /**
     * Cette methode permet de modifier Personne Physique
     */
    @PutMapping("/Update/{id}")
    public String UpdatePersonnePhysique
    (@Validated @RequestBody personnePhysique PP,@PathVariable Long id)
    {   PP.setCodeClient(id);

    //detection statut fatca
      fatca f = new fatca();
      f.setCodeFatca(id);
      if((PP.getUsResident()==true )
            ||(PP.getCarteVerte()==true)
            || (PP.isCodeTin()==true)
          ||PP.getPaysNaissance().equalsIgnoreCase("Etat_Unis"))
        { PP.setStatutFatca(true);
        f.setStatut(true);
        }

        else {
            PP.setStatutFatca(false);
            fService.saveFatca(f);
        }

        PService.updatePersonnePhysique(PP);
        return "La mise a jour a ete faite avec succees";
    }


    /**
     * Cette methode permet la suppression Personne Physique par son id
     */
    @DeleteMapping(value="/Delete/{id}")
    public String deletePersonnePhysique(@PathVariable String id)
    {
        PService.removePersonnePhysique (Long.parseLong(id));
        return "personne Physique supprimee avec succee";
    }
}
