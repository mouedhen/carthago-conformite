package com.Carthago.conformite.risque.serviceImp;

import com.Carthago.conformite.clients.serviceImp.nationaliteServiceImp;
import com.Carthago.conformite.risque.entities.secteur;
import com.Carthago.conformite.risque.services.secteurService;
import com.opencsv.CSVReader;
import org.springframework.stereotype.Service;

import java.io.*;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

@Service("secteurServiceImpl")
public class secteurServiceImp implements secteurService {
    private final ArrayList<secteur> countries1;

    public secteurServiceImp(ArrayList<secteur> countries1) {
        this.countries1 = countries1;
    }

    @Override
    public   ArrayList<secteur> findAllListeSecteur(){

        FileInputStream fis1 = null;

        try {

            String fileName = "src/main/resources/secteur_profession/Secteur.csv";

            fis1 = new FileInputStream(new File(fileName));
            CSVReader reader = new CSVReader(new InputStreamReader(fis1));
            String[] nextLine;
            reader.readNext();

            while ((nextLine = reader.readNext()) != null) {

                secteur newCountry = new secteur(Long.valueOf(nextLine[0]),
                    nextLine[1]);
                countries1.add(newCountry);
            }

        } catch (FileNotFoundException ex) {
            Logger.getLogger(nationaliteServiceImp.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(nationaliteServiceImp.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (fis1 != null) {
                    fis1.close();
                }
            } catch (IOException ex) {
                Logger.getLogger(nationaliteServiceImp.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return countries1;
    }

    @Override
    public   ArrayList<secteur> findAllListeProfession(){

        FileInputStream fis1 = null;

        try {

            String fileName = "src/main/resources/secteur_profession/Profession.csv";

            fis1 = new FileInputStream(new File(fileName));
            CSVReader reader = new CSVReader(new InputStreamReader(fis1));
            String[] nextLine;
            reader.readNext();

            while ((nextLine = reader.readNext()) != null) {

                secteur newCountry = new secteur(Long.valueOf(nextLine[0]),
                    nextLine[1]);
                countries1.add(newCountry);
            }

        } catch (FileNotFoundException ex) {
            Logger.getLogger(nationaliteServiceImp.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(nationaliteServiceImp.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (fis1 != null) {
                    fis1.close();
                }
            } catch (IOException ex) {
                Logger.getLogger(nationaliteServiceImp.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return countries1;
    }
}

