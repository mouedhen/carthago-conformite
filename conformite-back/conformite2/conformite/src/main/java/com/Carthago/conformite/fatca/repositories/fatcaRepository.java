package com.Carthago.conformite.fatca.repositories;

import com.Carthago.conformite.fatca.entities.fatca;
import org.springframework.data.jpa.repository.JpaRepository;

public interface fatcaRepository extends JpaRepository<fatca,Long> {
}
