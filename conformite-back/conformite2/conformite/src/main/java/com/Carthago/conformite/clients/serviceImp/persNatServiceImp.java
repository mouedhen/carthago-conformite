package com.Carthago.conformite.clients.serviceImp;

import com.Carthago.conformite.clients.entities.persNat;
import com.Carthago.conformite.clients.repositories.persNatRepository;
import com.Carthago.conformite.clients.services.persNatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service("persNatServiceImpl")
public class persNatServiceImp implements persNatService {

    @Autowired
    persNatRepository PNRepository;


    /**
     * C'est l'implementation de la methode qui permet d'enregistrer cet objet
     */
    @Override
    public void savePersNat(persNat N) {
        PNRepository.save(N);
    }


    /**
     * C'est l'implementation de la methode qui permet
     * d'enregitrer les modifications exercées sur cet objet
     */
    @Override
    public void updatePersNat(long codeNat,persNat newPersNat) {
        persNat persNat;
        Optional<persNat> OldPersNat=PNRepository.findById(codeNat);

        if(OldPersNat.isPresent())
        {
            persNat=OldPersNat.get();
            PNRepository.save(persNat);
        }
    }

    /**
     * C'est l'implementation de la methode qui permet de lister tous ces objets
     */
    @Override
    public List<persNat> listPersNat(){
        return PNRepository.findAll();
    }

    /**
     * C'est l'implementation de la methode qui permet de supprimer cet objet
     */
    @Override
    public  void removePersNat(long codeNat){
        PNRepository.deleteById(codeNat);
    }

    /**
     * C'est l'implementation de la methode qui permet de trouver cet objet par son id
     */
    @Override
    public Optional<persNat> findPersNat(long codeNat){
        return PNRepository.findById(codeNat);
    }

    /**
     * fonction qui retourne une liste contenant les codes
     * @param id
     * @return
     */
    @Override
    public ArrayList<Long> listPNCode(long id) {
        List<persNat> l=PNRepository.findAll();
        Long l1;

        ArrayList<Long>liste=new ArrayList<Long>();
        int i=0;

        for(i=0;i<l.size();i++)

        {
            if(l.get(i).getPers1().getCodeClient()==id)
            {

                l1=(l.get(i).getNat().getCodeNationalite());
                liste.add(l1);

            }
        }
        return liste;
    }
}


