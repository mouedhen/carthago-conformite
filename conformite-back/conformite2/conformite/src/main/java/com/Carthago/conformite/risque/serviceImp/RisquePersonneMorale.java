package com.Carthago.conformite.risque.serviceImp;

import com.Carthago.conformite.clients.entities.*;
import com.Carthago.conformite.clients.services.RepPersService;
import com.Carthago.conformite.clients.services.beneficiaireEffectifService;
import com.Carthago.conformite.clients.services.persBeService;
import com.Carthago.conformite.clients.services.representantLegalService;
import com.Carthago.conformite.pep.entities.pep;
import com.Carthago.conformite.pep.services.pepService;
import com.Carthago.conformite.risque.entities.*;
import com.Carthago.conformite.risque.repositories.risquePersonneMoraleRepository;
import com.Carthago.conformite.risque.repositories.risqueRepository;
import com.Carthago.conformite.risque.services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
@Service("risquePersonneMorale")
public class RisquePersonneMorale implements RisquePersonneMoraleService {

    @Autowired
    risquePersonneMoraleRepository RR;

    @Autowired
    secteurService S;

    @Autowired
    paysService PService;

    @Autowired
    risqueService RService;

    @Autowired
    pepService pe;

    @Autowired
    RepPersService RepService;

    @Autowired
    persBeService persBeService;

    @Autowired
    risqueFatcaServiceImp RFService ;

    @Autowired
    beneficiaireEffectifService BService ;

    @Autowired
    representantLegalService RLService;

    @Autowired
    modeleRisquePersonneMoraleService mService;

    /**
     * Fonction qui enregistre le risque
     */
    @Override
    public void saveRisque(risquePersonneMorale R) {
        RR.save(R);
    }


    @Override
    /**
     * Fonction qui calcule le risque de la personne Morale selon sa nature et secteur
     */
    public int risqueNature (personneMorale p)
    { int r=0;

      modeleRisquePersonneMorale m = new modeleRisquePersonneMorale();
      m.setCode(p.getCodeClient());

        if ((p.getNature()
            .equalsIgnoreCase(
                "Achat et vente ou autres actes de " +
                    "disposition de biens immeubles ou entités"))||
        (p.getOrigineFonds()
            .equalsIgnoreCase(
                "De la vente d’un immeuble")))
        {    m.setNature("L'origine des fonds de la societe est de la vente des immeubles");
             r=r+4;
        }


        //fonds non tunisiens
        if( !p.getOrigineFondsTunisien())
        {  m.setPaysPro("Le pays de provenance des fonds n'est pas la Tunisie");
            r=r+4;
        }

        //verifie si la societe est offshore
        if (!p.getPaysResidence()
            .equalsIgnoreCase(
                "Tunisie"))

        {  m.setOffshore("Il s'agit du Offshore");
            r=r+4;
        }

        //risque s'il s'agit d'ONG
        if(p.getNature().equalsIgnoreCase("ONG"))
        {   m.setOng("Il s'agit d'une ONG");
            r=r+4;
        }


        //si l'ese est cotée sur le marche boursier
        if(p.getMarcheBoursier())
        {   m.setMarcheBoursier("Cette societe est cotée sur le marché boursier");
            r=r+4;
        }
        if(p.getParticipationPub())
        {   m.setPub("Cette societe est à participation publique");
            r=r+4;
        }
        //verifie les actionnaires
        if(p.getActionnaireOffshore())
        {  m.setAssOffshore("Société ayant un actionnariat sur plusieurs niveaux et des actionnaires\n" +
            "sociétés offshore ");
            r=r+4;
        }

        mService.save(m);
        return r/4;
    }



    /**
     * Fonction qui calcule le risque  de la personne Morale selon le critére geographique
     */
    @Override
    public int risqueGeographique( @PathVariable long id, personneMorale p){
        int r=0;
        int res=0;


        //trouver le risque geographique pour les representants Legals :
         ArrayList<Long> listeRep=RepService.listRepPersCode(id);
         r=r+RisqueRepresentant(listeRep);

         //trouver le risque geographique pour les beneficiaires effectifs :
        ArrayList<Long> listeBe=persBeService.listPersBeCode(id);
        r=r+RisqueRepresentant(listeBe);

        //trouver le risque des pays
        r=r+ critereGeo(p.getPaysResidence());
        r=r+ critereGeo(p.getPaysConstruction());
        r=r+ critereGeo(p.getPaysOrigineFonds());


        if(p.isAssociesPpe())
        {   r=r+4; //un  des associées est politiquement exposé
            Optional<pep> t=pe.findPep(p.getCodeClient());
            r=r+critereGeo(t.get().getPaysPep());
        }

        return r/6;
    }
    /**
     * Fonction qui calcule le risque des beneficiaires effectifs
     */
    public int  RisqueBeneficiaire(ArrayList<Long> ListeRep)
    { int i=0;
      int r=0;
      String pays ;
       while (i < ListeRep.size())
       {
         Optional<beneficiaireEffectif> b = Optional.of(new beneficiaireEffectif());
         b=BService.findBeneficiaireEffectif(ListeRep.get(i));
         pays = b.get().getPaysResidence();
         r=r+critereGeo(pays);
         i++;

       }
       return r/i;
    }

    /**
     * Fonction qui calcule le risque des representants Legals
     */
    public int  RisqueRepresentant(ArrayList<Long> ListeRep)
    { int i=0;
      int r=0;
      String pays ;
        while (i < ListeRep.size())
        {
            Optional<representantLegal> b = Optional.of(new representantLegal());
            b=RLService.findRepresentantLegal(ListeRep.get(i));
            pays = b.get().getPaysResidence();
            r=r+critereGeo(pays);
            i++;



        }
        return r;
    }


    /**
     * Fonction qui calcule le risque selon le critere geographique
     */

    @Override
    public  int critereGeo(String pays)
    {
        int note_p=0;
        Boolean t=false;
        int i = 0;

        List<pays> l=PService.findAllListeGrise();
        List<pays> l1=PService.findAllListeNoire();
        List<pays> l2=PService.findAllListePaysGeurre();
        List<listeCorruption> l3=PService.findAllListeCorruption();
        List<pays> l4=PService.findAllListeEconomieInformelle();
        List<pays> l5=PService.findAllListePaysProducteursDrogue();

        while ( i < l.size() && t==false) {
            if(l.get(i).getPays().equalsIgnoreCase(pays))
            {
                t=true;
                note_p=note_p+4;
            }
            else{
                i++;
            }
        }
        i=0;
        t=false;
        while ( i < l1.size() && t==false) {
            if(l1.get(i).getPays().equalsIgnoreCase(pays))
            {
                t=true;
                note_p=note_p+5;
            }
            else{
                i++;
            }
        }
        t=false;
        i=0;
        while ( i < l2.size() && t==false) {
            if(l2.get(i).getPays().equalsIgnoreCase(pays))
            {
                t=true;
                note_p=note_p+4;
            }
            else{
                i++;
            }
        }
        t=false;
        i=0;
        while ( i < l3.size() && t==false) {
            if((l3.get(i).getPays().equalsIgnoreCase(pays))&&(l3.get(i).getRang()>70))
            {
                t=true;
                note_p=note_p+4;
            }
            else{
                i++;
            }
        }
        t=false;
        i=0;
        while ( i < l4.size() && t==false) {
            if(l4.get(i).getPays().equalsIgnoreCase(pays))
            {
                t=true;
                note_p=note_p+4;
            }
            else{
                i++;
            }
        }
        t=false;
        i=0;
        while ( i < l5.size() && t==false) {
            if(l5.get(i).getPays().equalsIgnoreCase(pays))
            {
                t=true;
                note_p=note_p+3;
            }
            else{
                i++;
            }
        }
        return note_p;
    }

    /**
     * Calcule le risque Par secteur
     * @param p
     * @return
     */
    @Override
    public int risqueSecteur(personneMorale p){

        int note_p=0;
        Boolean t=false;
        int i = 0;
        List<secteur> l=S.findAllListeSecteur();
        while ( i < l.size() && t==false) {
            String ch=l.get(i).getNom();
            if(ch.equalsIgnoreCase(p.getSecteurTravail()))
            {
                if((ch.equalsIgnoreCase("Import-Export"))||(ch.equalsIgnoreCase("Casinos"))
                    ||(ch.equalsIgnoreCase("Etablissement de jeux de hasard"))||(ch.equalsIgnoreCase("Clubs VIP")))
                {t=true;
                    note_p=note_p+25;}

                if((ch.equalsIgnoreCase("Petrole"))||(ch.equalsIgnoreCase("Chimie-Parachimie"))||(ch.equalsIgnoreCase("Arment")))
                {t=true;
                    note_p=note_p+20;}

                if((ch.equalsIgnoreCase("Transport maritime"))||(ch.equalsIgnoreCase("Bureaux de change")))
                {t=true;
                    note_p=note_p+20;}

                if((ch.equalsIgnoreCase("Commerce"))||(ch.equalsIgnoreCase("Automobile-Reparation automobiles"))||(ch.equalsIgnoreCase("Immobilier"))||(ch.equalsIgnoreCase("BTP-Materiaux de construction")))
                {t=true;
                    note_p=note_p+15;}

                if((ch.equalsIgnoreCase("Metaux et pierres precieuse"))||(ch.equalsIgnoreCase("Industrie petro-chimique"))||(ch.equalsIgnoreCase("Relations d affaire avec etats ou gouvernements etrangers")))
                {t=true;
                    note_p=note_p+18;}

                if((ch.equalsIgnoreCase("Objets d art et antiquite"))||(ch.equalsIgnoreCase("Informatique-Telecoms")))
                {t=true;
                    note_p=note_p+5;}
            }
            else{
                i++;
            }
        }

        return note_p;
    }

}
