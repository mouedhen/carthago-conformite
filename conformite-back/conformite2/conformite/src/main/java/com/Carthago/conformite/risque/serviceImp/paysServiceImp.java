package com.Carthago.conformite.risque.serviceImp;

import com.Carthago.conformite.clients.serviceImp.nationaliteServiceImp;
import com.Carthago.conformite.risque.entities.listeCorruption;
import com.Carthago.conformite.risque.entities.pays;
import com.Carthago.conformite.risque.services.paysService;
import org.springframework.stereotype.Service;
import com.opencsv.CSVReader;

import java.io.*;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

@Service("PaysServiceImpl")
public class paysServiceImp implements paysService {


    private final ArrayList<listeCorruption> countries3;
    private final ArrayList<pays> countries1;
    private final ArrayList<pays> countries2;

    public paysServiceImp() {

        countries1 = new ArrayList();
        countries2 = new ArrayList();
        countries3=new ArrayList();
    }

    //EXTRAIRE LA LISTE GRISE SELON GAFI
    @Override
    public ArrayList<pays> findAllListeGrise() {

        FileInputStream fis1 = null;

        try {

            String fileName = "src/main/resources/liste_pays/listeGrise.csv";

            fis1 = new FileInputStream(new File(fileName));
            CSVReader reader = new CSVReader(new InputStreamReader(fis1));
            String[] nextLine;
            reader.readNext();

            while ((nextLine = reader.readNext()) != null) {

                pays newCountry = new pays(Long.valueOf(nextLine[0]),
                    nextLine[1]);
                countries1.add(newCountry);
            }

        } catch (FileNotFoundException ex) {
            Logger.getLogger(nationaliteServiceImp.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(nationaliteServiceImp.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (fis1 != null) {
                    fis1.close();
                }
            } catch (IOException ex) {
                Logger.getLogger(nationaliteServiceImp.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return countries1;
    }

    //EXTRAIRE LA LISTE NOIRE SELON GAFI
    @Override
    public ArrayList<pays> findAllListeNoire() {

        FileInputStream fis1 = null;

        try {

            String fileName = "src/main/resources/liste_pays/listeNoire.csv";

            fis1 = new FileInputStream(new File(fileName));
            CSVReader reader = new CSVReader(new InputStreamReader(fis1));
            String[] nextLine;
            reader.readNext();

            while ((nextLine = reader.readNext()) != null) {

                pays newCountry = new pays(Long.valueOf(nextLine[0]),
                    nextLine[1]);
                countries1.add(newCountry);
            }

        } catch (FileNotFoundException ex) {
            Logger.getLogger(nationaliteServiceImp.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(nationaliteServiceImp.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (fis1 != null) {
                    fis1.close();
                }
            } catch (IOException ex) {
                Logger.getLogger(nationaliteServiceImp.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return countries1;
    }

    //EXTRAIRE LA LISTE des pays avec son rang dans la liste de corruption
    @Override
    public ArrayList<listeCorruption> findAllListeCorruption() {

        FileInputStream fis1 = null;

        try {

            String fileName = "src/main/resources/liste_pays/paysCorruption.csv";

            fis1 = new FileInputStream(new File(fileName));
            CSVReader reader = new CSVReader(new InputStreamReader(fis1));
            String[] nextLine;
            reader.readNext();

            while ((nextLine = reader.readNext()) != null) {

                listeCorruption newCountry = new listeCorruption(Long.valueOf(nextLine[0]),
                    Integer.valueOf(nextLine[1]),nextLine[2]);
                countries3.add(newCountry);
            }

        } catch (FileNotFoundException ex) {
            Logger.getLogger(nationaliteServiceImp.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(nationaliteServiceImp.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (fis1 != null) {
                    fis1.close();
                }
            } catch (IOException ex) {
                Logger.getLogger(nationaliteServiceImp.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return countries3;
    }


    //EXTRAIRE LES PAYS DE L ECONOMIE PARALLELE:
    @Override
    public ArrayList<pays> findAllListeEconomieInformelle() {

        FileInputStream fis1 = null;

        try {

            String fileName = "src/main/resources/liste_pays/paysEconomieInformelle.csv";

            fis1 = new FileInputStream(new File(fileName));
            CSVReader reader = new CSVReader(new InputStreamReader(fis1));
            String[] nextLine;
            reader.readNext();

            while ((nextLine = reader.readNext()) != null) {

                pays newCountry = new pays(Long.valueOf(nextLine[0]),
                    nextLine[1]);
                countries1.add(newCountry);
            }

        } catch (FileNotFoundException ex) {
            Logger.getLogger(nationaliteServiceImp.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(nationaliteServiceImp.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (fis1 != null) {
                    fis1.close();
                }
            } catch (IOException ex) {
                Logger.getLogger(nationaliteServiceImp.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return countries1;
    }


    //EXTRAIRE LES PAYS EN GEURRE:
    @Override
    public ArrayList<pays> findAllListePaysGeurre() {

        FileInputStream fis1 = null;

        try {

            String fileName = "src/main/resources/liste_pays/paysEnGuerre.csv";

            fis1 = new FileInputStream(new File(fileName));
            CSVReader reader = new CSVReader(new InputStreamReader(fis1));
            String[] nextLine;
            reader.readNext();

            while ((nextLine = reader.readNext()) != null) {

                pays newCountry = new pays(Long.valueOf(nextLine[0]),
                    nextLine[1]);
                countries1.add(newCountry);
            }

        } catch (FileNotFoundException ex) {
            Logger.getLogger(nationaliteServiceImp.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(nationaliteServiceImp.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (fis1 != null) {
                    fis1.close();
                }
            } catch (IOException ex) {
                Logger.getLogger(nationaliteServiceImp.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return countries1;
    }


    //EXTRAIRE LES PAYS Producteurs de drogue:
    @Override
    public   ArrayList<pays> findAllListePaysProducteursDrogue(){

        FileInputStream fis1 = null;

        try {

            String fileName = "src/main/resources/liste_pays/paysProducteursDrogue.csv";

            fis1 = new FileInputStream(new File(fileName));
            CSVReader reader = new CSVReader(new InputStreamReader(fis1));
            String[] nextLine;
            reader.readNext();

            while ((nextLine = reader.readNext()) != null) {

                pays newCountry = new pays(Long.valueOf(nextLine[0]),
                    nextLine[1]);
                countries1.add(newCountry);
            }

        } catch (FileNotFoundException ex) {
            Logger.getLogger(nationaliteServiceImp.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(nationaliteServiceImp.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (fis1 != null) {
                    fis1.close();
                }
            } catch (IOException ex) {
                Logger.getLogger(nationaliteServiceImp.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return countries1;
    }


}

