package com.Carthago.conformite.clients.services;

import com.Carthago.conformite.clients.entities.representantLegal;

import java.util.List;
import java.util.Optional;

public interface representantLegalService {

    /**
     * Cette Methode permet d'enregistrer un representant Legal.
     */
    void saveRepresentantLegal(representantLegal RL);


    /**
     * Cette Methode permet d'enregistrer les modifications exercées sur un representant Legal.
     */
    void updateRepresentantLegal(long codeClient,representantLegal RL);


    /**
     * Cette Methode permet de lister tous les representants Legals.
     */
    List<representantLegal> listRepresentantLegal();


    /**
     * Cette Methode permet de supprimer un representant Legal.
     */
    void removeRepresentantLegal(long codeClient);


    /**
     * Cette Methode permet de trouver un representant Legal par son CodeClient.
     */
    public Optional<representantLegal> findRepresentantLegal(long codeClient);
}
