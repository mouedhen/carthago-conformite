package com.Carthago.conformite.clients.serviceImp;

import com.Carthago.conformite.clients.entities.representantLegal;
import com.Carthago.conformite.clients.repositories.representantLegalRepository;
import com.Carthago.conformite.clients.services.representantLegalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service("representantLegalServiceImpl")
public class representantLegalServiceImp implements representantLegalService {

    @Autowired
    representantLegalRepository RLRepository;


    /**
     * C'est l'implementation de la methode qui permet d'enregistrer cet objet
     */
    @Override
    public void saveRepresentantLegal(representantLegal RL) {
        RLRepository.save(RL);
    }


    /**
     * C'est l'implementation de la methode qui permet
     * d'enregitrer les modifications exercées sur cet objet
     */
    @Override
    public void updateRepresentantLegal
    (long codeC,representantLegal newRepresentantLegal) {

        representantLegal representantLegal;
        Optional<representantLegal> OldRepresentantLegal
            =RLRepository.findById(codeC);

        if(OldRepresentantLegal.isPresent())
        {
            representantLegal=OldRepresentantLegal.get();
            RLRepository.save(representantLegal);
        }
    }

    /**
     * C'est l'implementation de la methode qui permet de lister tous ces objets
     */
    @Override
    public List<representantLegal> listRepresentantLegal()
    {
        return RLRepository.findAll();
    }

    /**
     * C'est l'implementation de la methode qui permet de supprimer cet objet
     */
    @Override
    public  void removeRepresentantLegal(long CodeC){
        RLRepository.deleteById(CodeC);
    }

    /**
     * C'est l'implementation de la methode qui permet de trouver cet objet par son id
     */
    @Override
    public Optional<representantLegal> findRepresentantLegal(long CodeC){
        return RLRepository.findById(CodeC);
    }
}

