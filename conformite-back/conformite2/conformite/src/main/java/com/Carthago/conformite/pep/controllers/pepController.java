package com.Carthago.conformite.pep.controllers;


import com.Carthago.conformite.pep.entities.pep;
import com.Carthago.conformite.pep.services.pepService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value="/pep",method= {RequestMethod.GET})
public class pepController {

    @Autowired
    pepService PService;


    /**
     * Cette methode permet la creation de persBe
     */
    @PostMapping("/Create")
    public String createPep
    (@Validated @RequestBody pep P)
    {
        PService.savePep(P);
        return "Pep cree";
    }


    /**
     * Cette methode de lister les persBes
     */
    @GetMapping("/GetAll")
    public List<pep> getAllPep()
    {
        return PService.listPep();

    }

    /**
     * Cette methode permet de trouver Pep par son id
     */
    @GetMapping("/Get/{id}")
    public Optional<pep> findPep
    (@PathVariable Long id)
    {

        return PService.findPep(id);
    }

    /**
     * Cette methode permet de modifier Pep
     */
    @PutMapping("/Update/{id}")
    public String UpdatePep
    (@Validated @RequestBody pep P,@PathVariable Long id)
    {
        PService.updatePep(id,P);
        return "La mise a jour a ete faite avec succees";
    }


    /**
     * Cette methode permet la suppression pep par son id
     */
    @DeleteMapping(value="/Delete/{id}")
    public String deletePep(@PathVariable String id)
    {
        PService.removePep (Long.parseLong(id));
        return " supprimee avec succee";
    }




}

