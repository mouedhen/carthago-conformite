package com.Carthago.conformite.clients.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
public class personneMorale extends contact {

    private String denominationSociale = ""; //Denomination sociale
    private String objetSocial = "";// Objet social de la personne morale
    private Long matriculeFiscale; // Matricule fiscale de la personne morale
    private Date dateConstrution; //date construction
    private String paysConstruction = ""; //pays de construction
    private Date dateEnregistrement; //date d'enregistrement
    private boolean associesPpe = false; //true s'il y a des associes politiquement exposés
    private String natureOperation = ""; //nature de l'operation de la PM
    private String origineFonds = ""; //origine des fonds de la personne Morale
    private Boolean origineFondsTunisien = true; //true si les fonds sont tunisiens
    private String paysOrigineFonds = ""; //specification de l'origine si false
    private String nature = ""; //nature personne morale
    private String domaineActivite = ""; //domaine d'activite de la personne morale
    private String anciennetéProfesionelle = ""; //anciennete de la personne
    private String formeJuridique = ""; //forme juridique
    private Boolean us= false;//true si La société est créée en vertu de la loi américaine
    private Boolean actionnaireUs=false;//true si  	Le capital de la société
           // est détenu par un actionnaire ou plus qui sont des contribuables américains
    private Boolean giin=false; //Institution Financière non américaine (disposant ou non d’un code GIIN)
    private Boolean statutFatca;//statut fatca de la personne morale
    private Boolean actionnaireOffshore=false ; //true si la personne detient un ou plusieurs
           //actionnaires offshore
    private Boolean marcheBoursier=false ; //true si l'ese est cotée sur le marche boursier
    private Boolean participationPub=false ;//true si l'ese est à participation Pub
    private String secteurTravail="";//secteur travail de la personne morale



    //getters et setters
    public String getDenominationSociale() {
        return denominationSociale;
    }

    public void setDenominationSociale(String denominationSociale) {
        this.denominationSociale = denominationSociale;
    }

    public String getObjetSocial() {
        return objetSocial;
    }

    public void setObjetSocial(String objetSocial) {
        this.objetSocial = objetSocial;
    }

    public Long getMatriculeFiscale() {
        return matriculeFiscale;
    }

    public void setMatriculeFiscale(Long matriculeFiscale) {
        this.matriculeFiscale = matriculeFiscale;
    }

    public Date getDateConstrution() {
        return dateConstrution;
    }

    public void setDateConstrution(Date dateConstrution) {
        this.dateConstrution = dateConstrution;
    }

    public String getPaysConstruction() {
        return paysConstruction;
    }

    public void setPaysConstruction(String paysConstruction) {
        this.paysConstruction = paysConstruction;
    }

    public Date getDateEnregistrement() {
        return dateEnregistrement;
    }

    public void setDateEnregistrement(Date dateEnregistrement) {
        this.dateEnregistrement = dateEnregistrement;
    }

    public boolean isAssociesPpe() {
        return associesPpe;
    }

    public void setAssociesPpe(boolean associesPpe) {
        this.associesPpe = associesPpe;
    }

    public String getNatureOperation() {
        return natureOperation;
    }

    public void setNatureOperation(String natureOperation) {
        this.natureOperation = natureOperation;
    }

    public String getOrigineFonds() {
        return origineFonds;
    }

    public void setOrigineFonds(String origineFonds) {
        this.origineFonds = origineFonds;
    }

    public Boolean getOrigineFondsTunisien() {
        return origineFondsTunisien;
    }

    public void setOrigineFondsTunisien(Boolean origineFondsTunisien) {
        this.origineFondsTunisien = origineFondsTunisien;
    }

    public String getPaysOrigineFonds() {
        return paysOrigineFonds;
    }

    public void setPaysOrigineFonds(String paysOrigineFonds) {
        this.paysOrigineFonds = paysOrigineFonds;
    }

    public String getNature() {
        return nature;
    }

    public void setNature(String nature) {
        this.nature = nature;
    }

    public String getDomaineActivite() {
        return domaineActivite;
    }

    public void setDomaineActivite(String domaineActivite) {
        this.domaineActivite = domaineActivite;
    }

    public String getAnciennetéProfesionelle() {
        return anciennetéProfesionelle;
    }

    public void setAnciennetéProfesionelle(String anciennetéProfesionelle) {
        this.anciennetéProfesionelle = anciennetéProfesionelle;
    }

    public String getFormeJuridique() {
        return formeJuridique;
    }

    public void setFormeJuridique(String formeJuridique) {
        this.formeJuridique = formeJuridique;
    }

    public List<representantLegal> getRepresentants() {
        return representants;
    }

    public void setRepresentants(List<representantLegal> representants) {
        this.representants = representants;
    }

    public List<beneficiaireEffectif> getBeneficiaireEffectifList() {
        return beneficiaireEffectifList;
    }

    public void setBeneficiaireEffectifList
        (List<beneficiaireEffectif> beneficiaireEffectifList) {
        this.beneficiaireEffectifList = beneficiaireEffectifList;
    }

    public Boolean getUs() {
        return us;
    }

    public void setUs(Boolean us) {
        this.us = us;
    }

    public Boolean getActionnaireUs() {
        return actionnaireUs;
    }

    public void setActionnaireUs(Boolean actionnaireUs) {
        this.actionnaireUs = actionnaireUs;
    }

    public Boolean getGiin() {
        return giin;
    }

    public void setGiin(Boolean giin) {
        this.giin = giin;
    }

    public Boolean getStatutFatca() {
        return statutFatca;
    }

    public void setStatutFatca(Boolean statutFatca) {
        this.statutFatca = statutFatca;
    }

    public Boolean getActionnaireOffshore() {
        return actionnaireOffshore;
    }

    public void setActionnaireOffshore(Boolean actionnaireOffshore) {
        this.actionnaireOffshore = actionnaireOffshore;
    }

    public Boolean getMarcheBoursier() {
        return marcheBoursier;
    }

    public void setMarcheBoursier(Boolean marcheBoursier) {
        this.marcheBoursier = marcheBoursier;
    }

    public Boolean getParticipationPub() {
        return participationPub;
    }

    public void setParticipationPub(Boolean participationPub) {
        this.participationPub = participationPub;
    }

    public String getSecteurTravail() {
        return secteurTravail;
    }

    public void setSecteurTravail(String secteurTravail) {
        this.secteurTravail = secteurTravail;
    }
    //constructeur

    public personneMorale(Long codeClient) {
        super(codeClient);
    }



    //relation personne morale et representant legal
    @JsonBackReference
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable( name = "RepPers",
        joinColumns = @JoinColumn( name = "codeClient" ),
        inverseJoinColumns = @JoinColumn( name = "codeRep" ) )
    private List<representantLegal> representants = new ArrayList<>();

    //relation personne morale et beneficiaire effectif

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable( name = "PersBe",
        joinColumns = @JoinColumn( name = "codeClient1" ),
        inverseJoinColumns = @JoinColumn( name = "codeBe" ) )
    private List<beneficiaireEffectif>  beneficiaireEffectifList= new ArrayList<>();

}
