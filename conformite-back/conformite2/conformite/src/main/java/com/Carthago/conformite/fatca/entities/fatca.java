package com.Carthago.conformite.fatca.entities;

import com.Carthago.conformite.clients.entities.contact;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
public class fatca {

    @Id
    private Long codeFatca;
    private boolean statut=false;
    private String statutP="";//statut fatca de la personne morale

    //Relations
    @OneToOne
    @JoinColumn(name = "codeClient1")
    private contact contact1;



}
