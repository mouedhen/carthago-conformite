package com.Carthago.conformite.clients.serviceImp;


import com.Carthago.conformite.clients.entities.RepPers;
import com.Carthago.conformite.clients.entities.persNat;
import com.Carthago.conformite.clients.entities.personneMorale;
import com.Carthago.conformite.clients.repositories.RepPersRepository;
import com.Carthago.conformite.clients.services.RepPersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service("RepPersServiceImpl")
public class RepPersServiceImp implements RepPersService {

    @Autowired
    RepPersRepository R;

    /**
     * C'est l'implementation de la methode qui permet d'enregistrer cet objet
     */
    @Override
    public void saveRepPers(RepPers P)
    {   P.setCode(P.getCodeClient());
        R.save(P);
    }

    /**
     * C'est l'implementation de la methode qui permet de lister tous ces objets
     */
    @Override
    public List<RepPers> listRepPers(){
        return R.findAll();
    }

    /**
     * C'est l'implementation de la methode qui permet
     * de trouver les Representants legals d'une personne Physique
     */

    @Override
    public List<RepPers> listRepPersCodeClient(Long code)
    {
        return R.findAllByCodeClient(code);
    }

    /**
     * Retourne une liste contenant tous les codes de rep d'une personne Morale
     * @param id
     * @return
     */
    @Override
    public ArrayList<Long> listRepPersCode(long id) {
        List<RepPers> l=R.findAll();
        Long l1;

        ArrayList<Long>liste=new ArrayList<Long>();
        for(int i=0;i<l.size();i++)

        {
            if(l.get(i).getCodeClient()==id)
            {

                l1=(l.get(i).getCodeRep());
                liste.add(l1);

            }
        }
        return liste;
    }

    @Override
    public RepPers findByCodeClient(Long codeClient){
        return R.findByCodeClient(codeClient);

    }
}
