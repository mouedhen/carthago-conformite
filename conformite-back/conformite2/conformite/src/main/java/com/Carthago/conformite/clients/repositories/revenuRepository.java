package com.Carthago.conformite.clients.repositories;

import com.Carthago.conformite.clients.entities.revenu;
import org.springframework.data.jpa.repository.JpaRepository;

public interface revenuRepository extends JpaRepository<revenu,Long> {
}
