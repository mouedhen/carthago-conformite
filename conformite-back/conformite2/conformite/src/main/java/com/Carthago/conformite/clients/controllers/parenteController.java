package com.Carthago.conformite.clients.controllers;

import com.Carthago.conformite.clients.entities.parente;
import com.Carthago.conformite.clients.services.parenteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value="/parente",method= {RequestMethod.GET})

public class parenteController {

    @Autowired
    parenteService PService;


    /**
     * Cette methode permet la creation de parente
     */
    @PostMapping("/Create")
    public String createParente
    (@Validated @RequestBody parente P)
    {
        PService.saveParente(P);
        return "Parente cree";
    }


    /**
     * Cette methode de lister les parentes
     */
    @GetMapping("/GetAll")
    public List<parente> getAllParente()
    {
        return PService.listParente();

    }

    /**
     * Cette methode permet de trouver Parente par son id
     */
    @GetMapping("/Get/{id}")
    public Optional<parente> findParente
    (@PathVariable Long id)
    {

        return PService.findParente(id);
    }

    /**
     * Cette methode permet de modifier Parente
     */
    @PutMapping("/Update/{id}")
    public String UpdateParente
    (@Validated @RequestBody parente P,@PathVariable Long id)
    {
        PService.updateParente(id,P);
        return "La mise a jour a ete faite avec succees";
    }


    /**
     * Cette methode permet la suppression Parente par son id
     */
    @DeleteMapping(value="/Delete/{id}")
    public String deleteParente(@PathVariable String id)
    {
        PService.removeParente (Long.parseLong(id));
        return "Parente supprimee avec succee";
    }



}

