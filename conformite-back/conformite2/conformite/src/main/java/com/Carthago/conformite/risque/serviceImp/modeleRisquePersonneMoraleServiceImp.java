package com.Carthago.conformite.risque.serviceImp;

import com.Carthago.conformite.risque.entities.modeleRisquePersonneMorale;
import com.Carthago.conformite.risque.repositories.modeleRisquePersonneMoraleRepository;
import com.Carthago.conformite.risque.services.modeleRisquePersonneMoraleService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;

public class modeleRisquePersonneMoraleServiceImp implements modeleRisquePersonneMoraleService {

    @Autowired
    modeleRisquePersonneMoraleRepository r ;

    @Override
    public void save (modeleRisquePersonneMorale P)
    {
       r.save(P);
    }


    @Override
    public Optional<modeleRisquePersonneMorale> find (long id)
    {
        return r.findById(id);
    }

    @Override
    public void update(modeleRisquePersonneMorale m , long id)
    {
        r.save(m);
    }
}
