package com.Carthago.conformite.clients.entities;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity

public class nationalite implements Serializable {

    //declaration des variables
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private  Long codeNationalite;
    private String nationalite="";

    //getters et setters
    public Long getCodeNationalite() {
        return codeNationalite;
    }

    public void setCodeNationalite(Long codeNationalite) {
        this.codeNationalite = codeNationalite;
    }

    public String getNationalite() {
        return nationalite;
    }

    public void setNationalite(String nationalite) {
        this.nationalite = nationalite;
    }

    public List<persNat> getListPersNat2() {
        return listPersNat2;
    }

    public void setListPersNat2(List<persNat> listPersNat2) {
        this.listPersNat2 = listPersNat2;
    }

    //constructeurs
    public nationalite(Long codeNationalite) {
        this.codeNationalite = codeNationalite;
    }

    public nationalite(Long code_nat, String nationalite) {
        this.codeNationalite = code_nat;
        this.nationalite = nationalite;
    }


    //Relation entre pers_nat et nationalite
    @OneToMany(mappedBy = "nat",cascade = CascadeType.ALL)
    private List<persNat> listPersNat2;





}
