package com.Carthago.conformite.clients.entities;

import com.Carthago.conformite.clients.repositories.personneMoraleRepository;
import com.Carthago.conformite.clients.repositories.personnePhysiqueRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.Rollback;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
class personnePhysiqueTest {

    @Autowired
    personnePhysiqueRepository repository;

    @Test
    @Rollback(false)
    void createPersonnePhysique()
    { personnePhysique p = new personnePhysique((long) 15014561);
        personnePhysique savedPersonne=repository.save(p);
        assertNotNull(savedPersonne);

    }
    @Test
    void findPersonnePhysique()
    {
        long codeClient = (long) 15014561;
        Optional<personnePhysique> p = repository.findById(codeClient);
        assertThat(codeClient).isEqualTo(p.get().getCodeClient());
    }

    @Test
    @Rollback(false)
    void updatePersonnePhysique()
    { String nom="islem";
        personnePhysique PP = new personnePhysique((long)15014561,nom,"sahli");
        repository.save(PP);
        Optional<personnePhysique> updatedPM=repository.findById((long)15014561);
        assertThat(nom).isEqualTo(updatedPM.get().getNom());


    }
    @Test
    void listPersonnePhysique()
    {
        List<personnePhysique> liste = repository.findAll();
        assertThat(liste).size().isGreaterThan(0);
    }

    @Test
    void deletePersonnePhysique()
    {
        long codeClient = (long) 15014561;
        repository.deleteById(codeClient);
        boolean present=repository.findById(codeClient).isPresent();
        assertFalse(present);
    }
}
