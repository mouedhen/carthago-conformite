package com.Carthago.conformite.clients.entities;

import com.Carthago.conformite.clients.repositories.personneMoraleRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.Rollback;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
class personneMoraleTest {

@Autowired
personneMoraleRepository repository;

@Test
@Rollback(false)
    void createPersonneMorale()
  { personneMorale p = new personneMorale((long) 15014560);
    personneMorale savedPersonne=repository.save(p);
    assertNotNull(savedPersonne);

  }
@Test
    void findPersonneMorale()
{
    long codeClient = (long) 15014560;
    Optional<personneMorale> p = repository.findById(codeClient);
    assertThat(codeClient).isEqualTo(p.get().getCodeClient());
}

@Test
@Rollback(false)
    void updatePersonneMorale()
{ String denominationSociale="exemple";
  personneMorale PM = new personneMorale((long)15014560,denominationSociale);
  repository.save(PM);
  Optional<personneMorale> updatedPM=repository.findById((long)15014560);
    assertThat(denominationSociale).isEqualTo(updatedPM.get().getDenominationSociale());


}
@Test
    void listPersonneMorale()
{
    List<personneMorale> liste = repository.findAll();
    assertThat(liste).size().isGreaterThan(0);
}

@Test
    void deletePersonneMorale()
{
    long codeClient = (long) 15014560;
    repository.deleteById(codeClient);
    boolean present=repository.findById(codeClient).isPresent();
    assertFalse(present);
}
}
