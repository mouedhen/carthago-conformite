package com.Carthago.conformite.compte.controllers;


import com.Carthago.conformite.compte.entities.compteCourant;
import com.Carthago.conformite.compte.services.compteCourantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
@RestController
@RequestMapping(value="/compteCourant",method= {RequestMethod.GET})
public class compteCourantController {
    @Autowired
    compteCourantService PService;


    /**
     * Cette methode permet la creation de compte courant pour un client moral
     */
    @PostMapping("/CreateCompteClientMoral/{id}")
    public String createCompteCourantClienMoral
    (@Validated @RequestBody compteCourant P,@PathVariable Long id)
    {
        PService.saveCompteCourantClientMoral(P,id);
        return "Parente cree";
    }
    /**
     * Cette methode permet la creation de compte courant pour un client moral
     */
    @PostMapping("/CreateCompteClientPhysique/{id}")
    public String createCompteCourantClientPhysique
    (@Validated @RequestBody compteCourant P,@PathVariable Long id)
    {
        PService.saveCompteCourantClientPhysique(P,id);
        return "Parente cree";
    }


    /**
     * Cette methode de lister les parentes
     */
    @GetMapping("/GetAll")
    public List<compteCourant> getAllParente()
    {
        return PService.listCompteCourant();

    }


    /**
     * Cette methode permet de modifier Parente
     */
    @PutMapping("/Update/{id}")
    public String UpdateParente
    (@Validated @RequestBody compteCourant P,@PathVariable Long id)
    {
        PService.updateCompteCourant(id,P);
        return "La mise a jour a ete faite avec succees";
    }


    /**
     * Cette methode permet la suppression compteCourant par son id
     */
    @DeleteMapping(value="/Delete/{id}")
    public String deleteParente(@PathVariable String id)
    {
        PService.removeCompteCourant (Long.parseLong(id));
        return "compteCourant supprimee avec succee";
    }



}
