package com.Carthago.conformite.compte.repositories;

import com.Carthago.conformite.compte.entities.compte;
import org.springframework.data.jpa.repository.JpaRepository;

public interface compteRepository extends JpaRepository<compte,Long> {
}
