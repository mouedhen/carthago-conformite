package com.Carthago.conformite.compte.repositories;


import com.Carthago.conformite.compte.entities.compteCourant;
import org.springframework.data.jpa.repository.JpaRepository;

public interface compteCourantRepository extends JpaRepository<compteCourant,Long> {
}
