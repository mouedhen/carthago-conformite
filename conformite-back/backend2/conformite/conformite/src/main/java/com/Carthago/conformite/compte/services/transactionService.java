package com.Carthago.conformite.compte.services;

import com.Carthago.conformite.clients.entities.contact;
import com.Carthago.conformite.clients.entities.stat;
import com.Carthago.conformite.compte.entities.compte;
import com.Carthago.conformite.compte.entities.transaction;

import java.util.List;
import java.util.Optional;

public interface transactionService {
    /**
     * Cette Methode permet d'enregistrer une transaction.
     */
    void saveTransaction(transaction t);
    /**
     * Cette Methode permet d'enregistrer les modifications exercées
     * sur une transaction.
     */
    void updateTransaction(long codeTransaction,transaction t);
    /**
     * Cette Methode permet de lister tous les transactions.
     */
    List<transaction> listTransactions(String ch);


    List<transaction> listDesTransactions();
    /**
     * Cette Methode permet de supprimer une transaction.
     */
    void removeTransaction(long codeTransaction);
    /**
     * Cette Methode permet de trouver une transaction par son codeTransaction.
     */
     transaction findTransaction(String codeE);
    /**
     * Cette Methode permet de verser un montant dans votre compte
     */
    void versement (transaction t);
    /**
     * Methode de virement
     */
    Optional<compte> virement (transaction t);
    /**
     * Methode de prelevement
     */
    void prelevement (transaction t);
    /**
     * Cette Methode permet de calculer le risque d'une transaction.
     */
    Boolean risqueTransaction(transaction t);
    /**
     * Cette Methode permet de trouver un compte par transaction.
     * @return
     */
    Optional<contact> findCompte(long codeTransaction);

    /**
     * Cette fonction permet de retourner la liste des transactions douteuses
     * @return
     */
    List<transaction> listeTransactionsDouteusesDD(String ch);

    /**
     * Cette fonction permet de retourner la liste des transactions douteuses
     * @return
     */
    List<transaction> listeTransactionsDouteuses();

    /**
     * Cette fonction permet de retourner la liste des transactions douteuses
     * @return
     */
    List<transaction> listeTransactionsRefuse(String ch);

    /**
     * Cette fonction permet de retourner la liste des transactions douteuses
     * @return
     */
    List<transaction> listeTransactionsDouteusesValides(String ch);

    /**
     * Cette fonction permet de retourner la liste des transactions douteuses
     * @return
     */
    List<transaction> listeTransactionsValides(String ch);

    String findContactCompte (Long code);

    /**
     * Cette fonction permet de retourner la liste des transactions douteuses
     * @return
     */
    List<stat> nbreTransactions(Long code);
    /**
     * Cette fonction permet de retourner la liste des transactions douteuses
     * @return
     */
    List<stat> nbreTransactionsD();
    /**
     * Cette fonction permet de retourner la liste des transactions par statut
     * @return
     */
    List<stat> nbreTransactionstype();

    /**
     * Cette fonction permet de retourner la liste des transactions douteuses
     * @return
     */
    List<stat> nbreTransactionsDN();
    /**
     * Cette Methode permet d'enregistrer les modifications exercées
     * sur une transaction.
     */
     void traiterTransaction(long codeTransaction,transaction t);
}
