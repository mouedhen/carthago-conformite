package com.Carthago.conformite.risque.services;
import com.Carthago.conformite.risque.entities.modeleCotationRisque;
import java.util.List;
import java.util.Optional;
public interface modeleCotationRisqueService {
    void saveModele(modeleCotationRisque m);
    void updateModele(modeleCotationRisque m);
    Optional<modeleCotationRisque> findModeleCotationRisque (Long id);
    List<modeleCotationRisque> listModeleCotationRisque();
}