package com.Carthago.conformite.clients.repositories;

import com.Carthago.conformite.clients.entities.parente;
import org.springframework.data.jpa.repository.JpaRepository;

public interface parenteRepository extends JpaRepository<parente,Long> {
}
