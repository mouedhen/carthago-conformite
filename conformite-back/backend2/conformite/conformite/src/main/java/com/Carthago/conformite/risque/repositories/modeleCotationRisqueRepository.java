package com.Carthago.conformite.risque.repositories;

import com.Carthago.conformite.risque.entities.modeleCotationRisque;
import org.springframework.data.jpa.repository.JpaRepository;
public interface modeleCotationRisqueRepository extends JpaRepository<modeleCotationRisque,Long>
{
}