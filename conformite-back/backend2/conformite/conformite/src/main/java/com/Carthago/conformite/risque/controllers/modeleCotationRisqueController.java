package com.Carthago.conformite.risque.controllers;
import com.Carthago.conformite.fatca.entities.fatca;
import com.Carthago.conformite.risque.entities.modeleCotationRisque;
import com.Carthago.conformite.risque.entities.risqueTransaction;
import com.Carthago.conformite.risque.services.modeleCotationRisqueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.Optional;
@RestController
@RequestMapping(value="/modeleCotationRisque",method= {RequestMethod.GET})
public class modeleCotationRisqueController {
    @Autowired
    modeleCotationRisqueService service;
    /**
     * Cette methode permet de trouver Modele par son id
     * @return
     */
    @GetMapping("/Get/{id}")
    public Optional<modeleCotationRisque> findModele
    (@PathVariable Long id)
    {
        return service.findModeleCotationRisque(id);
    }
    /**
     * Cette methode permet de modifier modeleCotationRisque
     */
    @PutMapping("/Update/{id}")
    public String modeleCotationRisque
    (@Validated @RequestBody modeleCotationRisque f, @PathVariable Long id)
    {
        service.updateModele(f);
        return "La mise a jour a ete faite avec succees";
    }
    @GetMapping("/GetAll")
    public List<modeleCotationRisque> listModeleCotationRisque(){
        return service.listModeleCotationRisque();
    }
    @PostMapping("/Create")
    public String create
            (@Validated @RequestBody modeleCotationRisque R)
    {
        service.saveModele(R);
        return " cree";
    }
}
