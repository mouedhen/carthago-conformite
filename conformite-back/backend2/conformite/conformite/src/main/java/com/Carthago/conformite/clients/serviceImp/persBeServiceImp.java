package com.Carthago.conformite.clients.serviceImp;


import com.Carthago.conformite.clients.entities.RepPers;
import com.Carthago.conformite.clients.entities.persBe;
import com.Carthago.conformite.clients.repositories.persBeRepository;
import com.Carthago.conformite.clients.services.persBeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.parameters.P;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.ArrayList;
import java.util.List;

@Service("PersBeServiceImpl")
public class persBeServiceImp  implements persBeService {

    @Autowired
    persBeRepository repository;


    /**
     * Retourne une liste contenant tous les codes de rep d'une personne Morale
     * @param id
     * @return
     */
    @Override
    public ArrayList<Long> listPersBeCode(@PathVariable long id) {
        List<persBe> l=repository.findAll();


        ArrayList<Long>liste=new ArrayList<Long>();
        for(int i=0;i<l.size();i++)

        {
            if(l.get(i).getCodeClient1()==id)
            {
               liste.add((l.get(i).getCodeBe()));

            }
        }
        return liste;
    }

    @Override
    public List<persBe> findAll ()
    {
        return repository.findAll();
    }
}
