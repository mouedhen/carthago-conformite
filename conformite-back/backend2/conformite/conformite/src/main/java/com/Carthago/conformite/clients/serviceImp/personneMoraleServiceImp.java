package com.Carthago.conformite.clients.serviceImp;

import com.Carthago.conformite.clients.entities.personneMorale;
import com.Carthago.conformite.clients.repositories.personneMoraleRepository;
import com.Carthago.conformite.clients.services.personneMoraleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service("personneMoraleServiceImpl")
public class personneMoraleServiceImp implements personneMoraleService {

    @Autowired
    personneMoraleRepository PMRepository;


    /**
     * C'est l'implementation de la methode qui permet d'enregistrer cet objet
     */
    @Override
    public void savePersonneMorale(personneMorale PM) {
        PMRepository.save(PM);
    }




    /**
     * C'est l'implementation de la methode qui permet
     * d'enregitrer les modifications exercées sur cet objet
     */
    @Override
    public void updatePersonneMorale(long codeC,personneMorale PM) {
        if(PM.getStatutPersonne().equalsIgnoreCase("En attente de validation"))
        {PMRepository.save(PM);}
        else
        {
            Optional<personneMorale> Old=PMRepository.findById(codeC);
            PM.setStatutPersonne("Client");
            PM.setListCompte(Old.get().getListCompte());
            PM.setBeneficiaireEffectifList(Old.get().getBeneficiaireEffectifList());
            PM.setRepresentants(Old.get().getRepresentants());
            PMRepository.save(PM);
        }
    }


    /**
     * C'est l'implementation de la methode qui permet de lister tous ces objets
     */
    @Override
    public List<personneMorale> listPersonneMorale(){
        return PMRepository.findAll();
    }




    /**
     * C'est l'implementation de la methode qui permet de supprimer cet objet
     */
    @Override
    public  void removePersonneMorale(long CodeC){
        PMRepository.deleteById(CodeC);
    }




    /**
     * C'est l'implementation de la methode qui permet de trouver cet objet par son id
     */
    @Override
    public Optional<personneMorale> findPersonneMorale(long CodeC){
        return PMRepository.findById(CodeC);
    }











































    @Override
    public void updateStatutClient(long id)
    {Optional<personneMorale> oldPersonneMorale=PMRepository.findById(id);
        Optional<personneMorale> P=PMRepository.findById(id);
        P.get().setBeneficiaireEffectifList(oldPersonneMorale.get().getBeneficiaireEffectifList());
        P.get().setRepresentants(oldPersonneMorale.get().getRepresentants());
        P.get().setNature(oldPersonneMorale.get().getNature());
        P.get().setFormeJuridique(oldPersonneMorale.get().getFormeJuridique());
        P.get().setNiveauRisque(oldPersonneMorale.get().getNiveauRisque());
        P.get().setRevue(oldPersonneMorale.get().getRevue());
        P.get().setStatutFatca(oldPersonneMorale.get().getStatutFatca());
        P.get().setMatriculeFiscale(oldPersonneMorale.get().getMatriculeFiscale());
        P.get().setPaysConstruction(oldPersonneMorale.get().getPaysConstruction());
        P.get().setActionnaireOffshore(oldPersonneMorale.get().getActionnaireOffshore());
        P.get().setActionnaireUs(oldPersonneMorale.get().getActionnaireUs());
        P.get().setAssociesPpe(oldPersonneMorale.get().isAssociesPpe());
        P.get().setChiffreAffaire(oldPersonneMorale.get().getChiffreAffaire());
        P.get().setDateConstrution(oldPersonneMorale.get().getDateConstrution());
        P.get().setDateEnregistrement(oldPersonneMorale.get().getDateEnregistrement());
        P.get().setDenominationSociale(oldPersonneMorale.get().getDenominationSociale());
        P.get().setDomaineActivite(oldPersonneMorale.get().getDomaineActivite());
        P.get().setGiin(oldPersonneMorale.get().getGiin());
        P.get().setMarcheBoursier(oldPersonneMorale.get().getMarcheBoursier());
        P.get().setNatureOperation(oldPersonneMorale.get().getNatureOperation());
        P.get().setObjetSocial(oldPersonneMorale.get().getObjetSocial());
        P.get().setOrigineFonds(oldPersonneMorale.get().getOrigineFonds());
        P.get().setParticipationPub(oldPersonneMorale.get().getParticipationPub());
        P.get().setPaysOrigineFonds(oldPersonneMorale.get().getPaysOrigineFonds());
        P.get().setSecteurTravail(oldPersonneMorale.get().getSecteurTravail());
        P.get().setUs(oldPersonneMorale.get().getUs());
        P.get().setActionnaireOffshore(oldPersonneMorale.get().getActionnaireOffshore());
        P.get().setAdresse(oldPersonneMorale.get().getAdresse());
        P.get().setCodeTin(oldPersonneMorale.get().isCodeTin());
        P.get().setEmail(oldPersonneMorale.get().getEmail());
        P.get().setFatca(oldPersonneMorale.get().getFatca());
        P.get().setNumTelephone(oldPersonneMorale.get().getNumTelephone());
        P.get().setPaysResidence(oldPersonneMorale.get().getPaysResidence());
        P.get().setPep(oldPersonneMorale.get().getPep());
        P.get().setStatutPersonne("Client");
    }
}

