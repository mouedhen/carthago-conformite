package com.Carthago.conformite.risque.entities;

import com.Carthago.conformite.clients.entities.contact;
import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
public class modeleGlobalPhysique {
    @Id
    private  Long code;
    private String nom;
    private String listeGrise;
    private String listeNoire;
    private String listeEconomieParallele;
    private String listeDrogue;
    private String listeGeurre;
    private String listeCorruption;
    private String secteur;
    private String profession;
    private String statut;
    private String residence;
    private int valeur;
    private int nbreTransactionFrauduleuse=0;

    //GETTERS AND SETTERS :

    public int getNbreTransactionFrauduleuse() {
        return nbreTransactionFrauduleuse;
    }

    public void setNbreTransactionFrauduleuse(int nbreTransactionFrauduleuse) {
        this.nbreTransactionFrauduleuse = nbreTransactionFrauduleuse;
    }

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public String getListeGrise() {
        return listeGrise;
    }

    public void setListeGrise(String listeGrise) {
        this.listeGrise = listeGrise;
    }

    public int getValeur() {
        return valeur;
    }

    public void setValeur(int valeur) {
        this.valeur = valeur;
    }

    public String getListeNoire() {
        return listeNoire;
    }

    public void setListeNoire(String listeNoire) {
        this.listeNoire = listeNoire;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getListeEconomieParallele() {
        return listeEconomieParallele;
    }

    public void setListeEconomieParallele(String listeEconomieParallele) {
        this.listeEconomieParallele = listeEconomieParallele;
    }

    public String getListeDrogue() {
        return listeDrogue;
    }

    public void setListeDrogue(String listeDrogue) {
        this.listeDrogue = listeDrogue;
    }

    public String getListeGeurre() {
        return listeGeurre;
    }

    public void setListeGeurre(String listeGeurre) {
        this.listeGeurre = listeGeurre;
    }

    public String getListeCorruption() {
        return listeCorruption;
    }

    public void setListeCorruption(String listeCorruption) {
        this.listeCorruption = listeCorruption;
    }

    public String getSecteur() {
        return secteur;
    }

    public void setSecteur(String secteur) {
        this.secteur = secteur;
    }

    public String getProfession() {
        return profession;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }


    //Relations
    @OneToOne
    @JoinColumn(name = "codeClient1")
    @JsonBackReference
    private contact contactModelRisque;
}
