package com.Carthago.conformite.risque.serviceImp;

import com.Carthago.conformite.clients.serviceImp.nationaliteServiceImp;
import com.Carthago.conformite.risque.entities.listeCorruption;
import com.Carthago.conformite.risque.entities.pays;
import com.Carthago.conformite.risque.services.paysService;
import org.springframework.stereotype.Service;
import com.opencsv.CSVReader;

import java.io.*;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

@Service("PaysServiceImpl")
public class paysServiceImp implements paysService {

    private final ArrayList<listeCorruption> countries3;
    private final ArrayList<pays> countries1;
    private final ArrayList<pays> countries2;
    private final ArrayList<pays> countries4;
    private final ArrayList<pays> countries5;
    private final ArrayList<pays> countries6;

    public paysServiceImp() {
        countries1 = new ArrayList();
        countries2 = new ArrayList();
        countries3= new ArrayList();
        countries4= new ArrayList();
        countries5= new ArrayList();
        countries6= new ArrayList();
    }

    //EXTRAIRE LA LISTE GRISE SELON GAFI
    @Override
    public ArrayList<pays> findAllListeGrise() {

        FileInputStream fis1 = null;

        try {

            String fileName = "src/main/resources/liste_pays/listeGrise.csv";

            fis1 = new FileInputStream(new File(fileName));
            CSVReader reader = new CSVReader(new InputStreamReader(fis1));
            String[] nextLine;
            reader.readNext();

            while ((nextLine = reader.readNext()) != null) {

                pays newCountry = new pays(Long.valueOf(nextLine[0]),
                    nextLine[1]);
                countries1.add(newCountry);
            }

        } catch (FileNotFoundException ex) {
            Logger.getLogger(nationaliteServiceImp.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(nationaliteServiceImp.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (fis1 != null) {
                    fis1.close();
                }
            } catch (IOException ex) {
                Logger.getLogger(nationaliteServiceImp.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return countries1;
    }

    //EXTRAIRE LA LISTE NOIRE SELON GAFI
    @Override
    public ArrayList<pays> findAllListeNoire() {

        FileInputStream fis2 = null;

        try {

            String fileName = "src/main/resources/liste_pays/listeNoire.csv";

            fis2 = new FileInputStream(new File(fileName));
            CSVReader reader = new CSVReader(new InputStreamReader(fis2));
            String[] nextLine;
            reader.readNext();

            while ((nextLine = reader.readNext()) != null) {

                pays newCountry = new pays(Long.valueOf(nextLine[0]),
                    nextLine[1]);
                countries2.add(newCountry);
            }

        } catch (FileNotFoundException ex) {
            Logger.getLogger(nationaliteServiceImp.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(nationaliteServiceImp.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (fis2 != null) {
                    fis2.close();
                }
            } catch (IOException ex) {
                Logger.getLogger(nationaliteServiceImp.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return countries2;
    }

    //EXTRAIRE LA LISTE des pays avec son rang dans la liste de corruption
    @Override
    public ArrayList<listeCorruption> findAllListeCorruption() {

        FileInputStream fis3 = null;

        try {

            String fileName = "src/main/resources/liste_pays/paysCorruption.csv";

            fis3 = new FileInputStream(new File(fileName));
            CSVReader reader = new CSVReader(new InputStreamReader(fis3));
            String[] nextLine;
            reader.readNext();

            while ((nextLine = reader.readNext()) != null) {

                listeCorruption newCountry = new listeCorruption(Long.valueOf(nextLine[0]),
                    Integer.valueOf(nextLine[1]),nextLine[2]);
                countries3.add(newCountry);
            }

        } catch (FileNotFoundException ex) {
            Logger.getLogger(nationaliteServiceImp.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(nationaliteServiceImp.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (fis3 != null) {
                    fis3.close();
                }
            } catch (IOException ex) {
                Logger.getLogger(nationaliteServiceImp.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return countries3;
    }

    //EXTRAIRE LES PAYS DE L ECONOMIE PARALLELE:
    @Override
    public ArrayList<pays> findAllListeEconomieInformelle() {

        FileInputStream fis4 = null;

        try {

            String fileName = "src/main/resources/liste_pays/paysEconomieInformelle.csv";

            fis4 = new FileInputStream(new File(fileName));
            CSVReader reader = new CSVReader(new InputStreamReader(fis4));
            String[] nextLine;
            reader.readNext();

            while ((nextLine = reader.readNext()) != null) {

                pays newCountry = new pays(Long.valueOf(nextLine[0]),
                    nextLine[1]);
                countries4.add(newCountry);
            }

        } catch (FileNotFoundException ex) {
            Logger.getLogger(nationaliteServiceImp.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(nationaliteServiceImp.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (fis4 != null) {
                    fis4.close();
                }
            } catch (IOException ex) {
                Logger.getLogger(nationaliteServiceImp.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return countries4;
    }

    //EXTRAIRE LES PAYS EN GEURRE:
    @Override
    public ArrayList<pays> findAllListePaysGeurre() {

        FileInputStream fis5 = null;

        try {

            String fileName = "src/main/resources/liste_pays/paysEnGuerre.csv";

            fis5 = new FileInputStream(new File(fileName));
            CSVReader reader = new CSVReader(new InputStreamReader(fis5));
            String[] nextLine;
            reader.readNext();

            while ((nextLine = reader.readNext()) != null) {

                pays newCountry = new pays(Long.valueOf(nextLine[0]),
                    nextLine[1]);
                countries5.add(newCountry);
            }

        } catch (FileNotFoundException ex) {
            Logger.getLogger(nationaliteServiceImp.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(nationaliteServiceImp.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (fis5 != null) {
                    fis5.close();
                }
            } catch (IOException ex) {
                Logger.getLogger(nationaliteServiceImp.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return countries5;
    }

    //EXTRAIRE LES PAYS Producteurs de drogue:
    @Override
    public   ArrayList<pays> findAllListePaysProducteursDrogue(){

        FileInputStream fis6 = null;

        try {

            String fileName = "src/main/resources/liste_pays/paysProducteursDrogue.csv";

            fis6 = new FileInputStream(new File(fileName));
            CSVReader reader = new CSVReader(new InputStreamReader(fis6));
            String[] nextLine;
            reader.readNext();

            while ((nextLine = reader.readNext()) != null) {

                pays newCountry = new pays(Long.valueOf(nextLine[0]),
                    nextLine[1]);
                countries6.add(newCountry);
            }

        } catch (FileNotFoundException ex) {
            Logger.getLogger(nationaliteServiceImp.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(nationaliteServiceImp.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (fis6!= null) {
                    fis6.close();
                }
            } catch (IOException ex) {
                Logger.getLogger(nationaliteServiceImp.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return countries6;
    }

}

