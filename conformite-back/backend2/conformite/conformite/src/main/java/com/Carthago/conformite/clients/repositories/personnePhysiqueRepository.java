package com.Carthago.conformite.clients.repositories;

import com.Carthago.conformite.clients.entities.personnePhysique;
import org.springframework.data.jpa.repository.JpaRepository;

public interface personnePhysiqueRepository extends JpaRepository<personnePhysique,Long> {
    Boolean existsByCodeClient(Long codeClient);
}
