package com.Carthago.conformite.risque.services;

import com.Carthago.conformite.clients.entities.personneMorale;
import com.Carthago.conformite.risque.entities.modeleGlobalPhysique;
import com.Carthago.conformite.risque.entities.modeleRisquePersonneMorale;
import com.Carthago.conformite.risque.entities.risque;
import com.Carthago.conformite.risque.entities.risquePersonneMorale;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.Optional;

public interface RisquePersonneMoraleService {

    int critereGeo(String x, String nat, Optional<modeleRisquePersonneMorale> m, long id);
    int risqueGeographique(long id,personneMorale P);
    void saveRisque(risquePersonneMorale R);
    int risqueNature (personneMorale p);
     int risqueSecteur(personneMorale p,Optional<modeleRisquePersonneMorale> m3,long id);

}
