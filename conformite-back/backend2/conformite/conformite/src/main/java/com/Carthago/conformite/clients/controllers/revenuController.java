package com.Carthago.conformite.clients.controllers;

import com.Carthago.conformite.clients.entities.revenu;
import com.Carthago.conformite.clients.services.revenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value="/revenu",method= {RequestMethod.GET})


public class revenuController {
    @Autowired
    revenuService RService;


    /**
     * Cette methode permet la creation du revenu
     */
    @PostMapping("/Create")
    public String createRevenu
    (@Validated @RequestBody revenu R)
    {
        RService.saveRevenu(R);
        return " cree";
    }


    /**
     * Cette methode de lister les revenus
     */
    @GetMapping("/GetAll")
    public List<revenu> getAllRevenuLegal()
    {
        return RService.listRevenu();

    }

    /**
     * Cette methode permet de trouver revenu par son id
     */
    @GetMapping("/Get/{id}")
    public Optional<revenu> findRevenu
    (@PathVariable Long id)
    {

        return RService.findRevenu(id);
    }

    /**
     * Cette methode permet de modifier revenu
     */
    @PutMapping("/Update/{id}")
    public String UpdateRevenu
    (@Validated @RequestBody revenu R,@PathVariable Long id)
    {
        RService.updateRevenu(id,R);
        return "La mise a jour a ete faite avec succees";
    }


    /**
     * Cette methode permet la suppression revenu par son id
     */
    @DeleteMapping(value="/Delete/{id}")
    public String deleteRevenu(@PathVariable String id)
    {
        RService.removeRevenu (Long.parseLong(id));
        return "supprimee avec succee";
    }
}
