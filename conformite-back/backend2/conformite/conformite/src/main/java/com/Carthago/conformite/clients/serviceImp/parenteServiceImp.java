package com.Carthago.conformite.clients.serviceImp;

import com.Carthago.conformite.clients.entities.parente;
import com.Carthago.conformite.clients.repositories.parenteRepository;
import com.Carthago.conformite.clients.services.parenteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service("parenteServiceImpl")
public class parenteServiceImp implements parenteService {

    @Autowired
    parenteRepository PRepository;


    /**
     * C'est l'implementation de la methode qui permet d'enregistrer cet objet
     */
    @Override
    public void saveParente(parente p) {
        PRepository.save(p);
    }


    /**
     * C'est l'implementation de la methode qui permet d'enregitrer les modifications exercées sur cet objet
     */
    @Override
    public void updateParente(long codeP,parente newParente) {
        parente parente;
        Optional<parente> OldParente=PRepository.findById(codeP);

        if(OldParente.isPresent())
        {
            parente=OldParente.get();
            PRepository.save(parente);
        }
    }

    /**
     * C'est l'implementation de la methode qui permet de lister tous ces objets
     */
    @Override
    public List<parente> listParente(){
        return PRepository.findAll();
    }

    /**
     * C'est l'implementation de la methode qui permet de supprimer cet objet
     */
    @Override
    public  void removeParente(long codeBe){
        PRepository.deleteById(codeBe);
    }

    /**
     * C'est l'implementation de la methode qui permet de trouver cet objet par son id
     */
    @Override
    public Optional<parente> findParente(long codeP){
        return PRepository.findById(codeP);
    }
}

