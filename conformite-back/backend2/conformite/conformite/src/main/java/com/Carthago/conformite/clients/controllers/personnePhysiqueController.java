package com.Carthago.conformite.clients.controllers;
import com.Carthago.conformite.clients.entities.personnePhysique;
import com.Carthago.conformite.clients.repositories.personneMoraleRepository;
import com.Carthago.conformite.clients.repositories.personnePhysiqueRepository;
import com.Carthago.conformite.clients.services.personnePhysiqueService;
import com.Carthago.conformite.fatca.services.fatcaService;
import com.Carthago.conformite.risque.entities.modeleCotationRisque;
import com.Carthago.conformite.risque.entities.modeleGlobalPhysique;
import com.Carthago.conformite.risque.entities.risque;
import com.Carthago.conformite.risque.services.modeleCotationRisqueService;
import com.Carthago.conformite.risque.services.modeleGlobalPhysiqueService;
import com.Carthago.conformite.risque.services.risqueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
@RestController
@RequestMapping(value="/personnePhysique",method= {RequestMethod.GET})
public class personnePhysiqueController {
    @Autowired
    personnePhysiqueService PService;
    @Autowired
    fatcaService fService;
    @Autowired
    risqueService R;
    @Autowired
    modeleGlobalPhysiqueService mservice ;
    @Autowired
    modeleCotationRisqueService modeleService;
    @Autowired
    personnePhysiqueRepository repository ;

    /**
     * Cette methode permet la creation de personne Physique
     */
    @PostMapping("/Create")
    public String createPersonnePhysique
    (@Validated @RequestBody personnePhysique P)
    {
        float res=R.Blacklist(P.getPrenom());

        PService.savePersonnePhysique(P);
        return "Personne Physique cree  "+res+"% ";
    }

    /**
     * Cette methode permet la creation de personne Physique
     */
    @PutMapping("/miseAjour")
    public String miseAjour
    (@Validated @RequestBody personnePhysique PP)
    {
        PService.updatePersonnePhysique(PP);
        return "Mise a jour d'un client ";
    }

    /**
     * Cette methode permet l existence de personne Physique
     */
    @GetMapping("/Exists/{id}")
    public Boolean personnePhysiqueExists (@PathVariable Long id){
        return repository.existsByCodeClient(id);
    }


    /**
     * Cette methode de lister les personne physique
     */
    @GetMapping("/GetAll")
    public List<personnePhysique> getAllPersonnePhysique()
    {  List<personnePhysique> l=PService.listPersonnePhysique();
        List<personnePhysique> p = new ArrayList<personnePhysique>();
        personnePhysique pers;
        for(int i=0;i<l.size();i++)
        { if(l.get(i).getStatutPersonne()!=null)
        {pers=new personnePhysique();
            pers.setCodeClient(l.get(i).getCodeClient());
            pers.setNom(l.get(i).getNom());
            pers.setPrenom((l.get(i).getPrenom()));
            pers.setStatutPersonne(l.get(i).getStatutPersonne());
            pers.setStatutFatca((l.get(i).getStatutFatca()));
            pers.setNiveauRisque(l.get(i).getNiveauRisque());
            pers.setRevue(l.get(i).getRevue());
            p.add(pers);}
        }
        return p;
    }
    /**
     * Cette methode permet de trouver Personne physique par son id
     */
    @GetMapping("/Get/{id}")
    public Optional<personnePhysique>findPersonnePhysique
    (@PathVariable Long id)
    {  return PService.findPersonnePhysique(id);
        /*Optional<personnePhysique> p= PService.findPersonnePhysique(id);
        Optional<personnePhysique> pers= Optional.of(new personnePhysique());
      pers.get().setCodeClient(p.get().getCodeClient());
      pers.get().setNom(p.get().getNom());
      pers.get().setPrenom(p.get().getPrenom());
      return pers;*/
    }
    /**
     * Cette methode permet de modifier Personne Physique
     */
    @PutMapping("/Update/{id}")
    public String UpdatePersonnePhysique
    (@Validated @RequestBody personnePhysique PP,@PathVariable Long id)
    {   Optional<modeleCotationRisque> modele = modeleService.findModeleCotationRisque((long) 1);
        PP.setCodeClient(id);
        PP.setStatutPersonne("Accepted");
        //detection statut fatca
        fService.detecterFatca(PP);
        //Calcul du risque :
        int n=R.calculRisque(PP);
        risque r=new risque();
        r.setCodeRisque(id);
        r.setContactRisque(PP);
        if(n<=modele.get().getNoteRisqueFaible())
        {
            r.setStatut("Faible");
            PP.setNiveauRisque("Faible");
            PP.setRevue("Chaque 2 ans");
            PP.setStatutPersonne("En attente de validation");
        }
        else if((n>modele.get().getNoteRisqueFaible()) &&  (n<=modele.get().getNoteRisqueMoyennementFaible())){
            r.setStatut("Moyennement Faible");
            PP.setNiveauRisque("Moyennement Faible");
            PP.setRevue("Chaque  1 ans");
            PP.setStatutPersonne("En attente de validation");
        }
        else if ((n>modele.get().getNoteRisqueMoyennementFaible()) &&  (n<=modele.get().getNoteRisqueMoyennementEleve()))
        {
            r.setStatut("Moyennement Elevé");
            PP.setNiveauRisque("Moyennement Elevé");
            PP.setRevue("Chaque  7 mois");
            PP.setStatutPersonne("En attente de validation");
        }
        else
        {
            r.setStatut("Elevé");
            PP.setNiveauRisque("Elevé");
            PP.setRevue("Chaque  3 mois");
            PP.setStatutPersonne("En attente de validation");
        }
        r.setValeur(n);
        R.saveRisque(r);
        Optional<modeleGlobalPhysique> m =mservice.find(id);
        if(m.isPresent())
        {m.get().setValeur(n);
            mservice.updateModeleGlobale(m.get());}
        r.setValeur(n);
        PService.updatePersonnePhysique(PP);
        return"success:Update a ete bien faite avec calcul du risque"+n;
    }

    /**
     * Cette methode permet la suppression Personne Physique par son id
     */
    @DeleteMapping(value="/Delete/{id}")
    public String deletePersonnePhysique(@PathVariable String id)
    {
        PService.removePersonnePhysique (Long.parseLong(id));
        return "personne Physique supprimee avec succee";
    }
    @GetMapping(value="/Fatca")
    public List<personnePhysique> findPersonnes()
    {
        List<personnePhysique> l =PService.listPersonnePhysique();
        List<personnePhysique> l1 = new ArrayList<>();
        for(int i=0;i<l.size();i++)
        {
            if ((l.get(i).getStatutFatca().equals(true)&&(l.get(i).getStatutPersonne().equalsIgnoreCase("Client"))))
            {
                l1.add(l.get(i));
            }
        }
        return l1;
    }
    @GetMapping("/RisqueEleve")
    List<personnePhysique> getClientsRisqueEleve()
    {List<personnePhysique> l=PService.listPersonnePhysique();
        List<personnePhysique> p = new ArrayList<personnePhysique>();
        for(int i=0;i<l.size();i++)
        {if(l.get(i).getStatutPersonne()!=null)
        {
            { if((l.get(i).getNiveauRisque().equalsIgnoreCase("Elevé")&&(l.get(i).getStatutPersonne().equalsIgnoreCase("Client"))))
                p.add(l.get(i));}
        }
        }
        return p;
    }
    @GetMapping("/RisqueMoyennementEleve")
    List<personnePhysique> getClientsRisqueMoyennementElevé()
    {List<personnePhysique> l=PService.listPersonnePhysique();
        List<personnePhysique> p = new ArrayList<personnePhysique>();
        for(int i=0;i<l.size();i++)
        { if(l.get(i).getStatutPersonne()!=null)
        { if((l.get(i).getNiveauRisque().equalsIgnoreCase("Moyennement Elevé")&&(l.get(i).getStatutPersonne().equalsIgnoreCase("Client"))))
            p.add(l.get(i));}
        }
        return p;
    }
    @GetMapping("/RisqueFaible")
    List<personnePhysique> getClientsRisqueFaible()
    {List<personnePhysique> l=PService.listPersonnePhysique();
        List<personnePhysique> p = new ArrayList<personnePhysique>();
        for(int i=0;i<l.size();i++)
        { if(l.get(i).getStatutPersonne()!=null)
        {if((l.get(i).getNiveauRisque().equalsIgnoreCase("Faible"))&&(l.get(i).getStatutPersonne().equalsIgnoreCase("Client")))
            p.add(l.get(i));}
        }
        return p;
    }
    @GetMapping("/RisqueFaiblementEleve")
    List<personnePhysique> getClientsRisqueFaiblementEleve()
    {List<personnePhysique> l=PService.listPersonnePhysique();
        List<personnePhysique> p = new ArrayList<personnePhysique>();
        for(int i=0;i<l.size();i++)
        { if(l.get(i).getStatutPersonne()!=null)
        {if((l.get(i).getNiveauRisque().equalsIgnoreCase("Moyennement Faible"))&&(l.get(i).getStatutPersonne().equalsIgnoreCase("Client")))
            p.add(l.get(i));}
        }
        return p;
    }
    /**
     * affiche les comptes à traiter par le conseiller client
     */
    @GetMapping(value="/DossiersRisqueFaible")
    public List<personnePhysique> getDossiersRisqueFaible ()
    {
        List<personnePhysique> l=PService.listPersonnePhysique();
        List<personnePhysique> p = new ArrayList<personnePhysique>();
        for(int i=0;i<l.size();i++)
        {if(l.get(i).getStatutPersonne()!=null)
        { if((l.get(i).getNiveauRisque().equalsIgnoreCase("Faible"))&&l.get(i).getStatutPersonne().equalsIgnoreCase("En attente de validation"))
            p.add(l.get(i)); }
        }
        return p;
    }

    /**
     * affiche les comptes à traiter par le responsable Conformite
     */
    @GetMapping(value="/DossiersRisqueMoyennementFaibleouMoyennementEleve")
    public List<personnePhysique> getDossiersRisqueMFME ()
    {
        List<personnePhysique> l=PService.listPersonnePhysique();
        List<personnePhysique> p = new ArrayList<personnePhysique>();
        for(int i=0;i<l.size();i++)
        {if(l.get(i).getStatutPersonne()!=null)
        { if((l.get(i).getNiveauRisque().equalsIgnoreCase("Moyennement Elevé"))||(l.get(i).getNiveauRisque().equalsIgnoreCase("Moyennement Faible")) &&(l.get(i).getStatutPersonne().equalsIgnoreCase("En attente de validation")))
            p.add(l.get(i)); }
        }
        return p;
    }

    /**
     * affiche les comptes à traiter par le chef d'agence
     */
    @GetMapping(value="/DossiersRisqueEleve")
    public List<personnePhysique> getDossiersRisqueE()
    {
        List<personnePhysique> l=PService.listPersonnePhysique();
        List<personnePhysique> p = new ArrayList<personnePhysique>();
        for(int i=0;i<l.size();i++)
        {if(l.get(i).getStatutPersonne()!=null)
        { if((l.get(i).getNiveauRisque().equalsIgnoreCase("Elevé"))&&(l.get(i).getStatutPersonne().equalsIgnoreCase("En attente de validation")))
            p.add(l.get(i)); }
        }
        return p;
    }

    /**
     * affiche les comptes à traiter par le chef d'agence
     */
    @GetMapping(value="/Dossiers")
    public List<personnePhysique> getDossiers()
    {
        List<personnePhysique> l=PService.listPersonnePhysique();
        List<personnePhysique> p = new ArrayList<personnePhysique>();
        for(int i=0;i<l.size();i++)
        {if(l.get(i).getStatutPersonne()!=null)
        { if(l.get(i).getStatutPersonne().equalsIgnoreCase("En attente de validation"))
            p.add(l.get(i)); }
        }
        return p;
    }


    /**
     * Cette methode permet de modifier Personne Physique
     */
    @PutMapping("/UpdateP")
    public String UpdatePersonnePhysique(@Validated @RequestBody personnePhysique PP)
    {
        PService.updateClient(PP);
        return "hbiba ye dhiba";
    }


    /**
     * Cette methode permet de modifier Personne Physique
     */
    @PutMapping("/MAJ/{id}")
    public String MAJPersonnePhysique
    (@Validated @RequestBody personnePhysique PP,@PathVariable Long id)
    {   Optional<modeleCotationRisque> modele = modeleService.findModeleCotationRisque((long) 1);
        PP.setCodeClient(id);
        PP.setStatutPersonne("Accepted");
        //detection statut fatca
        fService.detecterFatca(PP);
        //Calcul du risque :
        int n=R.calculRisque(PP);
        risque r=new risque();
        r.setCodeRisque(id);
        r.setContactRisque(PP);
        if(n<=modele.get().getNoteRisqueFaible())
        {
            r.setStatut("Faible");
            PP.setNiveauRisque("Faible");
            PP.setRevue("Chaque 2 ans");
            PP.setStatutPersonne("Client");
        }
        else if((n>modele.get().getNoteRisqueFaible()) &&  (n<=modele.get().getNoteRisqueMoyennementFaible())){
            r.setStatut("Moyennement Faible");
            PP.setNiveauRisque("Moyennement Faible");
            PP.setRevue("Chaque  1 ans");
            PP.setStatutPersonne("Client");
        }
        else if ((n>modele.get().getNoteRisqueMoyennementFaible()) &&  (n<=modele.get().getNoteRisqueMoyennementEleve()))
        {
            r.setStatut("Moyennement Elevé");
            PP.setNiveauRisque("Moyennement Elevé");
            PP.setRevue("Chaque  7 mois");
            PP.setStatutPersonne("Client");
        }
        else
        {
            r.setStatut("Elevé");
            PP.setNiveauRisque("Elevé");
            PP.setRevue("Chaque  3 mois");
            PP.setStatutPersonne("Client");
        }
        r.setValeur(n);
        R.saveRisque(r);
        Optional<modeleGlobalPhysique> m =mservice.find(id);
        if(m.isPresent())
        {m.get().setValeur(n);
            mservice.updateModeleGlobale(m.get());}
        r.setValeur(n);
        PService.MAJPersonnePhysique(PP);
        return"success:Update a ete bien faite avec calcul du risque"+n;
    }








}






















