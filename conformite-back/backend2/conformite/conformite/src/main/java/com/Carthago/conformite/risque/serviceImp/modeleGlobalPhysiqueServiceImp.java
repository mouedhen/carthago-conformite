package com.Carthago.conformite.risque.serviceImp;

import com.Carthago.conformite.risque.entities.modeleGlobalPhysique;
import com.Carthago.conformite.risque.repositories.modeleGlobalPhysiqueRepository;
import com.Carthago.conformite.risque.services.modeleGlobalPhysiqueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service("ServiceModeleGlobal")
public class modeleGlobalPhysiqueServiceImp implements modeleGlobalPhysiqueService {


    @Autowired
    modeleGlobalPhysiqueRepository model;


    @Override
    public void saveModeleGobale(modeleGlobalPhysique par) {
        model.save(par);
    }

    @Override
    public void updateModeleGlobale(modeleGlobalPhysique par) {
        model.save(par);
    }

    @Override
    public Optional<modeleGlobalPhysique> find(Long id)
    {
       return model.findById(id) ;
    }

}
