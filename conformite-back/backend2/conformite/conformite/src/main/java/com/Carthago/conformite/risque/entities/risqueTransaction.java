package com.Carthago.conformite.risque.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
public class risqueTransaction {
    //declaration des variables
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long num;
    private Boolean compteInactif=false;  //Activite du compte
    private  Boolean montantMaximal=false;
    private Boolean paysBlackList=false;
    private Boolean personneE=false;
    private Boolean PersonneD=false;
    private Boolean paysSecteurSuspect=false;
}
