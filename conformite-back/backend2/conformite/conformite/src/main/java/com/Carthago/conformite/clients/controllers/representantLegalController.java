package com.Carthago.conformite.clients.controllers;

import com.Carthago.conformite.clients.entities.representantLegal;
import com.Carthago.conformite.clients.services.RepPersService;
import com.Carthago.conformite.clients.services.representantLegalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value="/representantLegal",method= {RequestMethod.GET})


public class representantLegalController {

    @Autowired
    representantLegalService RService;

    @Autowired
    RepPersService repService;


    /**
     * Cette methode permet la creation du representant Legal
     */
    @PostMapping("/Create")
    public String createRepresentantLegal
    (@Validated @RequestBody representantLegal R)
    {
        RService.saveRepresentantLegal(R);
        return " cree";
    }


    /**
     * Cette methode de lister les representants Legals
     */
    @GetMapping("/GetAll")
    public List<representantLegal> getAllRepresentantLegal()
    {
        return RService.listRepresentantLegal();

    }

    /**
     * Cette methode permet de trouver representant Legal par son id
     */
    @GetMapping("/Get/{id}")
    public Optional<representantLegal> findRepresentantLegal
    (@PathVariable Long id)
    {

        Optional<representantLegal> R= RService.findRepresentantLegal(id);
        Optional<representantLegal> pers= Optional.of(new representantLegal());
        pers.get().setCodeClient(R.get().getCodeClient());
        pers.get().setPaysResidence(R.get().getPaysResidence());
        pers.get().setNom(R.get().getNom());
        pers.get().setPrenom(R.get().getPrenom());
        pers.get().setDateNaissance(R.get().getDateNaissance());
        pers.get().setFonctionOrganisme(R.get().getFonctionOrganisme());
        return pers;
    }
    /**
     * fonction qui retourne les representants d'une personne morale
     *
     * @return
     */
    @GetMapping("/GetRepresentant/{id}")
    public Optional<representantLegal> findRepresentantByCodeClient
    (@PathVariable Long id)
    { Optional<representantLegal> r= Optional.of(new representantLegal());
        ArrayList<Long> l =repService.listRepPersCode(id);
      for(int i=0;i<l.size();i++)
      { r=findRepresentantLegal(l.get(i));}
      return r ;
    }

    /**
     * Cette methode permet de modifier representant Legal
     */
    @PutMapping("/Update/{id}")
    public String UpdateRepresentantLegal
    (@Validated @RequestBody representantLegal R,@PathVariable Long id)
    {
        RService.updateRepresentantLegal(id,R);
        return "La mise a jour a ete faite avec succees";
    }


    /**
     * Cette methode permet la suppression representant Legal par son id
     */
    @DeleteMapping(value="/Delete/{id}")
    public String deleteRepresentantLegal(@PathVariable String id)
    {
        RService.removeRepresentantLegal (Long.parseLong(id));
        return "supprimee avec succee";
    }
}

