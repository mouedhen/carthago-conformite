package com.Carthago.conformite.risque.serviceImp;

import com.Carthago.conformite.clients.entities.nationalite;
import com.Carthago.conformite.clients.entities.personnePhysique;
import com.Carthago.conformite.clients.serviceImp.nationaliteServiceImp;
import com.Carthago.conformite.clients.services.nationaliteService;
import com.Carthago.conformite.clients.services.persNatService;
import com.Carthago.conformite.pep.entities.pep;
import com.Carthago.conformite.pep.services.pepService;
import com.Carthago.conformite.risque.entities.*;
import com.Carthago.conformite.risque.repositories.risqueRepository;
import com.Carthago.conformite.risque.services.*;
import com.opencsv.CSVReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

@Service("risqueServiceImpl")
public class risqueFatcaServiceImp implements risqueService {

    @Autowired
    risqueRepository ParRepository;

    @Autowired
    paysService na;

    @Autowired
    secteurService s;

    @Autowired
    pepService pe;

    @Autowired
    persNatService pns;

    @Autowired
    modeleCotationRisqueService modeleService ;

    @Autowired
    modeleGlobalPhysiqueService model;

    @Autowired
    nationaliteService nationalite;

    private final ArrayList<personneBlackliste> blacklistePersonne;

    public risqueFatcaServiceImp(ArrayList<personneBlackliste> blacklistePersonne) {
        this.blacklistePersonne = blacklistePersonne;
    }

    @Override
    public void saveRisque(risque Par) {
        ParRepository.save(Par);
    }
    @Override
    public void updateRisque(risque new_Par) {
        ParRepository.save(new_Par);
    }
    @Override
    public List<risque> listRisque(){
        return ParRepository.findAll();
    }
    @Override
    public void removeRisque(long CodeC){
        ParRepository.deleteById(CodeC);
    }
    @Override
    public Optional<risque> findRisque(long CodeC){
        return ParRepository.findById(CodeC);

    }

    //fonctionne qui calcule le risque pour une personne physique :
    @Override
    public int calculRisque(personnePhysique p){
        int r=0,res;
        String chaine;
        float resultat;
        modeleGlobalPhysique m3;
        m3=new modeleGlobalPhysique();
        m3.setCode(p.getCodeClient());
        m3.setContactModelRisque(p);
        Optional<modeleCotationRisque> modele = modeleService.findModeleCotationRisque((long) 1);
        //personne Blacklisté
        chaine=p.getPrenom()+" "+p.getNom();
        resultat=Blacklist(chaine);
        if(resultat>70){
            r=r+modele.get().getNoteBlackListePP();
            m3.setNom(chaine);}
        //LES CRITERES DE RISQUE POUR PAYSNAIS /PAYSRESIDENCE / PAYSTRAVAIL
        r=r+critereGeo(" pays naissance : ",p.getPaysNaissance(),m3);
        r=r+critereGeo(" pays travail : ",p.getPaysTravail(),m3);
        r=r+critereGeo(" pays residence : ",p.getPaysResidence(),m3);
        //trouver le risque des pays PEP:
      /*  if(p.getStatutPep()==true)
        { Optional<pep> t=pe.findPep(p.getCodeClient());
            r=r+critereGeo(" pays pep :",t.get().getPaysPep(),m3); }*/

        if(p.getStatutPep()==true)
        {
            r=r+3; }
        //trouver le risque des nationalites :
        ArrayList<Long> liste_nat=pns.listPNCode(p.getCodeClient());
        res=critereGeoNat(liste_nat,m3);
        //trouver le risque secteur / profession /relatif au statut client :
        r=r+risqueSecteur(p,m3);
        r=r+risqueProfession(p,m3);
        r=r+risqueRelatifStatutClient(p,m3);
        r=r+res;
        m3.setValeur(r);
        return r;
    }

    //fonctionne qui calcule le risque d'un critere geographique
    @Override
    public  int critereGeo(String x,String nat,modeleGlobalPhysique m)
    {
        int note_p=0;
        Boolean t=false,u,o,y;
        int i = 0,j,f,k,v;
        String ch=" ",ch1=" ",ch2=" ",ch3=" ",ch4=" ",ch5=" ";
        List<pays> l=na.findAllListeGrise();
        List<pays> l1=na.findAllListeNoire();
        List<pays> l2=na.findAllListePaysGeurre();
        List<pays> l4=na.findAllListeEconomieInformelle();
        List<pays> l5=na.findAllListePaysProducteursDrogue();
        ArrayList<listeCorruption> l3=na.findAllListeCorruption();
        Optional<modeleCotationRisque> modele = modeleService.findModeleCotationRisque((long) 1);
        while (i < l.size() && t == false) {
            if (l.get(i).getPays().equalsIgnoreCase(nat)) {
                t = true;
                note_p = note_p + modele.get().getNoteListeGrise();
                if(m.getListeGrise()!=null)
                {ch=m.getListeGrise();}
                ch=ch+x+l.get(i).getPays();
                m.setListeGrise(ch); }
            else { i++; } }
        f = 0;
        y = false;
        while ( f < l1.size() && y==false) {
            if(l1.get(f).getPays().equalsIgnoreCase(nat))
            {   y=true;
                note_p=note_p+modele.get().getNoteListeNoire();
                if(m.getListeNoire()!=null)
                {ch1=m.getListeNoire();}
                ch1=ch1+x+l1.get(f).getPays();
                m.setListeNoire(ch1); }
            else{ f++; } }
        t=false;
        i=0;
        while ( i < l2.size() && t==false) {
            if(l2.get(i).getPays().equalsIgnoreCase(nat))
            { t=true;
                if(m.getListeGeurre()!=null)
                { ch2=m.getListeGeurre();}
                note_p=note_p+modele.get().getNotePaysGuerre();
                ch2=ch2+x+l2.get(i).getPays();
                m.setListeGeurre(ch2); }
            else{ i++; } }
        t=false;
        i=0;
        while ( i < l3.size() && t==false) {
            if((l3.get(i).getPays().equalsIgnoreCase(nat))&&(l3.get(i).getRang()>70))
            {   t=true;
                note_p=note_p+modele.get().getNotePaysCorruption();
                if(m.getListeCorruption()!=null)
                {ch3=m.getListeCorruption();}
                ch3=ch3+x+l3.get(i).getPays();
                m.setListeCorruption(ch3); }
            else{
                i++;
            } }
        u = false;
        k = 0;
        while ( k < l4.size() && u==false) {
            if(l4.get(k).getPays().equalsIgnoreCase(nat))
            { u=true;
                note_p=note_p+modele.get().getNotePaysEconomieParallele();
                if(m.getListeEconomieParallele()!=null)
                {ch4=m.getListeEconomieParallele();}
                ch4=ch4+x+l4.get(k).getPays();
                m.setListeEconomieParallele(ch4); }
            else{ k++; } }
        o = false;
        v = 0;
        while ( v < l5.size() && o==false) {
            if(l5.get(v).getPays().equalsIgnoreCase(nat))
            {   o=true;
                note_p=note_p+modele.get().getNotePaysDrogues();
                if(m.getListeDrogue()!=null)
                {ch5=m.getListeDrogue();}
                ch5=ch5+x+l5.get(v).getPays();
                m.setListeDrogue(ch5); }
            else{
                v++; }}
        int valeur= m.getValeur();
        m.setValeur(valeur+note_p);
        model.saveModeleGobale(m);
        return note_p;
    }




    //fonctionne qui calcule le risque d'un critere geographique pour les nationnalites
    @Override
    public  int critereGeoNat(ArrayList<Long> liste_nat,modeleGlobalPhysique m)
    {int note_p=0;

        for (int j=0;j<liste_nat.size();j++) {

            long num=liste_nat.get(j);
            Optional<nationalite> n=nationalite.findNationalite(num);
            String nat=n.get().getNationalite();
            note_p=note_p+critereGeo(" nationalite :",nat,m); }

        return note_p;
    }

    /**
     * calcule le risque par secteur
     * @param p
     * @param m
     * @return
     */
    @Override
    public int risqueSecteur(personnePhysique p ,modeleGlobalPhysique m){
        int note_p=0;
        Boolean t=false;
        int i = 0;
        List<secteur> l=s.findAllListeSecteur();
        Optional<modeleCotationRisque> modele = modeleService.findModeleCotationRisque((long) 1);
        while ( i < l.size() && t==false) {
            String ch=l.get(i).getNom();
            if(ch.equalsIgnoreCase(p.getSecteurTravail()))
            {
                if((ch.equalsIgnoreCase("Import-Export"))||(ch.equalsIgnoreCase("Casinos"))
                        ||(ch.equalsIgnoreCase("Etablissement de jeux de hasard"))||(ch.equalsIgnoreCase("Clubs VIP")))
                {t=true;
                    note_p=note_p+modele.get().getNoteSecteur();}
                if((ch.equalsIgnoreCase("Industrie petro-chimique"))||(ch.equalsIgnoreCase("Transport maritime"))||(ch.equalsIgnoreCase("Petrole"))||(ch.equalsIgnoreCase("Chimie-Parachimie"))||(ch.equalsIgnoreCase("Arment")))
                {t=true;
                    note_p=note_p+modele.get().getNoteSecteur1();}
                if((ch.equalsIgnoreCase("Commerce"))||(ch.equalsIgnoreCase("Automobile-Reparation automobiles"))||(ch.equalsIgnoreCase("Immobilier"))||(ch.equalsIgnoreCase("Relations d affaire avec etats ou gouvernements etrangers"))||(ch.equalsIgnoreCase("Bureaux de change")))
                {t=true;
                    note_p=note_p+modele.get().getNoteSecteur2();}
                if((ch.equalsIgnoreCase("Objets d art et antiquite"))||(ch.equalsIgnoreCase("Informatique-Telecoms"))||(ch.equalsIgnoreCase("Metaux et pierres precieuse"))||(ch.equalsIgnoreCase("BTP-Materiaux de construction")))
                {t=true;
                    note_p=note_p+modele.get().getNoteSecteur3();}
                m.setSecteur(p.getSecteurTravail());
                model.saveModeleGobale(m);
            }
            else{
                i++;
            }
        }
        return note_p;
    }

    @Override
    public int risqueProfession(personnePhysique p,modeleGlobalPhysique m){
        int note_p=0;
        Boolean t=false;
        int i = 0;
        List<secteur> l=s.findAllListeProfession();
        Optional<modeleCotationRisque> modele = modeleService.findModeleCotationRisque((long) 1);
        while ( i < l.size() && t==false) {
            String ch=l.get(i).getNom();
            if(ch.equalsIgnoreCase(p.getProfession()))
            {
                if((ch.equalsIgnoreCase("Agent du location"))||(ch.equalsIgnoreCase("Agent immobilier"))
                        ||(ch.equalsIgnoreCase("Commercant")))
                {t=true;
                    note_p=note_p+modele.get().getNoteProfession();}
                if((ch.equalsIgnoreCase("Casinos"))||(ch.equalsIgnoreCase("Tresorier d une association"))||(ch.equalsIgnoreCase("President d une association"))||(ch.equalsIgnoreCase("Vendeurs et revendeur de bateaux")))
                {t=true;
                    note_p=note_p+modele.get().getNoteProfession1();}
                if((ch.equalsIgnoreCase("Homme d affaire"))||(ch.equalsIgnoreCase("Femme d affaire")))
                {t=true;
                    note_p=note_p+modele.get().getNoteProfession2();;}
                if((ch.equalsIgnoreCase("Agent de joueurs"))||(ch.equalsIgnoreCase("Sans activité")))
                {t=true;
                    note_p=note_p+modele.get().getNoteProfession3();}
                m.setProfession(p.getProfession());
                model.saveModeleGobale(m);
            }
            else{
                i++;
            }
        }
        return note_p;
    }

    //autre risque
    @Override
    public int risqueRelatifStatutClient(personnePhysique PP,modeleGlobalPhysique m) {
        int n = 0;
        Optional<modeleCotationRisque> modele = modeleService.findModeleCotationRisque((long) 1);
        if(PP.getStatutPep()==true) { n=n+modele.get().getNotePepPersonnePhysique();
            m.setStatut("Cette personne est politiquement exposée.");}
        if(!PP.getPaysResidence().equals("Tunisie")){ n=n+modele.get().getResidence(); m.setResidence("Cette personne n'est pas résident en Tunisie.");}
        if(PP.getIntermediaire()==true){n=n+modele.get().getIntermediaire(); }
        model.saveModeleGobale(m);
        return n;}


    //EXTRAIRE LA LISTE DES PERSONNES AU LISTE NOIRE
    @Override
    public ArrayList<personneBlackliste> findBlackListe(){

        FileInputStream fis1 = null;

        try {

            String fileName = "src/main/resources/liste_pays/blackList_personne.csv";

            fis1 = new FileInputStream(new File(fileName));
            CSVReader reader = new CSVReader(new InputStreamReader(fis1));
            String[] nextLine;
            reader.readNext();

            while ((nextLine = reader.readNext()) != null) {

                personneBlackliste newCountry = new personneBlackliste(Long.valueOf(nextLine[0]),
                        nextLine[1]);
                blacklistePersonne.add(newCountry);
            }

        } catch (FileNotFoundException ex) {
            Logger.getLogger(nationaliteServiceImp.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(nationaliteServiceImp.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (fis1 != null) {
                    fis1.close();
                }
            } catch (IOException ex) {
                Logger.getLogger(nationaliteServiceImp.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return blacklistePersonne;
    }
    @Override
    public float  Blacklist(String P) {
        float res=0 ,nbre=0,v,v1=0;
        List<personneBlackliste> l=findBlackListe();
        for (int i = 0; i<l.size(); i++)
        {   v=0;
            v =calculate(P,l.get(i).getNomPrenom());
            if((i==0)||(v<v1)){
                v1=v;
               if(P.length()>l.get(i).getNomPrenom().length()){nbre=P.length();}
               else{nbre=l.get(i).getNomPrenom().length();}
            }
        }
        res=100-((v1/nbre)*100);
        return res;
    }

    static int calculate(String x, String y) {
        int[][] dp = new int[x.length() + 1][y.length() + 1];

        for (int i = 0; i <= x.length(); i++) {
            for (int j = 0; j <= y.length(); j++) {
                if (i == 0) {
                    dp[i][j] = j;
                }
                else if (j == 0) {
                    dp[i][j] = i;
                }
                else {
                    dp[i][j] = min(dp[i - 1][j - 1]
                                    + costOfSubstitution(x.charAt(i - 1), y.charAt(j - 1)),
                            dp[i - 1][j] + 1,
                            dp[i][j - 1] + 1);
                }
            }
        }

        return dp[x.length()][y.length()];
    }

    public static int costOfSubstitution(char a, char b) {
        return a == b ? 0 : 1;
    }

    public static int min(int... numbers) {
        return Arrays.stream(numbers)
                .min().orElse(Integer.MAX_VALUE);
    }



}

