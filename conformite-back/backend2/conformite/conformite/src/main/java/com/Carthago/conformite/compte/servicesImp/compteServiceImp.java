package com.Carthago.conformite.compte.servicesImp;

import com.Carthago.conformite.compte.entities.compte;
import com.Carthago.conformite.compte.entities.compteEpargne;
import com.Carthago.conformite.compte.repositories.compteRepository;
import com.Carthago.conformite.compte.services.compteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


@Service("compteserviceImpl")
public class compteServiceImp implements compteService {
    @Autowired
    compteRepository PRepository;


    /**
     * C'est l'implementation de la methode qui permet d'enregistrer cet objet
     */
    @Override
    public void saveCompte(compte p) {

        long f=PRepository.count()+1;
        String ch1=" ",ch=" ";
        p.setDateOuverture(java.time.LocalDate.now());
        ch=String.valueOf(f);
        PRepository.save(p);
    }


    /**
     * C'est l'implementation de la methode qui permet d'enregitrer les modifications exercées sur cet objet
     */
    @Override
    public void updateCompte(long codeP,compte newParente) {
        compte parente;
        Optional<compte> OldParente=PRepository.findById(codeP);

        if(OldParente.isPresent())
        {
            parente=OldParente.get();
            PRepository.save(parente);
        }
    }

    /**
     * C'est l'implementation de la methode qui permet de lister tous ces objets
     */
    @Override
    public List<compte> listCompte(){
        return PRepository.findAll();
    }

    /**
     * C'est l'implementation de la methode qui permet de supprimer cet objet
     */
    @Override
    public  void removeCompte(long codeBe){
        PRepository.deleteById(codeBe);
    }

    /**
     * C'est l'implementation de la methode qui permet de trouver cet objet par son id
     */
    @Override
    public compte findCompte(String codeP){
        compte c = new compte();
        int i=0;
        List<compte>l=PRepository.findAll();
        while (l.size()>i)
        { if(l.get(i).getRIB().equalsIgnoreCase(codeP))
            {   c=l.get(i);
                return c; }
            else {
                i++;
            } }
        return c;
    }
    /**
     * C'est l'implementation de la methode qui permet de trouver cet objet par son id
     * @return
     */
    public Optional<compte> findCompteCode(long id) {
       return PRepository.findById(id);
    }

    @Override
    public void crediter (compte c , double montant)
    {

        c.setDateOuverture(java.time.LocalDate.now());

        c.setSolde(c.getSolde()+montant);
        PRepository.save(c);
    }
    @Override
    public void debiter (compte c , double montant)
    {
        c.setDateOuverture(java.time.LocalDate.now());

        c.setSolde(c.getSolde()-montant);
        PRepository.save(c);
    }
}
