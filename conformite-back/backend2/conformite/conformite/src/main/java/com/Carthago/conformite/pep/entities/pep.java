package com.Carthago.conformite.pep.entities;


import com.Carthago.conformite.clients.entities.contact;
import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.sql.Date;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
public class pep {

    //declaration des variables ainsi du id
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private  Long codePep;
    private String fonctionPep=""; //fonction de la personne PE
    private String paysPep=""; //Pays ou de la personne est PE
    private Date dateDebut; //Date debut du poste
    private Date dateFin; //Date fin

    //getters et setters

    public Long getCodePep() {
        return codePep;
    }

    public void setCodePep(Long codePep) {
        this.codePep = codePep;
    }

    public String getFonctionPep() {
        return fonctionPep;
    }

    public void setFonctionPep(String fonctionPep) {
        this.fonctionPep = fonctionPep;
    }

    public String getPaysPep() {
        return paysPep;
    }

    public void setPaysPep(String paysPep) {
        this.paysPep = paysPep;
    }

    public Date getDateDebut() {
        return dateDebut;
    }

    public void setDateDebut(Date dateDebut) {
        this.dateDebut = dateDebut;
    }

    public Date getDateFin() {
        return dateFin;
    }

    public void setDateFin(Date dateFin) {
        this.dateFin = dateFin;
    }

    public contact getContact() {
        return contact;
    }

    public void setContact(contact contact) {
        this.contact = contact;
    }





    //Relations
    @OneToOne
    @JoinColumn(name="codeClient")
    @JsonBackReference
    private contact contact ;
}
