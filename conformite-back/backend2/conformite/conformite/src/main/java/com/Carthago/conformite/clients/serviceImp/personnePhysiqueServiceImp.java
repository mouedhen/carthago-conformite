package com.Carthago.conformite.clients.serviceImp;

import com.Carthago.conformite.clients.entities.personnePhysique;
import com.Carthago.conformite.clients.repositories.personnePhysiqueRepository;
import com.Carthago.conformite.clients.services.personnePhysiqueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service("personnePhysiqueServiceImpl")

public class personnePhysiqueServiceImp implements personnePhysiqueService {

    @Autowired
    personnePhysiqueRepository PPRepository;


    /**
     * C'est l'implementation de la methode qui permet d'enregistrer cet objet
     */
    @Override
    public void savePersonnePhysique(personnePhysique PP) {
        PPRepository.save(PP);
    }


    /**
     * C'est l'implementation de la methode qui permet
     * d'enregitrer les modifications exercées sur cet objet
     */
    @Override
    public void updatePersonnePhysique
    (personnePhysique personnePhysique) {
        Optional<personnePhysique> oldPersonneMorale = PPRepository.findById(personnePhysique.getCodeClient());
        personnePhysique.setListPersNat1(oldPersonneMorale.get().getListPersNat1());
        personnePhysique.setListPersPar(oldPersonneMorale.get().getListPersPar());
        personnePhysique.setListRevenu(oldPersonneMorale.get().getListRevenu());
        personnePhysique.setListCompte(oldPersonneMorale.get().getListCompte());
        PPRepository.save(personnePhysique);
    }

    /**
     * C'est l'implementation de la methode qui permet de lister tous ces objets
     */
    @Override
    public List<personnePhysique> listPersonnePhysique(){
        return PPRepository.findAll();
    }

    /**
     * C'est l'implementation de la methode qui permet de supprimer cet objet
     */
    @Override
    public  void removePersonnePhysique(long CodeC){
        PPRepository.deleteById(CodeC);
    }

    /**
     * C'est l'implementation de la methode qui permet de trouver cet objet par son id
     */
    @Override
    public Optional<personnePhysique> findPersonnePhysique(long CodeC){
        return PPRepository.findById(CodeC);
    }

    @Override
    public personnePhysique findPersonne(long codeClient) {
        personnePhysique p=new personnePhysique();
        Optional<personnePhysique> p1= PPRepository.findById(codeClient);
        p.setCodeClient(p1.get().getCodeClient());
        p.setSecteurTravail(p1.get().getSecteurTravail());
        p.setProfession(p1.get().getProfession());
        p.setStatutPep(p1.get().getStatutPep());
        return p;
    }


    @Override
    public void updateStatutClient(long id)
    {Optional<personnePhysique> oldPersonneMorale=PPRepository.findById(id);
        Optional<personnePhysique> P=PPRepository.findById(id);
       // P.get().setNiveauRisque(oldPersonneMorale.get().getNiveauRisque());
        P.get().setRevue(oldPersonneMorale.get().getRevue());
        P.get().setStatutFatca(oldPersonneMorale.get().getStatutFatca());
        P.get().setAdresse(oldPersonneMorale.get().getAdresse());
        P.get().setCodeTin(oldPersonneMorale.get().isCodeTin());
        P.get().setEmail(oldPersonneMorale.get().getEmail());
        P.get().setFatca(oldPersonneMorale.get().getFatca());
        P.get().setNumTelephone(oldPersonneMorale.get().getNumTelephone());
        P.get().setPaysResidence(oldPersonneMorale.get().getPaysResidence());
        P.get().setPep(oldPersonneMorale.get().getPep());
        P.get().setNom(oldPersonneMorale.get().getNom());
        P.get().setPrenom(oldPersonneMorale.get().getPrenom());
        P.get().setListPersNat1(oldPersonneMorale.get().getListPersNat1());
        P.get().setListPersPar(oldPersonneMorale.get().getListPersPar());
        P.get().setListRevenu(oldPersonneMorale.get().getListRevenu());
        P.get().setListCompte(oldPersonneMorale.get().getListCompte());
        P.get().setNumCin(oldPersonneMorale.get().getNumCin());
        P.get().setNumAff(oldPersonneMorale.get().getNumAff());
        P.get().setNumeroRne(oldPersonneMorale.get().getNumeroRne());
        P.get().setNumPassport(oldPersonneMorale.get().getNumPassport());
        P.get().setDateNaissance(oldPersonneMorale.get().getDateNaissance());
        P.get().setPaysNaissance(oldPersonneMorale.get().getPaysNaissance());
        P.get().setStatutFatca(oldPersonneMorale.get().getStatutFatca());
        P.get().setStatutPep(oldPersonneMorale.get().getStatutPep());
        P.get().setSecteurTravail(oldPersonneMorale.get().getSecteurTravail());
        P.get().setProfession(oldPersonneMorale.get().getProfession());
        P.get().setStatutPersonne("Client");
        P.get().setCodeClient(oldPersonneMorale.get().getCodeClient());
        P.get().setCarteSejour(oldPersonneMorale.get().getCarteSejour());
        P.get().setCapaciteJuridique(oldPersonneMorale.get().getCapaciteJuridique());
        P.get().setCarteVerte(oldPersonneMorale.get().getCarteVerte());
        P.get().setCatSocioprofessionnelle(oldPersonneMorale.get().getCatSocioprofessionnelle());
        P.get().setCodeDouane(oldPersonneMorale.get().getCodeDouane());
        P.get().setContratTravail(oldPersonneMorale.get().getContratTravail());
        P.get().setIndiTelephonique(oldPersonneMorale.get().getIndiTelephonique());
        P.get().setIntermediaire(oldPersonneMorale.get().getIntermediaire());
        P.get().setDateExtraitRne(oldPersonneMorale.get().getDateExtraitRne());
        P.get().setEtatCivil(oldPersonneMorale.get().getEtatCivil());
        P.get().setMatriculeFiscale(oldPersonneMorale.get().getMatriculeFiscale());
        P.get().setRegimeFiscal(oldPersonneMorale.get().getRegimeFiscal());
        P.get().setParentePep(oldPersonneMorale.get().getParentePep());
        P.get().setAffSociale(oldPersonneMorale.get().getAffSociale());
        P.get().setNumAff(oldPersonneMorale.get().getNumAff());
        P.get().setSexe(oldPersonneMorale.get().getSexe());
        P.get().setNumeroRne(oldPersonneMorale.get().getNumeroRne());

    }









































    @Override
    public void updateClient(personnePhysique PP) {
        Optional<personnePhysique> oldPersonneMorale=PPRepository.findById(PP.getCodeClient());
        if(!oldPersonneMorale.get().getNiveauRisque().isEmpty())
        { PP.setNiveauRisque(oldPersonneMorale.get().getNiveauRisque());}
        PP.setRevue(oldPersonneMorale.get().getRevue());
       /* P.get().setStatutFatca(oldPersonneMorale.get().getStatutFatca());
        P.get().setAdresse(oldPersonneMorale.get().getAdresse());
        P.get().setCodeTin(oldPersonneMorale.get().isCodeTin());
        P.get().setEmail(oldPersonneMorale.get().getEmail());
        P.get().setFatca(oldPersonneMorale.get().getFatca());
        P.get().setNumTelephone(oldPersonneMorale.get().getNumTelephone());
        P.get().setPaysResidence(oldPersonneMorale.get().getPaysResidence());
        P.get().setPep(oldPersonneMorale.get().getPep());
        P.get().setNom(oldPersonneMorale.get().getNom());
        P.get().setPrenom(oldPersonneMorale.get().getPrenom());
        P.get().setListPersNat1(oldPersonneMorale.get().getListPersNat1());
        P.get().setListPersPar(oldPersonneMorale.get().getListPersPar());
        P.get().setListRevenu(oldPersonneMorale.get().getListRevenu());
        P.get().setNumCin(oldPersonneMorale.get().getNumCin());
        P.get().setNumAff(oldPersonneMorale.get().getNumAff());
        P.get().setNumeroRne(oldPersonneMorale.get().getNumeroRne());
        P.get().setNumPassport(oldPersonneMorale.get().getNumPassport());
        P.get().setDateNaissance(oldPersonneMorale.get().getDateNaissance());
        P.get().setPaysNaissance(oldPersonneMorale.get().getPaysNaissance());
        P.get().setStatutFatca(oldPersonneMorale.get().getStatutFatca());
        P.get().setStatutPep(oldPersonneMorale.get().getStatutPep());
        P.get().setSecteurTravail(oldPersonneMorale.get().getSecteurTravail());
        P.get().setProfession(oldPersonneMorale.get().getProfession());
        P.get().setStatutPersonne("Client");
        P.get().setCodeClient(oldPersonneMorale.get().getCodeClient());
        P.get().setCarteSejour(oldPersonneMorale.get().getCarteSejour());
        P.get().setCapaciteJuridique(oldPersonneMorale.get().getCapaciteJuridique());
        P.get().setCarteVerte(oldPersonneMorale.get().getCarteVerte());
        P.get().setCatSocioprofessionnelle(oldPersonneMorale.get().getCatSocioprofessionnelle());
        P.get().setCodeDouane(oldPersonneMorale.get().getCodeDouane());
        P.get().setContratTravail(oldPersonneMorale.get().getContratTravail());
        P.get().setIndiTelephonique(oldPersonneMorale.get().getIndiTelephonique());
        P.get().setIntermediaire(oldPersonneMorale.get().getIntermediaire());
        P.get().setDateExpirationCin(oldPersonneMorale.get().getDateExpirationCin());
        P.get().setDateExpirationPassport(oldPersonneMorale.get().getDateExpirationPassport());
        P.get().setDateExpirationCarteSejour(oldPersonneMorale.get().getDateExpirationCarteSejour());
        P.get().setDateExtraitRne(oldPersonneMorale.get().getDateExtraitRne());
        P.get().setDateLivraisonCarteSejour(oldPersonneMorale.get().getDateLivraisonCarteSejour());
        P.get().setDateLivraisonCin(oldPersonneMorale.get().getDateLivraisonCin());
        P.get().setDateLivraisonPassport(oldPersonneMorale.get().getDateLivraisonPassport());
        P.get().setEtatCivil(oldPersonneMorale.get().getEtatCivil());
        P.get().setMatriculeFiscaleP(oldPersonneMorale.get().getMatriculeFiscaleP());
        P.get().setRegimeFiscal(oldPersonneMorale.get().getRegimeFiscal());
        P.get().setParentePep(oldPersonneMorale.get().getParentePep());
        P.get().setAffSociale(oldPersonneMorale.get().getAffSociale());
        P.get().setNumAff(oldPersonneMorale.get().getNumAff());
        P.get().setSexe(oldPersonneMorale.get().getSexe()); */
        PPRepository.save(PP);



    }


    @Override
    public void MAJPersonnePhysique
            (personnePhysique personnePhysique) {
        Optional<personnePhysique> oldPersonneMorale = PPRepository.findById(personnePhysique.getCodeClient());
        personnePhysique.setListPersNat1(oldPersonneMorale.get().getListPersNat1());

        personnePhysique.setListCompte(oldPersonneMorale.get().getListCompte());
        PPRepository.save(personnePhysique);
    }














}

