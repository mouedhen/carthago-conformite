package com.Carthago.conformite.springsecurity.repositories;

import java.util.Optional;

import com.Carthago.conformite.springsecurity.entities.ERole;
import com.Carthago.conformite.springsecurity.entities.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;



@Repository
public interface RoleRepository extends JpaRepository<Role,Long> {
    Optional<Role> findByName(ERole name);
}


