package com.Carthago.conformite.clients.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
public class stat {

    private float valeur;
    private String name;

    public stat() {
    }

    public stat(float valeur, String name) {
        this.valeur = valeur;
        this.name = name;
    }

    public float getValeur() {
        return valeur;
    }

    public void setValeur(float valeur) {
        this.valeur = valeur;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
