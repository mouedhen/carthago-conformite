package com.Carthago.conformite.clients.serviceImp;

import com.Carthago.conformite.clients.entities.nationalite;
import com.Carthago.conformite.clients.repositories.nationaliteRepository;
import com.Carthago.conformite.clients.services.nationaliteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service("nationaliteServiceImpl")
public class nationaliteServiceImp implements nationaliteService {

    @Autowired
    nationaliteRepository NRepository;


    /**
     * C'est l'implementation de la methode qui permet d'enregistrer cet objet
     */
    @Override
    public void saveNationalite(nationalite N) {
        NRepository.save(N);
    }


    /**
     * C'est l'implementation de la methode qui permet
     * d'enregitrer les modifications exercées sur cet objet
     */
    @Override
    public void updateNationalite(long codeNat,nationalite newNationalite) {
        nationalite nationalite;
        Optional<nationalite> OldNationalite=NRepository.findById(codeNat);


        if(OldNationalite.isPresent())
        {
            nationalite=OldNationalite.get();
            NRepository.save(nationalite);
        }
    }

    /**
     * C'est l'implementation de la methode qui permet de lister tous ces objets
     */
    @Override
    public List<nationalite> listNationalite(){
        return NRepository.findAll();
    }

    /**
     * C'est l'implementation de la methode qui permet de supprimer cet objet
     */
    @Override
    public  void removeNationalite(long codeNat){
        NRepository.deleteById(codeNat);
    }

    /**
     * C'est l'implementation de la methode qui permet de trouver cet objet par son id
     */
    @Override
    public Optional<nationalite> findNationalite(long codeNat){
        return NRepository.findById(codeNat);
    }
}

