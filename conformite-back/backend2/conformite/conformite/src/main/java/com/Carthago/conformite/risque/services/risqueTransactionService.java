package com.Carthago.conformite.risque.services;


import com.Carthago.conformite.risque.entities.risqueTransaction;

import java.util.Optional;

public interface risqueTransactionService {
    void saveRisque(risqueTransaction Nat);
    risqueTransaction find();
    Optional <risqueTransaction> findTransaction (Long id);
}
