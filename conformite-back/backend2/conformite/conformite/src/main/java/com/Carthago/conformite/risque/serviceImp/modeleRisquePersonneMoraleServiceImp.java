package com.Carthago.conformite.risque.serviceImp;


import com.Carthago.conformite.risque.entities.modeleRisquePersonneMorale;
import com.Carthago.conformite.risque.repositories.modeleRisquePersonneMoraleRepository;
import com.Carthago.conformite.risque.services.modeleRisquePersonneMoraleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service("ServiceModeleRisquePersonneMorale")
public class modeleRisquePersonneMoraleServiceImp implements modeleRisquePersonneMoraleService {

    @Autowired
    modeleRisquePersonneMoraleRepository r ;

    @Override
    public void save (modeleRisquePersonneMorale P)
    {
       r.save(P);
    }


    @Override
    public Optional<modeleRisquePersonneMorale> find (long id)
    {
        return r.findById(id);
    }

    @Override
    public void update(modeleRisquePersonneMorale m,long id)
    {
        modeleRisquePersonneMorale modeleRisquePersonneMorale;
        Optional<modeleRisquePersonneMorale> OldModeleRisquePersonneMorale=r.findById(id);


        if(OldModeleRisquePersonneMorale.isPresent())
        {
            modeleRisquePersonneMorale=OldModeleRisquePersonneMorale.get();
            r.save(modeleRisquePersonneMorale);
        }
    }
    }

