package com.Carthago.conformite.compte.entities;

import com.Carthago.conformite.clients.entities.contact;
import com.Carthago.conformite.clients.entities.personnePhysique;
import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.*;

import javax.persistence.*;
import java.sql.Date;
import java.text.DateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class transaction {


    //declaration des variables
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long codeTransaction ; //identifiant de la transaction
    private String typeTransaction=""; //type de la transaction : credit, debit
    private String statutTransaction=""; //statut de la transaction : achevée ou bloquée
    private double montant;
    private LocalDate dateTransaction; //date de la transaction
    private String nomEmetteur=""; //nom du l'emetteur
    private String ribEmetteur="";//numéro compte de l'emetteur
    private String nomDestinataire="";//nom du Destinataire
    private String paysDestinataire="";//pays du Destinataire
    private String ribDestinataire=""; //RIB du Destinataire
    private String motif="";//justificatif
    private String nomBanque=""; //nom du banque
    //constructeurs:

    public transaction(long codeTransaction) {
        this.codeTransaction = codeTransaction;
    }

    public transaction(compte compteTransaction) {
        this.compteTransaction = compteTransaction;
    }

    //relation entre transaction et compte
    @ManyToOne
    @JsonBackReference
    @JoinColumn(name="codeTrans")
    private compte compteTransaction;





}