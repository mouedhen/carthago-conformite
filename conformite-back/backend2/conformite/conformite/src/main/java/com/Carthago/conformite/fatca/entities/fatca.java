package com.Carthago.conformite.fatca.entities;

import com.Carthago.conformite.clients.entities.contact;
import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
public class fatca {

    @Id
    private Long codeFatca;
    private boolean statut=false;
    private String statutP="";//statut fatca de la personne morale
    @Lob
    private byte[] file;
    private String nom;
    private String dateCreation;
    private String typeContenu;

    public fatca(byte[] file, String nom, String dateCreation, String typeContenu) {
        this.file = file;
        this.nom = nom;
        this.dateCreation = dateCreation;
        this.typeContenu = typeContenu;
    }

    //Relations
    @OneToOne
    @JoinColumn(name = "codeClient1")
    @JsonBackReference
    private contact contact1;



}
