package com.Carthago.conformite.clients.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
public class revenu {

    //declaration des variables
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private  Long codeRevenu; //code Revenu

    private  String nature=""; //nature des autres revenus
    private  float montant;//montant de ce revenu
    private  String devise=""; //devise de ce revenu
    private String periodicite=""; //periodicite de ce revenu

    //getters et setters


    public Long getCodeRevenu() {
        return codeRevenu;
    }

    public void setCodeRevenu(Long codeRevenu) {
        this.codeRevenu = codeRevenu;
    }

    public String getNature() {
        return nature;
    }

    public void setNature(String nature) {
        this.nature = nature;
    }

    public float getMontant() {
        return montant;
    }

    public void setMontant(float montant) {
        this.montant = montant;
    }

    public String getDevise() {
        return devise;
    }

    public void setDevise(String devise) {
        this.devise = devise;
    }

    public String getPeriodicite() {
        return periodicite;
    }

    public void setPeriodicite(String periodicite) {
        this.periodicite = periodicite;
    }

    public personnePhysique getPersPhy() {
        return persPhy;
    }

    public void setPersPhy(personnePhysique persPhy) {
        this.persPhy = persPhy;
    }

    //constructeur
    public revenu(personnePhysique persPhy) {
        this.persPhy = persPhy;
    }


    //Relation many to one  entre revenu et personne physique
    @JsonBackReference
    @ManyToOne
    @JoinColumn(name="codePers")
    private personnePhysique persPhy;

}
