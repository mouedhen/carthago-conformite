package com.Carthago.conformite.clients.services;

import com.Carthago.conformite.clients.entities.personneMorale;

import java.util.List;
import java.util.Optional;

public interface personneMoraleService {
    /**
     * Cette Methode permet d'enregistrer une personne morale.
     */
    void savePersonneMorale(personneMorale PM);


    /**
     * Cette Methode permet d'enregistrer les modifications exercées sur une personne morale.
     */
    void updatePersonneMorale(long codeClient,personneMorale PM);


    /**
     * Cette Methode permet de lister toutes les personnes morales.
     */
    List<personneMorale> listPersonneMorale();


    /**
     * Cette Methode permet de supprimer une personne morale.
     */
    void removePersonneMorale(long codeClient);


    /**
     * Cette Methode permet de trouver une personne morale par son CodeClient.
     */
    public Optional<personneMorale> findPersonneMorale(long codeClient);

    /**
     * mettre à jour le statut du client
     */
     void updateStatutClient(long id);
}
