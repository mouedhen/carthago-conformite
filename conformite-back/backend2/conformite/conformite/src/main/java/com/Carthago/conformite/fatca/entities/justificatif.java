package com.Carthago.conformite.fatca.entities;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@AllArgsConstructor
@NoArgsConstructor
@Entity
public class justificatif {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idJustificatif;
    @Lob
    private byte[] file;
    private String nom;
    private String dateCreation;
    private String typeContenu;

    public justificatif(byte[] file, String nom, String dateCreation, String typeContenu) {
        this.file = file;
        this.nom = nom;
        this.dateCreation = dateCreation;
        this.typeContenu = typeContenu;
    }
}
