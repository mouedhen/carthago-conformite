package com.Carthago.conformite.risque.services;

import com.Carthago.conformite.risque.entities.modeleRisquePersonneMorale;
import java.util.Optional;

public interface modeleRisquePersonneMoraleService {

    void save (modeleRisquePersonneMorale M);
    Optional<modeleRisquePersonneMorale> find (long id);
    void update (modeleRisquePersonneMorale m,long id);
}
