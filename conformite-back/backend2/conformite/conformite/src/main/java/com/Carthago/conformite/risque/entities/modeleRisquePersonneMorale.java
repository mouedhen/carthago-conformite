package com.Carthago.conformite.risque.entities;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
public class modeleRisquePersonneMorale {

    //declaration des variables
    @Id
    private Long code ;
    private String nature="";//prend une valeur lorsque la nature de la PM est sus
    private String pub="";
    private String offshore="";
    private String paysPro="";
    private String marcheBoursier="";
    private String accPpe="";
    private String ong="";
    private String assOffshore="";
    private String listeGrise="";
    private String listeNoire="";
    private String listeEconomieParallele="";
    private String listeDrogue="";
    private String listeGeurre="";
    private String listeCorruption="";
    private String secteur="";
    private String profession="";
    private int valeur;
    private String chiffreAffaires="";
    private String blackListe="";
    private int nbreTransactionFrauduleuse;





}
