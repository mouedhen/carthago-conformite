package com.Carthago.conformite.clients.repositories;

import com.Carthago.conformite.clients.entities.personneMorale;
import org.springframework.data.jpa.repository.JpaRepository;

public interface personneMoraleRepository extends JpaRepository<personneMorale,Long> {

    Boolean existsByCodeClient(Long codeClient);
}
