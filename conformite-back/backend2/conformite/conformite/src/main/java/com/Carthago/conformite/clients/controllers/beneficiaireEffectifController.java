package com.Carthago.conformite.clients.controllers;
import com.Carthago.conformite.clients.entities.beneficiaireEffectif;
import com.Carthago.conformite.clients.services.beneficiaireEffectifService;
import com.Carthago.conformite.clients.services.persBeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
@RestController
@RequestMapping(value="/beneficiaireEffectif",method= {RequestMethod.GET})
public class beneficiaireEffectifController  {
    @Autowired
    beneficiaireEffectifService BEService;
    @Autowired
    persBeService service;
    /**
     * Cette methode permet la creation du beneficiaire effectif
     */
    @PostMapping("/Create")
    public String createBeneficiaireEffectif
    (@Validated @RequestBody beneficiaireEffectif BE)
    {
        BEService.saveBeneficiaireEffectif(BE);
        return "BeneficiaireEffectif cree";
    }
    /**
     * Cette methode de lister les beneficiaires effectifs
     */
    @GetMapping("/GetAll")
    public List<beneficiaireEffectif> getAllBeneficiaireEffectif()
    {
        return BEService.listBeneficiaireEffectif();
    }
    /**
     * Cette methode permet de trouver un beneficiaire effectif par son id
     */
    @GetMapping("/Get/{id}")
    public Optional<beneficiaireEffectif> findBeneficiaireEffectif
    (@PathVariable Long id)
    {
        return BEService.findBeneficiaireEffectif(id);
    }
    /**
     * Cette methode permet de modifier un beneficiaire effectif
     */
    @PutMapping("/Update/{id}")
    public String UpdateBeneficiaireEffectif
    (@Validated @RequestBody beneficiaireEffectif BF, @PathVariable Long id)
    {
        BEService.updateBeneficiaireEffectif(id,BF);
        return"La mise a jour a ete faite avec succees";
    }
    /**
     * Cette methode permet la suppression d'un beneficiaire effectif par son id
     */
    @DeleteMapping(value="/Delete/{id}")
    public String deleteBeneficiaireEffectif(@PathVariable String id)
    {
        BEService.removeBeneficiaireEffectif(Long.parseLong(id));
        return "BeneficiaireEffectif supprimee avec succee";
    }
    /**
     * fonction qui retourne les beneficiaires d'une personne morale
     *
     * @return
     */
    @GetMapping("/GetBeneficiaire/{id}")
    public Optional<beneficiaireEffectif> findBeneficiaireEffectif1ByCodeClient
    (@PathVariable Long id)
    { Optional<beneficiaireEffectif> r= Optional.of(new beneficiaireEffectif());
        ArrayList<Long> l =service.listPersBeCode(id);
        r=findBeneficiaireEffectif(l.get(0));
        return r ;
    }
    /**
     * fonction qui retourne les beneficiaires d'une personne morale
     *
     * @return
     */
    @GetMapping("/GetBeneficiaire1/{id}")
    public Optional<beneficiaireEffectif> findBeneficiaireEffectif2ByCodeClient
    (@PathVariable Long id)
    { Optional<beneficiaireEffectif> r= Optional.of(new beneficiaireEffectif());
        ArrayList<Long> l =service.listPersBeCode(id);
        r=findBeneficiaireEffectif(l.get(1));
        return r ;
    }
    /**
     * fonction qui retourne les beneficiaires d'une personne morale
     *
     * @return
     */
    @GetMapping("/GetBeneficiaire2/{id}")
    public Optional<beneficiaireEffectif> findBeneficiaireEffectif3ByCodeClient
    (@PathVariable Long id)
    { Optional<beneficiaireEffectif> r= Optional.of(new beneficiaireEffectif());
        ArrayList<Long> l =service.listPersBeCode(id);
        r=findBeneficiaireEffectif(l.get(2));
        return r ;
    }
    /**
     * fonction qui retourne les beneficiaires d'une personne morale
     *
     * @return
     */
    @GetMapping("/GetBeneficiaire3/{id}")
    public Optional<beneficiaireEffectif> findBeneficiaireEffectifByCodeClient4
    (@PathVariable Long id)
    {  Optional<beneficiaireEffectif> r= Optional.of(new beneficiaireEffectif());
        ArrayList<Long> l =service.listPersBeCode(id);
        r=findBeneficiaireEffectif(l.get(3));
        return r ;
    }
}
