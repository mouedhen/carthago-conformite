package com.Carthago.conformite.compte.repositories;

import com.Carthago.conformite.compte.entities.transaction;
import org.springframework.data.jpa.repository.JpaRepository;

public interface transactionRepository extends JpaRepository<transaction,Long> {
}
