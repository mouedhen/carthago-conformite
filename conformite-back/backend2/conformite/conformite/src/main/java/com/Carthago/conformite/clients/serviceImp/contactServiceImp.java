package com.Carthago.conformite.clients.serviceImp;
import com.Carthago.conformite.clients.entities.beneficiaireEffectif;
import com.Carthago.conformite.clients.entities.contact;
import com.Carthago.conformite.clients.repositories.contactRepository;
import com.Carthago.conformite.clients.services.contactService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;

@Service("contactService")
public class contactServiceImp implements contactService {
    @Autowired
    contactRepository  repository;
    @Override
    public List<contact> listClients(){
        return repository.findAll();
    }
    /**
     * C'est l'implementation de la methode qui permet de supprimer cet objet
     */
    @Override
    public  void removeContact(long CodeC){
        repository.deleteById(CodeC);
    }

    @Override
    public Optional<contact> findPersonne(long codeClient) {
        return repository.findById(codeClient);
    }
}
