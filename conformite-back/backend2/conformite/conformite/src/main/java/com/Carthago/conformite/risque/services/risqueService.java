package com.Carthago.conformite.risque.services;

import com.Carthago.conformite.clients.entities.personnePhysique;
import com.Carthago.conformite.risque.entities.modeleGlobalPhysique;
import com.Carthago.conformite.risque.entities.pays;
import com.Carthago.conformite.risque.entities.personneBlackliste;
import com.Carthago.conformite.risque.entities.risque;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public interface risqueService {
    void saveRisque(risque Nat);
    void updateRisque(risque Nat);
    List<risque> listRisque();
    void removeRisque(long CodeC);
    Optional<risque> findRisque(long CodeC);
     int critereGeo(String x,String nat,modeleGlobalPhysique m);
    int calculRisque(personnePhysique p);
    int critereGeoNat(ArrayList<Long> liste_nat, modeleGlobalPhysique m);
    int risqueSecteur(personnePhysique p,modeleGlobalPhysique m);
    int risqueProfession(personnePhysique p,modeleGlobalPhysique m);
    int risqueRelatifStatutClient(personnePhysique PP,modeleGlobalPhysique m);
    ArrayList<personneBlackliste> findBlackListe();
    float Blacklist(String ch);

}
