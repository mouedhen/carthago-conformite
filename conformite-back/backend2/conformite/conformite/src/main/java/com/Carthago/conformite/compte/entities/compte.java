package com.Carthago.conformite.compte.entities;

import com.Carthago.conformite.clients.entities.contact;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.sql.Date;
import java.time.LocalDate;
import java.util.List;

@AllArgsConstructor
@Getter
@Setter
@Entity
public class compte {
    //declaration des variables
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long numCompte; //NUMERO DE COMPTE
    private double solde;  //SOLDE DU COMPTE
    private LocalDate dateOuverture; //DATE OUVERTURE D UNE COMPTE
    private String RIB; //RIB du client
    private  String nomAgence;//le nom de l agence
    public compte() { }

    //getters and setters

    public double getSolde() {
        return solde;
    }

    public void setSolde(double solde) {
        this.solde = solde;
    }

    public String getRIB() {
        return RIB;
    }

    public void setRIB(String RIB) {
        this.RIB = RIB;
    }

    public Long getId() {
        return numCompte;
    }

    public void setId(Long id) {
        this.numCompte = id;
    }

    public LocalDate getDateOuverture() {
        return dateOuverture;
    }

    public void setDateOuverture(LocalDate dateOuverture) {
        this.dateOuverture = dateOuverture;
    }

    //CONSTRUCTEURS
    public compte(Long id) {
        this.numCompte = id;
    }

    public compte(contact contactCompte) {
        this.contactCompte = contactCompte;
    }

    //Relation entre contact et compte
    @ManyToOne
    @JoinColumn(name="codePers")
    private contact contactCompte;

    //relation entre transaction et compte
    @OneToMany(mappedBy = "compteTransaction", cascade = CascadeType.ALL)
    private List<transaction> listTransaction;
}
