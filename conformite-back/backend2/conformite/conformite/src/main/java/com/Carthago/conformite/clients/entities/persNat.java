package com.Carthago.conformite.clients.entities;
import com.fasterxml.jackson.annotation.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;


@AllArgsConstructor

@Getter
@Setter
@Entity

public class persNat implements Serializable {

      //declaration des variables
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        private  Long codeNatPers; //Code

        //Getters and setters
        public Long getCodeNatPers() {
            return codeNatPers;
        }

        public void setCodeNatPers(Long codeNatPers) {
            this.codeNatPers = codeNatPers;
        }

        public personnePhysique getPers1() {
        return pers1;
             }
        public void setPers1(personnePhysique pers1) {
        this.pers1 = pers1;
         }
        public nationalite getNat() {
        return nat;
           }
        public void setNat(nationalite nat) {
        this.nat = nat;
             }

    //constructeur


    public persNat( nationalite nat) {

        this.nat= nat;
    }

    public persNat() {
        super();
    }


    // many to one  entre pers_nat et personne physique
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @ManyToOne
    @JoinColumn(name="Code_pers")
    private personnePhysique pers1;


    //many to one  entre pers_nat et nationalite
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @ManyToOne
    @JoinColumn(name="Code_nationalite")
    private nationalite nat;










}


