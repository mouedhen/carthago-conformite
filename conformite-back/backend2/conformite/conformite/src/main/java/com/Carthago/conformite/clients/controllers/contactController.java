package com.Carthago.conformite.clients.controllers;

import com.Carthago.conformite.clients.entities.contact;
import com.Carthago.conformite.clients.entities.personnePhysique;
import com.Carthago.conformite.clients.entities.stat;
import com.Carthago.conformite.clients.services.contactService;
import com.Carthago.conformite.clients.services.personneMoraleService;
import com.Carthago.conformite.clients.services.personnePhysiqueService;
import com.Carthago.conformite.compte.entities.compte;
import com.Carthago.conformite.compte.services.compteService;
import com.Carthago.conformite.fatca.repositories.fatcaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value="/contact",method= {RequestMethod.GET})
public class contactController {
    @Autowired
    contactService service;
    @Autowired
    personnePhysiqueService PService;
    @Autowired
    personneMoraleService MService;
    @Autowired
    compteService CService;
    @Autowired
    fatcaRepository FRepository;

    @GetMapping("/GetAllCompte")
    public int getClientTotalCMP()
    {   return CService.listCompte().size();
    }
    @GetMapping("/GetCaisse")
    public double getCaisse()
    {List<compte> l=CService.listCompte();
      double c=0;
        for(int i=0;i<l.size();i++)
        {

         c=c+l.get(i).getSolde();
        }
        return c;
    }

    @GetMapping("/GetAll")
    List<contact> getClients()
    {List<contact> l=service.listClients();
        List<contact> p = new ArrayList<contact>();
        for(int i=0;i<l.size();i++)
        { if((l.get(i).getStatutPersonne()!=null)&&(l.get(i).getStatutPersonne().equalsIgnoreCase("Client")))
            p.add(l.get(i));
        }
        return p;
    }
    @GetMapping("/GetAllFatca")
    List<stat> getClientsFatca()
    {List<contact> l=service.listClients();
        List<stat> p = new ArrayList<stat>();
        float c1=0,c2=0,d1=0,d2=0;
     c1=  getClients().size();
     c2=FRepository.findAll().size();
     d1=(c2*100)/c2;
     d2=100-d1;
        stat e=new stat();stat e1=new stat();
        e.setValeur(d1);e.setName("Statut Fatca Actif");
        e1.setValeur(d2);e1.setName("Statut Fatca Non Actif");
        p.add(e);p.add(e1);
        return p;
    }

    /**
     * Cette methode permet de trouver Personne par son id
     */
    @GetMapping("/Get/{id}")
    public Optional<contact> findContact
    (@PathVariable Long id)
    {  return service.findPersonne(id); }

    @GetMapping("/RisqueEleve")
    List<contact> getClientsRisqueEleve()
    {List<contact> l=service.listClients();
        List<contact> p = new ArrayList<contact>();
        for(int i=0;i<l.size();i++)
        { if(l.get(i).getNiveauRisque().equalsIgnoreCase("Elevé"))
            p.add(l.get(i));
        }
        return p;
    }
    @GetMapping("/RisqueMoyennementEleve")
    List<contact> getClientsRisqueMoyennementElevé()
    {List<contact> l=service.listClients();
        List<contact> p = new ArrayList<contact>();
        for(int i=0;i<l.size();i++)
        { if(l.get(i).getNiveauRisque().equalsIgnoreCase("Moyennement Elevé"))
            p.add(l.get(i));
        }
        return p;
    }
    @GetMapping("/RisqueFaible")
    List<contact> getClientsRisqueFaible()
    {List<contact> l=service.listClients();
        List<contact> p = new ArrayList<contact>();
        for(int i=0;i<l.size();i++)
        { if(l.get(i).getNiveauRisque().equalsIgnoreCase("Faible"))
            p.add(l.get(i));
        }
        return p;
    }
    @GetMapping("/RisqueFaiblementEleve")
    List<contact> getClientsRisqueFaiblementEleve()
    {List<contact> l=service.listClients();
        List<contact> p = new ArrayList<contact>();
        for(int i=0;i<l.size();i++)
        { if(l.get(i).getNiveauRisque().equalsIgnoreCase("Faiblement Elevé"))
            p.add(l.get(i));
        }
        return p;
    }
    /**
     * Cette methode permet la suppression d'un contact par son id
     */
    @DeleteMapping(value="/Delete/{id}")
    public String deleteContact(@PathVariable String id)
    {
        service.removeContact(Long.parseLong(id));
        return "supprimee avec succee";
    }

    @GetMapping("/GetAlltotal")
    public int getClientTotal()
    {List<contact> l=service.listClients();

        int c=0;
        for(int i=0;i<l.size();i++)
        { if(l.get(i).getStatutPersonne()!=null)
        {  if(l.get(i).getStatutPersonne().equalsIgnoreCase("Client"))
        { c=c+1;

        }}}
    return c;
    }


    @GetMapping("/GetAllStatus")
   public List<stat> getClientStatus()
    {List<contact> l=service.listClients();
        List<stat> p = new ArrayList<stat>();
        int c=0,c1=0,c2=0,c3=0;
        for(int i=0;i<l.size();i++)
        { if(l.get(i).getStatutPersonne()!=null)
        {  if(l.get(i).getStatutPersonne().equalsIgnoreCase("Client"))
        { if(l.get(i).getNiveauRisque().equalsIgnoreCase("Faible")){ c++;}
            if(l.get(i).getNiveauRisque().equalsIgnoreCase("Moyennement Faible")){ c1++;}
            if(l.get(i).getNiveauRisque().equalsIgnoreCase("Elevé")){ c2++;}
            if(l.get(i).getNiveauRisque().equalsIgnoreCase("Moyennement Elevé")){ c3++;}
        }
        }}
        stat e=new stat();stat e1=new stat();stat e2=new stat();stat e3=new stat();
        e.setValeur(c);e.setName("Faible");
        e1.setValeur(c1);e1.setName("Moyennement Faible");
        e2.setValeur(c2);e2.setName("Elevé");
        e3.setValeur(c3);e3.setName("Moyennement Elevé");
        p.add(e);p.add(e1);p.add(e2);p.add(e3);
        return p;
    }
    @GetMapping("/nbre")
    List<stat> getClientsnbre()
    {
    List<stat> p = new ArrayList<stat>();
        stat e=new stat();stat e1=new stat();stat e2=new stat();stat e3=new stat();
        e.setValeur(PService.listPersonnePhysique().size());e.setName("Client Physique");
        e1.setValeur(MService.listPersonneMorale().size());e1.setName("Client Morale");
        p.add(e);p.add(e1);
        return p;
    }
}