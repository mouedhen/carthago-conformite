package com.Carthago.conformite.risque.services;

import com.Carthago.conformite.risque.entities.listeCorruption;
import com.Carthago.conformite.risque.entities.pays;

import java.util.ArrayList;

public interface paysService {
    ArrayList<pays> findAllListeGrise();
    ArrayList<pays> findAllListeNoire();
    ArrayList<listeCorruption> findAllListeCorruption();
    ArrayList<pays> findAllListeEconomieInformelle();
    ArrayList<pays> findAllListePaysGeurre();
    ArrayList<pays> findAllListePaysProducteursDrogue();
}
