package com.Carthago.conformite.compte.servicesImp;

import com.Carthago.conformite.clients.entities.parente;
import com.Carthago.conformite.clients.repositories.parenteRepository;
import com.Carthago.conformite.clients.services.personneMoraleService;
import com.Carthago.conformite.clients.services.personnePhysiqueService;
import com.Carthago.conformite.compte.entities.compteCourant;
import com.Carthago.conformite.compte.entities.compteEpargne;
import com.Carthago.conformite.compte.repositories.compteCourantRepository;
import com.Carthago.conformite.compte.services.compteCourantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service("compteCourantserviceImpl")
public class compteCourantServiceImp implements compteCourantService {

    @Autowired
    compteCourantRepository PRepository;

    @Autowired
    personneMoraleService service ;
    @Autowired
    personnePhysiqueService Pservice;

    /**
     * C'est l'implementation de la methode qui permet d'enregistrer cet objet
     */
    @Override
    public void saveCompteCourantClientMoral(compteCourant p, long id  ) {
        p.setDateOuverture(java.time.LocalDate.now());
        service.updateStatutClient(id);
        PRepository.save(p);
        p.setRIB(p.getNumCompte().toString());
        PRepository.save(p);
    }
    /**
     * C'est l'implementation de la methode qui permet d'enregistrer cet objet
     */
    @Override
    public void saveCompteCourantClientPhysique(compteCourant p, long id  ) {
        p.setDateOuverture(java.time.LocalDate.now());
        Pservice.updateStatutClient(id);
        PRepository.save(p);
        p.setRIB(p.getNumCompte().toString());
        PRepository.save(p);
    }


    /**
     * C'est l'implementation de la methode qui permet d'enregitrer les modifications exercées sur cet objet
     */
    @Override
    public void updateCompteCourant(long codeP,compteCourant newParente) {
        compteCourant parente;
        Optional<compteCourant> OldParente=PRepository.findById(codeP);

        if(OldParente.isPresent())
        {
            parente=OldParente.get();
            PRepository.save(parente);
        }
    }

    /**
     * C'est l'implementation de la methode qui permet de lister tous ces objets
     */
    @Override
    public List<compteCourant> listCompteCourant(){
        return PRepository.findAll();
    }

    /**
     * C'est l'implementation de la methode qui permet de supprimer cet objet
     */
    @Override
    public  void removeCompteCourant(long codeBe){
        PRepository.deleteById(codeBe);
    }

    /**
     * C'est l'implementation de la methode qui permet de trouver cet objet par son id
     */
    @Override
    public compteCourant findCompteCourant(String codeP){

        compteCourant c = new compteCourant();
        int i=0;
        List<compteCourant>l=PRepository.findAll();
        while (l.size()>i)
        {
            if(l.get(i).getRIB().equals(codeP))
            {   c=l.get(i);
                return c;
            }
            else {
                i++;
            }
        }
        return c;
    }

    /**
     * l'implementation de la méthode credit
     */
    @Override
    public void crediterCourant (compteCourant c , double montant)
    {
        c.setSolde(c.getSolde()-montant);
    }
    @Override
    public void debiterCourant (compteCourant c , double montant)
    {
        c.setSolde(c.getSolde()+montant);
    }
}
