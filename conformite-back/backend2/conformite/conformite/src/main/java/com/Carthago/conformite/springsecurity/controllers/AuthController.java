package com.Carthago.conformite.springsecurity.controllers;


import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.validation.Valid;

import com.Carthago.conformite.springsecurity.entities.ERole;
import com.Carthago.conformite.springsecurity.entities.Role;
import com.Carthago.conformite.springsecurity.entities.User;
import com.Carthago.conformite.springsecurity.payload.request.LoginRequest;
import com.Carthago.conformite.springsecurity.payload.request.SignupRequest;
import com.Carthago.conformite.springsecurity.payload.response.JwtResponse;
import com.Carthago.conformite.springsecurity.payload.response.MessageResponse;
import com.Carthago.conformite.springsecurity.repositories.RoleRepository;
import com.Carthago.conformite.springsecurity.repositories.UserRepository;
import com.Carthago.conformite.springsecurity.security.jwt.JwtUtils;
import com.Carthago.conformite.springsecurity.security.services.UserDetailsImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/auth")
public class AuthController {
    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    PasswordEncoder encoder;

    @Autowired
    JwtUtils jwtUtils;

    @GetMapping("/findUserByUsername/{username}")
    public Optional<User> findUserByUsername(@PathVariable String username)
    {
        return userRepository.findUserByUsername(username);
    }

    @PostMapping("/signin")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));

        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtUtils.generateJwtToken(authentication);

        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        List<String> roles = userDetails.getAuthorities().stream()
                .map(item -> item.getAuthority())
                .collect(Collectors.toList());

        return ResponseEntity.ok(new JwtResponse(jwt,
                userDetails.getId(),
                userDetails.getUsername(),
                userDetails.getEmail(),
                roles));
    }

    @PostMapping("/signup")
    public ResponseEntity<?> registerUser(@Valid @RequestBody SignupRequest signUpRequest) {
        if (userRepository.existsByUsername(signUpRequest.getUsername())) {
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: Username is already taken!"));
        }

        if (userRepository.existsByEmail(signUpRequest.getEmail())) {
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: Email is already in use!"));
        }

        // Create new user's account
        User user = new User(signUpRequest.getUsername(),
                signUpRequest.getEmail(),
                encoder.encode(signUpRequest.getPassword()));

        Set<String> strRoles = signUpRequest.getRole();
        Set<Role> roles = new HashSet<>();

        if (strRoles == null) {
            Role userRole = roleRepository.findByName(ERole.ROLE_USER)
                    .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
            roles.add(userRole);
        } else {
            strRoles.forEach(role -> {
                switch (role) {
                    case "admin":
                        Role adminRole = roleRepository.findByName(ERole.ROLE_ADMIN)
                                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                        roles.add(adminRole);

                        break;
                    case "cons":
                        Role chefRole = roleRepository.findByName(ERole.CONSEILLER_CLIENTS)
                                .orElseThrow(() -> new RuntimeException("Error: Role is not found woh."));
                        roles.add(chefRole);

                        break;
                    case "chefAgence":
                        Role chefAgenceRole = roleRepository.findByName(ERole.CHEF_AGENCE)
                                .orElseThrow(() -> new RuntimeException("Error: Role is not found woh."));
                        roles.add(chefAgenceRole);

                        break;
                    case "res":
                        Role modRole = roleRepository.findByName(ERole.ROLE_RESPONSABLE)
                                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                        roles.add(modRole);

                        break;


                    default:
                        Role userRole = roleRepository.findByName(ERole.ROLE_USER)
                                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                        roles.add(userRole);
                }
            });
        }

        user.setRoles(roles);
        userRepository.save(user);

        return ResponseEntity.ok(new MessageResponse("User registered successfully!"));
    }

    @DeleteMapping(value="/DeleteUser/{nom}")
    public String deleteUser(@PathVariable String nom)
    {  User user=userRepository.findByUsername(nom).get();
        userRepository.deleteById(user.getId());
        return "Supprimé avec succés ";
    }
    @PutMapping(value="/UpdateUser")
    public String UpdateUser(@Validated @RequestBody User u)
    { User  user=userRepository.findByUsername(u.getUsername()).get();
        user.setPassword(encoder.encode(u.getPassword()));
        user.setEmail(u.getEmail());
        userRepository.save(user);
        return "succés ";
    }

    @GetMapping(value="/GetAll")
    public List<User> getUsers()
    {
        return userRepository.findAll();
    }
}
