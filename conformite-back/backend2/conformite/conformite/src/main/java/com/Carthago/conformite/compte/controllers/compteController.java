package com.Carthago.conformite.compte.controllers;



import com.Carthago.conformite.clients.entities.stat;
import com.Carthago.conformite.clients.services.contactService;
import com.Carthago.conformite.compte.entities.compte;
import com.Carthago.conformite.compte.repositories.compteRepository;
import com.Carthago.conformite.compte.services.compteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value="/compte",method= {RequestMethod.GET})
public class compteController {
   @Autowired
   compteService PService;

   @Autowired
   compteRepository R;

    @Autowired
    contactService service;
   /**
    * Cette methode permet de trouver Parente par son id
    * @return
    */
   @GetMapping("/Get/{id}")
   public Optional<compte> findParente
   (@PathVariable long id)
   {

      return PService.findCompteCode(id);
   }


   /**
    * Cette methode permet de trouver Parente par son id
    * @return
    */
   @GetMapping("/GetID/{id}")
   public Long findParenteC
   (@PathVariable long id)
   {int i=0;
      Optional<compte> c= Optional.of(new compte());
   boolean v=false;
     List<compte> l=R.findAll();
       while(i<l.size()&&(v==false)){
          if(l.get(i).getContactCompte().getCodeClient()==id){
             v=true;
             c= Optional.ofNullable(l.get(i));
          }
          else {
             i++;
          }
       }
       return  c.get().getNumCompte();
   }

    /**
     * Cette methode permet de trouver Parente par son id
     * @return
     */
    @GetMapping("/GetAllCE")
    public List<compte> findAllCE ()
    { List<compte> l=R.findAll();
        return l;
    }



   }
