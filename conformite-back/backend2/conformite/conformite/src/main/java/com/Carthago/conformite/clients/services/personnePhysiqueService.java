package com.Carthago.conformite.clients.services;

import com.Carthago.conformite.clients.entities.personnePhysique;

import java.util.List;
import java.util.Optional;

public interface personnePhysiqueService {

    /**
     * Cette Methode permet d'enregistrer une personne physique.
     */
    void savePersonnePhysique(personnePhysique PP);


    /**
     * Cette Methode permet d'enregistrer les modifications exercées sur une personne physique.
     */
    void updatePersonnePhysique(personnePhysique PP);


    /**
     * Cette Methode permet de lister toutes les personnes physiques.
     */
    List<personnePhysique> listPersonnePhysique();


    /**
     * Cette Methode permet de supprimer une personne physique.
     */
    void removePersonnePhysique(long codeClient);


    /**
     * Cette Methode permet de trouver une personne physique par son CodeClient.
     */
     Optional<personnePhysique> findPersonnePhysique(long codeClient);

    /**
     * Cette Methode permet de trouver une personne physique par son CodeClient.
     */
    personnePhysique findPersonne(long codeClient);

    /**
     * mettre à jour le statut du client
     */
     void updateStatutClient(long id);

    /**
     * mettre à jour le statut du client
     */
    void updateClient(personnePhysique PP);






















    void MAJPersonnePhysique
            (personnePhysique personnePhysique);
}
