package com.Carthago.conformite.fatca.controllers;


import com.Carthago.conformite.fatca.entities.fatca;
import com.Carthago.conformite.fatca.services.fatcaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.*;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value="/fatca",method= {RequestMethod.GET})

public class fatcaController {

    @Autowired
    fatcaService FService;


    /**
     * Cette methode permet la creation de fatca
     */
    @PostMapping("/Create")
    public String createFatca
    (@Validated @RequestBody fatca f)
    {
        FService.saveFatca(f);
        return "Fatca cree";
    }


    /**
     * Cette methode de lister les fatcas
     */
    @GetMapping("/GetAll")
    public List<fatca> getAllFatca()
    {
        return FService.listFatca();

    }

    /**
     * Cette methode permet de trouver fatca par son id
     */
    @GetMapping("/Get/{id}")
    public Optional<fatca> findFatca
    (@PathVariable Long id)
    {

        return FService.findFatca(id);
    }

    /**
     * Cette methode permet de modifier fatca
     */
    @PutMapping("/Update/{id}")
    public String UpdateFatca
    (@Validated @RequestBody fatca f, @PathVariable Long id)
    {
        FService.updateFatca(id,f);
        return "La mise a jour a ete faite avec succees";
    }


    /**
     * Cette methode permet la suppression fatca par son id
     */
    @DeleteMapping(value="/Delete/{id}")
    public String deleteFatca(@PathVariable String id)
    {
        FService.removeFatca (Long.parseLong(id));
        return "fatca  supprimee avec succee";
    }

    /**
     * Ajouter justificatif
     */
    @PostMapping(value = ("/Add"),headers = "content-type=multipart/form-data")
    public String addDocument(@RequestParam(value="file", required=true) MultipartFile file, @RequestParam String nom, @RequestParam String dateDeCreation){

        String message = "";
        try {
            FService.addDocument(nom,dateDeCreation,file);

            return message = "Uploaded the file successfully: " + file.getOriginalFilename();
             //ResponseEntity.status(HttpStatus.OK).body(new ResponseMessage(message));
        } catch (Exception e) {
            return  message = "Could not upload the file: " + file.getOriginalFilename() + "!";
             //ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new ResponseMessage(message));
        }
    }


}

