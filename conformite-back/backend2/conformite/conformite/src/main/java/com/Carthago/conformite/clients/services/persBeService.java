package com.Carthago.conformite.clients.services;

import com.Carthago.conformite.clients.entities.persBe;

import java.util.ArrayList;
import java.util.List;

public interface persBeService {

    /**
     * Cette Methode permet de retouner la liste des codes des beneficiaires effectifs
     */
    ArrayList<Long> listPersBeCode(long id);

    /**
     * find all
     */
    List<persBe> findAll();
}
