package com.Carthago.conformite.springsecurity.entities;







public enum ERole {
    ROLE_USER,
    ROLE_RESPONSABLE,
    ROLE_ADMIN,
    CHEF_AGENCE,
    CONSEILLER_CLIENTS
}
