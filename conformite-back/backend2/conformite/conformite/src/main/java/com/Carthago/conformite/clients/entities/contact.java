package com.Carthago.conformite.clients.entities;

import com.Carthago.conformite.compte.entities.compte;
import com.Carthago.conformite.fatca.entities.fatca;
import com.Carthago.conformite.pep.entities.pep;
import com.Carthago.conformite.risque.entities.modeleGlobalPhysique;

import com.Carthago.conformite.risque.entities.risque;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Entity
public class contact {

    //declaration des variables
    private static final long serialVersionUID=1L;
    @Id
    private  Long codeClient; //Code du client
    private  String paysResidence=""; //Pays residence du client
    private  String email=""; //Email du client
    private  String numTelephone=""; //Numero du telephone du client
    private String indiTelephonique; //indicatif telephonique
    private  boolean codeTin=false ; //true si le client a un code TIN
    private String Adresse=""; //adresse du client
    private  String revue=""; // periodicite de revue
    private Date derniereDate ; //date de la derniere mise à jour
    private String niveauRisque ; //niveau du risque de chaque client
    private String statutPersonne;//statut dela personne
    private String secteurTravail=""; //secteur Travail
    private  String matriculeFiscale; //matricule Fiscale
    private Long numeroRne; //numero RNE


    public String getMatriculeFiscale() {
        return matriculeFiscale;
    }

    public void setMatriculeFiscale(String matriculeFiscale) {
        this.matriculeFiscale = matriculeFiscale;
    }

    public Long getNumeroRne() {
        return numeroRne;
    }

    public void setNumeroRne(Long numeroRne) {
        this.numeroRne = numeroRne;
    }

    public String getSecteurTravail() {
        return secteurTravail;
    }

    public void setSecteurTravail(String secteurTravail) {
        this.secteurTravail = secteurTravail;
    }

    public Long getCodeClient() {
        return codeClient;
    }

    public void setCodeClient(Long codeClient) {
        this.codeClient = codeClient;
    }

    public String getPaysResidence() {
        return paysResidence;
    }

    public void setPaysResidence(String paysResidence) {
        this.paysResidence = paysResidence;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNumTelephone() {
        return numTelephone;
    }

    public void setNumTelephone(String numTelephone) {
        this.numTelephone = numTelephone;
    }

    public boolean isCodeTin() {
        return codeTin;
    }

    public void setCodeTin(boolean codeTin) {
        this.codeTin = codeTin;
    }

    public pep getPep() {
        return pep;
    }

    public void setPep(pep pep) {
        this.pep = pep;
    }

    public String getAdresse() {
        return Adresse;
    }

    public void setAdresse(String adresse) {
        Adresse = adresse;
    }

    public fatca getFatca() {
        return fatca;
    }

    public void setFatca(fatca fatca) {
        this.fatca = fatca;
    }

    public String getRevue() {
        return revue;
    }

    public void setRevue(String revue) {
        this.revue = revue;
    }

    public Date getDerniereDate() {
        return derniereDate;
    }

    public void setDerniereDate(Date derniereDate) {
        this.derniereDate = derniereDate;
    }

    public String getNiveauRisque() {
        return niveauRisque;
    }

    public void setNiveauRisque(String niveauRisque) {
        this.niveauRisque = niveauRisque;
    }

    public String getIndiTelephonique() {
        return indiTelephonique;
    }

    public void setIndiTelephonique(String indiTelephonique) {
        this.indiTelephonique = indiTelephonique;
    }

    //constructeur
    public contact(Long codeClient) {
        this.codeClient = codeClient;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public List<compte> getListCompte() {
        return listCompte;
    }

    public void setListCompte(List<compte> listCompte) {
        this.listCompte = listCompte;
    }

    public void setStatutPersonne(String statutPersonne) {
        this.statutPersonne = statutPersonne;
    }

    public String getStatutPersonne() {
        return statutPersonne;
    }

    //Relations
    @OneToOne(mappedBy ="contact",cascade = CascadeType.ALL)
    private pep pep ;
    //relation client et fatca
    @OneToOne(mappedBy ="contact1",cascade = CascadeType.ALL)
    private fatca fatca;

    //relation client et risque
    @OneToOne(mappedBy ="contactRisque",cascade = CascadeType.ALL)
    private risque risquePersonne;
    //relation entre conatct et compte
    @OneToMany(mappedBy = "contactCompte", cascade = CascadeType.ALL)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private List<compte> listCompte;

























    //relation client et modele risque
    @OneToOne(mappedBy ="contactModelRisque",cascade = CascadeType.ALL)
    private modeleGlobalPhysique  modeleRisque;
}
