package com.Carthago.conformite.compte.entities;

import com.Carthago.conformite.clients.entities.contact;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
public class compteEpargne extends compte {

    private double tauxDeRenumeration=0.03;

    //getters and setters :

    public double getTauxDeRenumeration() {
        return tauxDeRenumeration;
    }

    public void setTauxDeRenumeration(double tauxDeRenumeration) {
        this.tauxDeRenumeration = tauxDeRenumeration;
    }

    public compteEpargne(Long id) {
        super(id);
    }

    public compteEpargne(contact contactCompte) {
        super(contactCompte);
    }
}
