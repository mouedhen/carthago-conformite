package com.Carthago.conformite.clients.entities;

import lombok.*;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
public class beneficiaireEffectif extends personnePhysique{

    //declaration des variables
    private float pourcentage; //pourcentage de participation

    //getter and setter
    public float getPourcentage() {
        return pourcentage;
    }

    public void setPourcentage(float pourcentage) {
        this.pourcentage = pourcentage;
    }

    public List<personneMorale> getPersonneMoraleArrayList() {
        return personneMoraleArrayList;
    }

    public void setPersonneMoraleArrayList(List<personneMorale> personneMoraleArrayList) {
        this.personneMoraleArrayList = personneMoraleArrayList;
    }

   /* public List<persBe> getListPersBe() {
        return listPersBe;
    }

    public void setListPersBe(List<persBe> listPersBe) {
        this.listPersBe = listPersBe;
    }

   */
    //constructeur

    public beneficiaireEffectif(Long codeClient) {
        super(codeClient);
    }

    public beneficiaireEffectif(Long codeClient, float pourcentage) {
        super(codeClient);
        this.pourcentage = pourcentage;
    }
    //Relation entre beneficiaire effectif et table association
    /*@OneToMany(mappedBy = "beneficiaireEff",cascade = CascadeType.ALL)
    private List<persBe> listPersBe;*/

    //relation beneficiaire effectif et personne morale

    @ManyToMany
    @JoinTable( name = "PersBe",
        joinColumns = @JoinColumn( name = "codeBe" ),
        inverseJoinColumns = @JoinColumn( name = "codeClient1" ) )
    private List<personneMorale> personneMoraleArrayList = new ArrayList<>();




}
