package com.Carthago.conformite.risque.repositories;

import com.Carthago.conformite.risque.entities.risque;
import org.springframework.data.jpa.repository.JpaRepository;

public interface risqueRepository extends JpaRepository<risque,Long> {
}
