package com.Carthago.conformite.risque.controllers;

import com.Carthago.conformite.risque.entities.modeleGlobalPhysique;
import com.Carthago.conformite.risque.services.RisquePersonneMoraleService;
import com.Carthago.conformite.risque.services.modeleGlobalPhysiqueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;
@RestController
@RequestMapping(value="/modelePersonnePhysique",method= {RequestMethod.GET})
public class modeleGlobalPhysiqueController {


    @Autowired
    modeleGlobalPhysiqueService service;

    @Autowired
    RisquePersonneMoraleService  s;
    /**
     * Cette methode permet de trouver le modele du risque par code client
     * @return
     */
    @GetMapping("/Get/{id}")
    public Optional<modeleGlobalPhysique> findModele
    (@PathVariable long id)
    {


        return service.find(id);
    }
}
