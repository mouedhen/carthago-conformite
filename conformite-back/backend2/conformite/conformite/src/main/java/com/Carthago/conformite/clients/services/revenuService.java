package com.Carthago.conformite.clients.services;

import com.Carthago.conformite.clients.entities.revenu;

import java.util.List;
import java.util.Optional;

public interface revenuService {

    /**
     * Cette Methode permet d'enregistrer un revenu.
     */
    void saveRevenu(revenu R);


    /**
     * Cette Methode permet d'enregistrer les modifications exercées sur un revenu.
     */
    void updateRevenu(long codeRevenu,revenu R);


    /**
     * Cette Methode permet de lister tous les revenus.
     */
    List<revenu> listRevenu();


    /**
     * Cette Methode permet de supprimer un revenu.
     */
    void removeRevenu(long codeRevenu);


    /**
     * Cette Methode permet de trouver un revenu par son CodeRevenu.
     */
    public Optional<revenu> findRevenu(long codeRevenu);
}
