package com.Carthago.conformite.compte.repositories;

import com.Carthago.conformite.compte.entities.compteEpargne;
import org.springframework.data.jpa.repository.JpaRepository;

public interface compteEpargneRepository extends JpaRepository<compteEpargne,Long> {
}
