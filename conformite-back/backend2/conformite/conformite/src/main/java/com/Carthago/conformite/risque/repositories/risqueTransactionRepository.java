package com.Carthago.conformite.risque.repositories;


import com.Carthago.conformite.risque.entities.risqueTransaction;
import org.springframework.data.jpa.repository.JpaRepository;

public interface risqueTransactionRepository  extends JpaRepository<risqueTransaction,Long> {
}
