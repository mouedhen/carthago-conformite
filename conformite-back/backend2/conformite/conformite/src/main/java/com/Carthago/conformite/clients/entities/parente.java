package com.Carthago.conformite.clients.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
public class parente {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private  Long code;
    private  Long codeParente;
    private String lienParente=""; //lien du parente qui existe

    //getters et setters


    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public Long getCodeParente() {
        return codeParente;
    }

    public void setCodeParente(Long codeParente) {
        this.codeParente = codeParente;
    }

    public String getLienParente() {
        return lienParente;
    }

    public void setLienParente(String lienParente) {
        this.lienParente = lienParente;
    }

    public personnePhysique getPerPhy() {
        return perPhy;
    }

    public void setPerPhy(personnePhysique perPhy) {
        this.perPhy = perPhy;
    }

    //Constructeur


    public parente(personnePhysique perPhy) {
        this.perPhy = perPhy;
    }

    //Relation many to one  entre parente et personne physique
    @JsonBackReference
    @ManyToOne
    @JoinColumn(name="codePers")
    private personnePhysique perPhy;

}
