package com.Carthago.conformite.clients.serviceImp;
import com.Carthago.conformite.clients.entities.beneficiaireEffectif;
import com.Carthago.conformite.clients.repositories.beneficiareEffectifRepository;
import com.Carthago.conformite.clients.services.beneficiaireEffectifService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;
@Service("beneficiaireEfffectifServiceImpl")
public class beneficiaireEffectifServiceImp implements beneficiaireEffectifService {
    @Autowired
    beneficiareEffectifRepository BFRepository;
    /**
     * C'est l'implementation de la methode qui permet d'enregistrer cet objet
     */
    @Override
    public void saveBeneficiaireEffectif(beneficiaireEffectif BF) {
        BFRepository.save(BF);
    }
    /**
     * C'est l'implementation de la methode qui permet
     * d'enregitrer les modifications exercées sur cet objet
     */
    @Override
    public void updateBeneficiaireEffectif
    (long codeC,beneficiaireEffectif newBeneficiaireEffectif) {
        Optional<beneficiaireEffectif> OldBeneficiaireEffectif=BFRepository.findById(codeC);
        newBeneficiaireEffectif.setPersonneMoraleArrayList(OldBeneficiaireEffectif.get().getPersonneMoraleArrayList());
        BFRepository.save(newBeneficiaireEffectif);
    }
    /**
     * C'est l'implementation de la methode qui permet de lister tous ces objets
     */
    @Override
    public List<beneficiaireEffectif> listBeneficiaireEffectif(){
        return BFRepository.findAll();
    }
    /**
     * C'est l'implementation de la methode qui permet de supprimer cet objet
     */
    @Override
    public  void removeBeneficiaireEffectif(long CodeC){
        BFRepository.deleteById(CodeC);
    }
    /**
     * C'est l'implementation de la methode qui permet de trouver cet objet par son id
     */
    @Override
    public Optional<beneficiaireEffectif> findBeneficiaireEffectif(long CodeC){
        return BFRepository.findById(CodeC);
    }
}
