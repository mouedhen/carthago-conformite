package com.Carthago.conformite.compte.servicesImp;



import com.Carthago.conformite.clients.services.personneMoraleService;
import com.Carthago.conformite.clients.services.personnePhysiqueService;
import com.Carthago.conformite.compte.entities.compteEpargne;
import com.Carthago.conformite.compte.repositories.compteEpargneRepository;
import com.Carthago.conformite.compte.repositories.transactionRepository;
import com.Carthago.conformite.compte.services.compteEpargneService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service("compteEpargneserviceImpl")
public class compteEpargneServiceImp  implements compteEpargneService {
    @Autowired
    compteEpargneRepository PRepository;

    @Autowired
    transactionRepository trepository;



    @Autowired
    personneMoraleService service;
    @Autowired
    personnePhysiqueService Pservice;
    /**
     * C'est l'implementation de la methode qui permet d'enregistrer cet objet
     */
    @Override
    public void saveCompteEpargneClientMoral(compteEpargne p, long id  ) {
        service.updateStatutClient(id);

        p.setDateOuverture(java.time.LocalDate.now());
        PRepository.save(p);
        p.setRIB(p.getNumCompte().toString());
        PRepository.save(p);
    }
    /**
     * C'est l'implementation de la methode qui permet d'enregistrer cet objet
     */
    @Override
    public void saveCompteEpargneClientPhysique(compteEpargne p, long id  ) {

        p.setDateOuverture(java.time.LocalDate.now());
        Pservice.updateStatutClient(id);
        PRepository.save(p);
        p.setRIB(p.getNumCompte().toString());
        PRepository.save(p);
    }


    /**
     * C'est l'implementation de la methode qui permet d'enregitrer les modifications exercées sur cet objet
     */
    @Override
    public void updateCompteEpargne(long codeP,compteEpargne newParente) {
        compteEpargne parente;
        Optional<compteEpargne> OldParente=PRepository.findById(codeP);

        if(OldParente.isPresent())
        {
            parente=OldParente.get();
            PRepository.save(parente);
        }
    }

    /**
     * C'est l'implementation de la methode qui permet de lister tous ces objets
     */
    @Override
    public List<compteEpargne> listCompteEpargne(){
        return PRepository.findAll();
    }

    /**
     * C'est l'implementation de la methode qui permet de supprimer cet objet
     */
    @Override
    public  void removeCompteEpargne(long codeBe){
        PRepository.deleteById(codeBe);
    }

    /**
     * C'est l'implementation de la methode qui permet de trouver cet objet par son id
     */
    @Override
    public compteEpargne findCompteEpargne(String code){
        compteEpargne c = new compteEpargne();
        int i=0;
        List<compteEpargne>l=PRepository.findAll();
        while (l.size()>i)
        {
            if(l.get(i).getRIB().equals(code))
            {   c=l.get(i);
                return c;
            }
            else {
                i++;
            }
        }
        return c;
    }

    /**
     * l'implementation de la méthode credit
     */
    @Override
    public void crediterEpargne (compteEpargne c ,double montant)
    {
        c.setSolde(c.getSolde()+montant);
    }
    @Override
    public void debiterEpargne (compteEpargne c , double montant)
    {
        c.setSolde(c.getSolde()-montant);
    }
}
