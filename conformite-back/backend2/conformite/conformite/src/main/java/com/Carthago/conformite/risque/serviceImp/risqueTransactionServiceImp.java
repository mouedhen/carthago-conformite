package com.Carthago.conformite.risque.serviceImp;

import com.Carthago.conformite.risque.entities.risqueTransaction;
import com.Carthago.conformite.risque.repositories.risqueTransactionRepository;
import com.Carthago.conformite.risque.services.risqueTransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service("risqueTransaction")
public class risqueTransactionServiceImp implements risqueTransactionService {
    @Autowired
    risqueTransactionRepository RR;

    /**
     * Fonction qui enregistre le risque d'une transaction
     */
    @Override
    public void saveRisque(risqueTransaction R) {
        RR.save(R);
    }

    @Override
    public risqueTransaction find() {
        risqueTransaction r;
        List<risqueTransaction> l=RR.findAll();
        r=l.get(l.size()-1);
        return r;
    }

    @Override
    public Optional <risqueTransaction> findTransaction (Long id)
    {
        return RR.findById(id);
    }

}
