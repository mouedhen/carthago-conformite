package com.Carthago.conformite.risque.controllers;


import com.Carthago.conformite.clients.entities.personneMorale;
import com.Carthago.conformite.risque.entities.modeleRisquePersonneMorale;
import com.Carthago.conformite.risque.services.RisquePersonneMoraleService;
import com.Carthago.conformite.risque.services.modeleRisquePersonneMoraleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping(value="/modelePersonneMorale",method= {RequestMethod.GET})
public class modeleRisquePersonneMoraleController {

    @Autowired
    modeleRisquePersonneMoraleService service;

    @Autowired
    RisquePersonneMoraleService s1;

    /**
     * Cette methode permet de trouver le modele du risque par code client
     */
    @GetMapping("/Get/{id}")
    public Optional<modeleRisquePersonneMorale> findModele
    (@PathVariable long id) {
        return service.find(id);
    }
}

