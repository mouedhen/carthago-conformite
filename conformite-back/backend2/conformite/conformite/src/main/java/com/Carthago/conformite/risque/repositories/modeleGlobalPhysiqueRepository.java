package com.Carthago.conformite.risque.repositories;

import com.Carthago.conformite.risque.entities.modeleGlobalPhysique;
import org.springframework.data.jpa.repository.JpaRepository;

public interface modeleGlobalPhysiqueRepository extends JpaRepository<modeleGlobalPhysique,Long> {
}
