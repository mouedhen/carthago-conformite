package com.Carthago.conformite.risque.services;

import com.Carthago.conformite.risque.entities.modeleGlobalPhysique;

import java.util.Optional;

public interface modeleGlobalPhysiqueService {


    void saveModeleGobale( modeleGlobalPhysique r);
    void updateModeleGlobale( modeleGlobalPhysique r);
    Optional<modeleGlobalPhysique> find(Long id);
}
