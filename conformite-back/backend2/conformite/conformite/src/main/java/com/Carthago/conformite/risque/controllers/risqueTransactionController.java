package com.Carthago.conformite.risque.controllers;


import com.Carthago.conformite.risque.entities.risqueTransaction;
import com.Carthago.conformite.risque.services.risqueTransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping(value="/RisqueTransaction",method= {RequestMethod.GET})
public class risqueTransactionController {

    @Autowired
    risqueTransactionService RService ;

    /**
     * Cette methode permet la creation du revenu
     */
    @PostMapping("/Create")
    public String createRevenu
    (@Validated @RequestBody risqueTransaction R)
    {
        RService.saveRisque(R);
        return " cree";
    }

    /**
     * Cette methode permet de trouver le modele du risque par code client
     */
    @GetMapping("/Get")
    public risqueTransaction findModele() {
        return RService.find( );
    }

    /**
     * Trouver un modéle
     */
    @GetMapping("/GetRisqueTransaction/{id}")
    public Optional<risqueTransaction> findTransaction(@PathVariable Long id)
    {
        return RService.findTransaction(id);
    }
}
