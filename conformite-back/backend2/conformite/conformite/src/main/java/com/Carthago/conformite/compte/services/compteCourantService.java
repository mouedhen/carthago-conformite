package com.Carthago.conformite.compte.services;

import com.Carthago.conformite.compte.entities.compteCourant;
import java.util.List;
import java.util.Optional;

public interface compteCourantService {


    /**
     * Cette Methode permet d'enregistrer CompteCourant.
     */
    void saveCompteCourantClientMoral(compteCourant P, long id  );
    /**
     * Cette Methode permet d'enregistrer CompteCourant.
     */
    void saveCompteCourantClientPhysique(compteCourant P, long id  );

    /**
     * Cette Methode permet d'enregistrer les modifications exercées sur CompteCourant.
     */
    void updateCompteCourant(long codeCompteCourant,compteCourant P);


    /**
     * Cette Methode permet de lister tous les CompteCourants.
     */
    List<compteCourant> listCompteCourant();


    /**
     * Cette Methode permet de supprimer CompteCourant.
     */
    void removeCompteCourant(long codeCompteCourant);


    /**
     * Cette Methode permet de trouver CompteCourant par son codeCompteCourant.
     */
    public compteCourant findCompteCourant(String codeCompteCourant);

    /**
     * methode de credit
     */
    void crediterCourant(compteCourant c, double montant );
    /**
     * methide de débit
     */
    void debiterCourant(compteCourant c , double montant);


}
