package com.Carthago.conformite.clients.services;

import com.Carthago.conformite.clients.entities.parente;

import java.util.List;
import java.util.Optional;

public interface parenteService {
    /**
     * Cette Methode permet d'enregistrer Parente.
     */
    void saveParente(parente P);


    /**
     * Cette Methode permet d'enregistrer les modifications exercées sur parente.
     */
    void updateParente(long codeParente,parente P);


    /**
     * Cette Methode permet de lister tous les parentes.
     */
    List<parente> listParente();


    /**
     * Cette Methode permet de supprimer parente.
     */
    void removeParente(long codeParente);


    /**
     * Cette Methode permet de trouver parente par son codeParente.
     */
    public Optional<parente> findParente(long codeParente);


}

