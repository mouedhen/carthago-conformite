package com.Carthago.conformite.risque.serviceImp;
import com.Carthago.conformite.clients.entities.*;
import com.Carthago.conformite.clients.serviceImp.nationaliteServiceImp;
import com.Carthago.conformite.clients.services.RepPersService;
import com.Carthago.conformite.clients.services.beneficiaireEffectifService;
import com.Carthago.conformite.clients.services.persBeService;
import com.Carthago.conformite.clients.services.representantLegalService;
import com.Carthago.conformite.pep.entities.pep;
import com.Carthago.conformite.pep.services.pepService;
import com.Carthago.conformite.risque.entities.*;
import com.Carthago.conformite.risque.repositories.risquePersonneMoraleRepository;
import com.Carthago.conformite.risque.repositories.risqueRepository;
import com.Carthago.conformite.risque.services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import com.opencsv.CSVReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
@Service("risquePersonneMorale")
public class RisquePersonneMorale implements RisquePersonneMoraleService {
    @Autowired
    risquePersonneMoraleRepository RR;
    @Autowired
    secteurService S;
    @Autowired
    paysService PService;
    @Autowired
    risqueService RService;
    @Autowired
    pepService pe;
    @Autowired
    RepPersService RepService;
    @Autowired
    persBeService persBeService;
    @Autowired
    risqueFatcaServiceImp RFService ;
    @Autowired
    beneficiaireEffectifService BService ;
    @Autowired
    representantLegalService RLService;
    @Autowired
    modeleRisquePersonneMoraleService mService;
    @Autowired
    modeleCotationRisqueService modeleService ;
    private final ArrayList<personneBlackliste> blacklistePersonne;
    public RisquePersonneMorale(ArrayList<personneBlackliste> blacklistePersonne) {
        this.blacklistePersonne = blacklistePersonne;
    }
    /**
     * Fonction qui enregistre le risque
     */
    @Override
    public void saveRisque(risquePersonneMorale R) {
        RR.save(R);
    }
    @Override
    /**
     * Fonction qui calcule le risque de la personne Morale selon sa nature et secteur
     */
    public int risqueNature (personneMorale p)
    { int r=0;
        modeleRisquePersonneMorale m = new modeleRisquePersonneMorale();
        m.setCode(p.getCodeClient());
        Optional<modeleCotationRisque> modele = modeleService.findModeleCotationRisque((long) 1);
        if ((p.getNatureOperation()
                .equalsIgnoreCase(
                        "Achat et vente ou autres actes de " +
                                "disposition de biens immeubles ou entités"))||
                (p.getOrigineFonds()
                        .equalsIgnoreCase(
                                "De la vente d'un immeuble")))
        {    m.setNature("L'origine des fonds de la societe est de la vente des immeubles");
            r=r+modele.get().getNoteNature();
        }
        //fonds non tunisiens
        if( !p.getPaysOrigineFonds().equalsIgnoreCase("Tunisie"))
        {  m.setPaysPro("Le pays de provenance des fonds n'est pas la Tunisie");
            r=r+modele.get().getNotePaysOrigineFonds();
        }
        //verifie si la societe est offshore
        if (!p.getPaysResidence()
                .equalsIgnoreCase(
                        "Tunisie"))
        {  m.setOffshore("Il s'agit du Offshore");
            r=r+modele.get().getNoteOffshore();
        }
        //risque s'il s'agit d'ONG
        if(p.getNature().equalsIgnoreCase("ONG"))
        {   m.setOng("Il s'agit d'une ONG");
            r=r+modele.get().getOng();
        }
        //si l'ese est cotée sur le marche boursier
        if(p.getMarcheBoursier())
        {   m.setMarcheBoursier("Cette societe est cotée sur le marché boursier");
            r=r+modele.get().getNoteMarcheBoursier();
        }
        if(p.getParticipationPub())
        {   m.setPub("Cette societe est à participation publique");
            r=r+modele.get().getNoteParticipationPub();
        }
        //verifie les actionnaires
        if(p.getActionnaireOffshore())
        {  m.setAssOffshore("Société ayant un actionnariat sur plusieurs niveaux et des actionnaires\n" +
                "sociétés offshore ");
            r=r+modele.get().getNoteAssOffshore();
        }
        if(p.getChiffreAffaire()>5000000)
        {
            m.setChiffreAffaires("Chiffre d'affaire supérieur à 5.000.000DT");
            r=r+modele.get().getNoteChiffreAffaire();
        }
        if(p.isAssociesPpe())
        {   r=r+modele.get().getNotePepPersonneMorale(); //un  des associées est politiquement exposé
            m.setAccPpe("Un des actionnaires est politiquement exposé");
        }
        //verifie si le representant legal est dans la black list
        ArrayList<Long> listeRep=RepService.listRepPersCode(p.getCodeClient());
        if(blackList(listeRep)>70)
        {
            m.setBlackListe("Representant Legal prensent dans la blacklist");
            r=r+modele.get().getNoteBlackListePM();
        }
        m.setValeur(r);
        mService.save(m);
        return r;
    }
    /**
     * Fonction qui calcule le risque  de la personne Morale selon le critére geographique
     * et le secteur
     */
    @Override
    public int risqueGeographique(@PathVariable long id, personneMorale p){
        int r=0,res;
        Optional<modeleRisquePersonneMorale> m3=mService.find(p.getCodeClient());
        //trouver le risque geographique pour les representants Legals :
        ArrayList<Long> listeRep=RepService.listRepPersCode(id);
        r=r+RisqueRepresentant(listeRep,m3,p.getCodeClient());
        //trouver le risque geographique pour les beneficiaires effectifs :
        ArrayList<Long> listeBe=persBeService.listPersBeCode(id);
        r=r+RisqueBeneficiaire(listeBe,m3,p.getCodeClient());
        //trouver le risque des pays
        r=r+ critereGeo("\n"+" Pays Origine Fonds :  "+"\n",
                p.getPaysOrigineFonds(),m3,p.getCodeClient());
        r=r+ critereGeo( "\n"+" Pays Residence :  "+"\n",
                p.getPaysResidence(),m3,p.getCodeClient());
        r=r+ critereGeo("\n"+"Pays de Construction : "+"\n",
                p.getPaysConstruction(),m3,p.getCodeClient());
        if(p.isAssociesPpe())
        {
            //Optional<pep> t=pe.findPep(p.getCodeClient());
            //r=r+critereGeo(" Pays ou la personne est politiquement exposée",t.get().getPaysPep(),m3,p.getCodeClient());
        }
        r=r+ risqueSecteur(p,m3,p.getCodeClient());
        return r/4;
    }
    /**
     * Fonction qui calcule le risque des beneficiaires effectifs
     */
    public int  RisqueBeneficiaire(ArrayList<Long> ListeRep, Optional<modeleRisquePersonneMorale> m3,long id )
    { int i=0;
        int r=0;
        String pays ;
        while (i < ListeRep.size())
        {
            Optional<beneficiaireEffectif> b = Optional.of(new beneficiaireEffectif());
            b=BService.findBeneficiaireEffectif(ListeRep.get(i));
            pays = b.get().getPaysResidence();
            r=r+critereGeo("\n"+"                  Pays Residence du beneficiaire :" +"\n",pays,m3,id);
            i++;
        }
        return r;
    }
    /**
     * Fonction qui calcule le risque des representants Legals
     */
    public int  RisqueRepresentant(ArrayList<Long> ListeRep,Optional<modeleRisquePersonneMorale> m3,long id)
    { int i=0;
        int r=0;
        String pays ;
        while (i < ListeRep.size())
        {
            Optional<representantLegal> b = Optional.of(new representantLegal());
            b=RLService.findRepresentantLegal(ListeRep.get(i));
            pays = b.get().getPaysResidence();
            r=r+critereGeo("\n"+"Pays Residence du representant :"+"\n",pays,m3,id);
            i++;
        }
        return r;
    }
    /**
     * Fonction qui calcule le risque selon le critere geographique
     */
    @Override
    public  int critereGeo(String x,String nat,Optional<modeleRisquePersonneMorale> m, long id)
    {
        int note_p=0;
        Boolean t=false,u,o,y;
        int i = 0,j,f,k,v;
        String ch=" ",ch1=" ",ch2=" ",ch3=" ",ch4=" ",ch5=" ",e="       ";
        List<pays> l=PService.findAllListeGrise();
        List<pays> l1=PService.findAllListeNoire();
        List<pays> l2=PService.findAllListePaysGeurre();
        List<pays> l4=PService.findAllListeEconomieInformelle();
        List<pays> l5=PService.findAllListePaysProducteursDrogue();
        ArrayList<listeCorruption> l3=PService.findAllListeCorruption();
        Optional<modeleCotationRisque> modele = modeleService.findModeleCotationRisque((long) 1);
        while (i < l.size() && t == false) {
            if (l.get(i).getPays().equalsIgnoreCase(nat)) {
                t = true;
                note_p = note_p + modele.get().getNoteListeGrise();
                if(m.get().getListeGrise()!=null)
                {ch=m.get().getListeGrise();}
                ch=ch+x+e+l.get(i).getPays()+e;
                m.get().setListeGrise(ch); }
            else { i++; } }
        f = 0;
        y = false;
        while ( f < l1.size() && y==false) {
            if(l1.get(f).getPays().equalsIgnoreCase(nat))
            {   y=true;
                note_p=note_p+modele.get().getNoteListeNoire();
                if(m.get().getListeNoire()!=null)
                {ch1=m.get().getListeNoire();}
                ch1=ch1+x+e+l1.get(f).getPays()+e;
                m.get().setListeNoire("\n"+ch1); }
            else{ f++; } }
        t=false;
        i=0;
        while ( i < l2.size() && t==false) {
            if(l2.get(i).getPays().equalsIgnoreCase(nat))
            { t=true;
                if(m.get().getListeGeurre()!=null)
                { ch2=m.get().getListeGeurre();}
                note_p=note_p+modele.get().getNotePaysGuerre();
                ch2=ch2+x+e+l2.get(i).getPays()+e;
                m.get().setListeGeurre("\n"+ch2); }
            else{ i++; } }
        t=false;
        i=0;
        while ( i < l3.size() && t==false) {
            if((l3.get(i).getPays().equalsIgnoreCase(nat))&&(l3.get(i).getRang()>70))
            {   t=true;
                note_p=note_p+modele.get().getNotePaysCorruption();
                if(m.get().getListeCorruption()!=null)
                {ch3=m.get().getListeCorruption();}
                ch3=ch3+x+e+l3.get(i).getPays()+e;
                m.get().setListeCorruption("\n"+ch3); }
            else{
                i++;
            } }
        u = false;
        k = 0;
        while ( k < l4.size() && u==false) {
            if(l4.get(k).getPays().equalsIgnoreCase(nat))
            { u=true;
                note_p=note_p+modele.get().getNotePaysEconomieParallele();
                if(m.get().getListeEconomieParallele()!=null)
                {ch4=m.get().getListeEconomieParallele();}
                ch4=ch4+x+e+l4.get(k).getPays()+e;
                m.get().setListeEconomieParallele("\n"+ch4); }
            else{ k++; } }
        o = false;
        v = 0;
        while ( v < l5.size() && o==false) {
            if(l5.get(v).getPays().equalsIgnoreCase(nat))
            {   o=true;
                note_p=note_p+modele.get().getNotePaysDrogues();
                if(m.get().getListeDrogue()!=null)
                {ch5=m.get().getListeDrogue();}
                ch5=ch5+x+e+l5.get(v).getPays()+e;
                m.get().setListeDrogue("\n"+ch5); }
            else{
                v++; }}
        modeleRisquePersonneMorale MR = m.get();
        int valeur =m.get().getValeur();
        MR.setValeur(valeur+note_p);
        mService.update(MR,id);
        return note_p;
    }
    /**
     * Calcule le risque Par secteur
     * @param p
     * @return
     */
    @Override
    public int risqueSecteur(personneMorale p,Optional<modeleRisquePersonneMorale> m3,long id){
        int note_p=0;
        Boolean t=false;
        int i = 0;
        List<secteur> l=S.findAllListeSecteur();
        Optional<modeleCotationRisque> modele = modeleService.findModeleCotationRisque((long) 1);
        while ( i < l.size() && t==false) {
            String ch=l.get(i).getNom();
            if(ch.equalsIgnoreCase(p.getSecteurTravail()))
            {
                if((ch.equalsIgnoreCase("Import-Export"))||(ch.equalsIgnoreCase("Casinos"))
                        ||(ch.equalsIgnoreCase("Etablissement de jeux de hasard"))||(ch.equalsIgnoreCase("Clubs VIP")))
                {t=true;
                    note_p=note_p+modele.get().getNoteSecteur();}
                if((ch.equalsIgnoreCase("Industrie petro-chimique"))||(ch.equalsIgnoreCase("Transport maritime"))||(ch.equalsIgnoreCase("Petrole"))||(ch.equalsIgnoreCase("Chimie-Parachimie"))||(ch.equalsIgnoreCase("Arment")))
                {t=true;
                    note_p=note_p+modele.get().getNoteSecteur1();}
                if((ch.equalsIgnoreCase("Commerce"))||(ch.equalsIgnoreCase("Automobile-Reparation automobiles"))||(ch.equalsIgnoreCase("Immobilier"))||(ch.equalsIgnoreCase("Relations d affaire avec etats ou gouvernements etrangers"))||(ch.equalsIgnoreCase("Bureaux de change")))
                {t=true;
                    note_p=note_p+modele.get().getNoteSecteur2();}
                if((ch.equalsIgnoreCase("Objets d art et antiquite"))||(ch.equalsIgnoreCase("Informatique-Telecoms"))||(ch.equalsIgnoreCase("Metaux et pierres precieuse"))||(ch.equalsIgnoreCase("BTP-Materiaux de construction")))
                {t=true;
                    note_p=note_p+modele.get().getNoteSecteur3();}}
            else{
                i++;
            }
        }
        m3.get().setSecteur(p.getSecteurTravail());
        modeleRisquePersonneMorale MR = m3.get();
        int valeur=m3.get().getValeur();
        m3.get().setValeur(valeur+note_p);
        mService.update(MR,id);
        return note_p;
    }
    //EXTRAIRE LA LISTE DES PERSONNES AU LISTE NOIRE
    public ArrayList<personneBlackliste> findBlackListe(){
        FileInputStream fis1 = null;
        try {
            String fileName = "src/main/resources/liste_pays/blackList_personne.csv";
            fis1 = new FileInputStream(new File(fileName));
            CSVReader reader = new CSVReader(new InputStreamReader(fis1));
            String[] nextLine;
            reader.readNext();
            while ((nextLine = reader.readNext()) != null) {
                personneBlackliste newCountry = new personneBlackliste(Long.valueOf(nextLine[0]),
                        nextLine[1]);
                blacklistePersonne.add(newCountry);
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(nationaliteServiceImp.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(nationaliteServiceImp.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (fis1 != null) {
                    fis1.close();
                }
            } catch (IOException ex) {
                Logger.getLogger(nationaliteServiceImp.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return blacklistePersonne;
    }
    public float  blackList(ArrayList<Long> ListeRep) {
        float res=0 ,nbre=0,v,v1=0;
        for (int j =0;j<ListeRep.size();j++)
        {   Optional<representantLegal> b = Optional.of(new representantLegal());
            b=RLService.findRepresentantLegal(ListeRep.get(j));
            String ch=b.get().getPrenom()+" "+b.get().getNom();
            List<personneBlackliste> l=findBlackListe();
            for (int i = 0; i<l.size(); i++)
            {   v=0;
                v =calculate(ch,l.get(i).getNomPrenom());
                if((i==0)||(v<v1)){
                    v1=v;
                    if(ch.length()>l.get(i).getNomPrenom().length()){nbre=ch.length();}
                    else{nbre=l.get(i).getNomPrenom().length();}
                }
            }
            res=100-((v1/nbre)*100);
        }
        return res;
    }
    static int calculate(String x, String y) {
        int[][] dp = new int[x.length() + 1][y.length() + 1];
        for (int i = 0; i <= x.length(); i++) {
            for (int j = 0; j <= y.length(); j++) {
                if (i == 0) {
                    dp[i][j] = j;
                }
                else if (j == 0) {
                    dp[i][j] = i;
                }
                else {
                    dp[i][j] = min(dp[i - 1][j - 1]
                                    + costOfSubstitution(x.charAt(i - 1), y.charAt(j - 1)),
                            dp[i - 1][j] + 1,
                            dp[i][j - 1] + 1);
                }
            }
        }
        return dp[x.length()][y.length()];
    }
    public static int costOfSubstitution(char a, char b) {
        return a == b ? 0 : 1;
    }
    public static int min(int... numbers) {
        return Arrays.stream(numbers)
                .min().orElse(Integer.MAX_VALUE);
    }
}
