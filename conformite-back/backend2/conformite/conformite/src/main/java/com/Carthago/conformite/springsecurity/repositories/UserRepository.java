package com.Carthago.conformite.springsecurity.repositories;

import java.util.Optional;

import com.Carthago.conformite.springsecurity.entities.User;
import org.springframework.data.jdbc.repository.query.Query;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;



@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    @Query("select u from User u where u.username=?1 ")
    public Optional<User> findUserByUsername(String username);

    Optional<User> findByUsername(String username);

    Boolean existsByUsername(String username);

    Boolean existsByEmail(String email);
}
