package com.Carthago.conformite.compte.services;

import com.Carthago.conformite.compte.entities.compte;
import java.util.List;
import java.util.Optional;

public interface compteService {
    /**
     * Cette Methode permet d'enregistrer Compte.
     */
    void saveCompte(compte P);


    /**
     * Cette Methode permet d'enregistrer les modifications exercées sur CompteEpargne.
     */
    void updateCompte(long codeCompte, compte P);


    /**
     * Cette Methode permet de lister tous les CompteEpargne.
     */
    List<compte> listCompte();


    /**
     * Cette Methode permet de supprimer CompteEpargne.
     */
    void removeCompte(long codeCompte);


    /**
     * Cette Methode permet de trouver Compte par son codeCompte.
     */
     public compte findCompte(String codeCompte);

    /**
     * Cette Methode permet de trouver Compte par son codeCompte.
     */
    Optional<compte> findCompteCode(long codeCompte);


    /**
     * methode de credit
     */
    void crediter(compte c, double montant );
    /**
     * methide de débit
     */
    void debiter(compte c , double montant);
}
