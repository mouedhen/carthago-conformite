package com.Carthago.conformite.risque.serviceImp;
import com.Carthago.conformite.risque.entities.modeleCotationRisque;
import com.Carthago.conformite.risque.repositories.modeleCotationRisqueRepository;
import com.Carthago.conformite.risque.services.modeleCotationRisqueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;
@Service("modeleCotationRisqueService")
public class modeleCotationRisqueServiceImp implements modeleCotationRisqueService {
    @Autowired
    modeleCotationRisqueRepository RR;
    /**
     * Fonction qui enregistre le risque d'une transaction
     */
    @Override
    public void saveModele(modeleCotationRisque R) {
        RR.save(R);
    }
    @Override
    public void updateModele(modeleCotationRisque R) {
        Optional<modeleCotationRisque> m = RR.findById(R.getNum());
        RR.save(R);
    }
    @Override
    public  Optional<modeleCotationRisque> findModeleCotationRisque (Long id)
    {
        return RR.findById(id);
    }
    @Override
    public List<modeleCotationRisque> listModeleCotationRisque(){
        return RR.findAll();
    }
}