package com.Carthago.conformite.clients.services;

import com.Carthago.conformite.clients.entities.RepPers;
import com.Carthago.conformite.clients.entities.persNat;
import com.Carthago.conformite.clients.entities.personnePhysique;

import java.util.ArrayList;
import java.util.List;

public interface RepPersService {

    /**
     * Cette Methode permet d'enregistrer PersNat.
     */
    void saveRepPers(RepPers P);

    /**
     * Cette Methode permet de lister toutes les RepPers .
     */
    List<RepPers> listRepPers();


    /**
     * Cette Methode permet de lister toutes les representants
     * legales d'une personne morale.
     */
     List<RepPers> listRepPersCodeClient(Long code);

    /**
     * Cette Methode permet de retouner la liste des codes des repLeg
     */
    ArrayList<Long> listRepPersCode(long id);


    /**
     * chercher par codeClient
     */
    RepPers findByCodeClient(Long codeClient);
}
