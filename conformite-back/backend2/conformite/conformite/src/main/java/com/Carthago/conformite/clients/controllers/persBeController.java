package com.Carthago.conformite.clients.controllers;


import com.Carthago.conformite.clients.entities.persBe;
import com.Carthago.conformite.clients.entities.persNat;
import com.Carthago.conformite.clients.services.persBeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value="/persBe",method= {RequestMethod.GET})
public class persBeController {

    @Autowired
    persBeService PService ;
    /**
     * Cette methode de lister les PERSBE
     */
    @GetMapping("/GetAll")
    public List<persBe> getAllPersNat()
    {
        return PService.findAll();

    }


}
