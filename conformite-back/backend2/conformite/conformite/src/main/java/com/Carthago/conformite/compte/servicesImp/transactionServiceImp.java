package com.Carthago.conformite.compte.servicesImp;

import com.Carthago.conformite.clients.entities.contact;
import com.Carthago.conformite.clients.entities.personneMorale;
import com.Carthago.conformite.clients.entities.personnePhysique;
import com.Carthago.conformite.clients.entities.stat;
import com.Carthago.conformite.clients.services.contactService;
import com.Carthago.conformite.clients.services.personneMoraleService;
import com.Carthago.conformite.clients.services.personnePhysiqueService;
import com.Carthago.conformite.compte.entities.compte;
import com.Carthago.conformite.compte.entities.transaction;
import com.Carthago.conformite.compte.repositories.transactionRepository;
import com.Carthago.conformite.compte.services.compteCourantService;
import com.Carthago.conformite.compte.services.compteEpargneService;
import com.Carthago.conformite.compte.services.compteService;
import com.Carthago.conformite.compte.services.transactionService;
import com.Carthago.conformite.risque.entities.*;
import com.Carthago.conformite.risque.repositories.risqueTransactionRepository;
import com.Carthago.conformite.risque.serviceImp.paysServiceImp;
import com.Carthago.conformite.risque.services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.time.LocalDate;
import java.util.*;

import static java.time.temporal.ChronoUnit.*;

@Service("transactionServiceImpl")
public class transactionServiceImp  implements transactionService {
    @Autowired
    transactionRepository repository;

    @Autowired
    modeleRisquePersonneMoraleService service; //REPOSITORY DU COMPTE COURANT

    @Autowired
    modeleGlobalPhysiqueService serviceModele;
    @Autowired
    compteService serviceCompte;

    @Autowired
    paysServiceImp servicePays;

    @Autowired
    secteurService risque;
    @Autowired
    risqueService blackList;

    @Autowired
    personnePhysiqueService servicePersonne;

    @Autowired
    contactService serviceContact;

    @Autowired
    risqueTransactionService RService ;

    @Autowired
    risqueTransactionRepository RR;

    @Autowired
    personnePhysiqueService PService;

    @Autowired
    personneMoraleService MService;
    /**
     * C'est l'implementation de la methode qui permet d'enregistrer cet objet
     */
    @Override
    public void saveTransaction(transaction t) {
        repository.save(t);
    }



    /**
     * C'est l'implementation de la methode qui permet
     * d'enregitrer les modifications exercées sur cet objet
     */
    @Override
    public void updateTransaction
    (long codeC,transaction newTransaction) {
       Optional<transaction> d= repository.findById(codeC);
        newTransaction.setCompteTransaction(d.get().getCompteTransaction());
        repository.save(newTransaction);
    }


    /**
     * C'est l'implementation de la methode qui permet de lister tous ces objets
     */
    @Override
    public List<transaction> listTransactions(String ch)
    {List<transaction> l=repository.findAll();
        List<transaction> l1 = new ArrayList<>();
        for(int i=0;i<l.size();i++)
        {
            if(l.get(i).getRibEmetteur().equalsIgnoreCase(ch))
            {
                l1.add(l.get(i));
            }
        }
        return l1;
    }

    /**
     * C'est l'implementation de la methode qui permet de lister tous ces objets
     */
    @Override
    public   List<transaction> listeTransactionsDouteusesDD(String ch)
    {List<transaction> l=repository.findAll();
        List<transaction> l1 = new ArrayList<>();
        for(int i=0;i<l.size();i++)
        {
            if((l.get(i).getRibEmetteur().equalsIgnoreCase(ch))&&(l.get(i).getStatutTransaction().equalsIgnoreCase("En attente de validation ")))
            {
                l1.add(l.get(i));
            }
        }
        return l1;
    }
    @Override
    public List<transaction> listDesTransactions(){
        return repository.findAll();

    }


    /**
     * C'est l'implementation de la methode qui permet de supprimer cet objet
     */
    @Override
    public  void removeTransaction(long code){
        Optional<transaction> t=repository.findById(code);
        if(t.get().getTypeTransaction().equalsIgnoreCase("virement"))
        {
            compte c = serviceCompte.findCompte(t.get().getRibEmetteur());
            serviceCompte.crediter(c,t.get().getMontant());
            Optional <compte> c1 = Optional.ofNullable(serviceCompte.findCompte(t.get().getRibDestinataire()));
            if(!c1.isEmpty())
            {
             serviceCompte.debiter(c1.get(),t.get().getMontant());
            }
        }
        else if (t.get().getTypeTransaction().equalsIgnoreCase("versement"))
        {
            compte c = serviceCompte.findCompte(t.get().getRibEmetteur());
            serviceCompte.debiter(c,t.get().getMontant());
        }
        else
        {
            compte c = serviceCompte.findCompte(t.get().getRibEmetteur());
            serviceCompte.crediter(c,t.get().getMontant());
        }
        RR.deleteById(code);
        repository.deleteById(code);
    }



    /**
     * C'est l'implementation de la methode qui permet de trouver cet objet par son id
     */
    @Override
    public transaction findTransaction(String codeE) {
        transaction t=null;
        List<transaction> l=repository.findAll();
        for(int i=0;i<l.size();i++)
        {
           if(l.get(i).getRibEmetteur().equalsIgnoreCase(codeE))
           {
               t=l.get(i);

           }
        }
        return t;
    }

    public void traiterTransaction(long codeC,transaction newTransaction)
    {
        Optional<transaction> d= repository.findById(codeC);
        newTransaction.setCompteTransaction(d.get().getCompteTransaction());
        Optional<compte> cmp= Optional.ofNullable(d.get().getCompteTransaction());
        Optional<modeleGlobalPhysique> pers=serviceModele.find(cmp.get().getContactCompte().getCodeClient());
        Optional<modeleRisquePersonneMorale> persM=service.find(cmp.get().getContactCompte().getCodeClient());
        //mise a jour du risque :
        if((newTransaction.getStatutTransaction().equalsIgnoreCase("Douteuse Validée"))||(newTransaction.getStatutTransaction().equalsIgnoreCase("Refusée")))
        {
            if(!pers.isEmpty()){

                pers.get().setValeur(pers.get().getValeur()+1);
                pers.get().setNbreTransactionFrauduleuse(pers.get().getNbreTransactionFrauduleuse()+1);
                serviceModele.saveModeleGobale(pers.get());
            }
            if(!persM.isEmpty()){

                persM.get().setValeur(persM.get().getValeur()+1);
                persM.get().setNbreTransactionFrauduleuse(persM.get().getNbreTransactionFrauduleuse()+1);
                service.save(persM.get());
            }

        }
        /* repository.save(newTransaction);*/
        //confirmation de la transaction :
        if(newTransaction.getTypeTransaction().equalsIgnoreCase("prelevement"))
        {
            prelevement(newTransaction);
        }
        if(newTransaction.getTypeTransaction().equalsIgnoreCase("versement"))
        {
            versement(newTransaction);
        }
        if(newTransaction.getTypeTransaction().equalsIgnoreCase("virement"))
        {
            virement(newTransaction);
        }
    }

    /**
     * Cette Methode permet de verser un montant dans votre compte
     */
    @Override
    public void versement(transaction t) {
        Boolean trouve;
        t.setRibEmetteur(t.getCompteTransaction().getNumCompte().toString());
        String rib= t.getRibEmetteur();
        t.setDateTransaction(java.time.LocalDate.now());
        t.setTypeTransaction("versement");
        //risqueTransaction(t);
        if(t.getStatutTransaction().equalsIgnoreCase(""))
        {trouve= risqueTransaction(t);
        if((trouve==true))
        { t.setStatutTransaction("En attente de validation ");

        }
        else
        {   t.setStatutTransaction("Validée");}}

        repository.save(t);
        if((t.getStatutTransaction().equalsIgnoreCase("Validée"))||((t.getStatutTransaction().equalsIgnoreCase("Douteuse Validée"))))
        {   compte c = serviceCompte.findCompte(rib);
                serviceCompte.crediter(c,t.getMontant());}



    }


    @Override
    public Optional<compte> virement (transaction t)
        {
        t.setRibEmetteur(t.getCompteTransaction().getNumCompte().toString());
        String rib= t.getRibEmetteur();
        boolean trouve;
        String ribDestinataire=t.getRibDestinataire();
        Optional<compte> compt= null ;
        t.setTypeTransaction("virement");
        t.setDateTransaction(java.time.LocalDate.now());
            // Les risques du transactions :
            if(t.getStatutTransaction().equalsIgnoreCase(""))
            {trouve= risqueTransaction(t);
        trouve= risqueTransaction(t);
        if(trouve==true)
        { t.setStatutTransaction("En attente de validation "); }
        else
        {   t.setStatutTransaction("Validée");}}
            repository.save(t);
            if((t.getStatutTransaction().equalsIgnoreCase("Validée"))||((t.getStatutTransaction().equalsIgnoreCase("Douteuse Validée"))))
            {   compte c = serviceCompte.findCompte(rib);
            serviceCompte.debiter(c,t.getMontant());
            compt=serviceCompte.findCompteCode(Long.parseLong(ribDestinataire));
        if((compt.isPresent())&&((t.getNomBanque().equalsIgnoreCase("BFI"))||(t.getNomBanque().equalsIgnoreCase("bfi"))))
        {   compte c1;
            c1=compt.get();
            serviceCompte.crediter(c1,t.getMontant()); }}
            return compt;
         }



    @Override
    public void prelevement (transaction t )
    {   Boolean trouve=false;
        t.setRibEmetteur(t.getCompteTransaction().getNumCompte().toString());
        String rib= t.getRibEmetteur();
        t.setTypeTransaction("prelevement");
        t.setDateTransaction(java.time.LocalDate.now());
      // risqueTransaction(t);
        if(t.getStatutTransaction().equalsIgnoreCase(""))
        {trouve= risqueTransaction(t);
        // montant superieur a 5000 ou bien pays destinataire appartient au liste grise
        // ou listenoire ou bien le compte n'est pas actif plus qu'une annee
        if((trouve==true))
        { t.setStatutTransaction("En attente de validation ");}

        else
        {   t.setStatutTransaction("Validée");}}
        repository.save(t);
          if((t.getStatutTransaction().equalsIgnoreCase("Validée"))||((t.getStatutTransaction().equalsIgnoreCase("Douteuse Validée"))))
        {   compte c = serviceCompte.findCompte(rib);
            serviceCompte.debiter(c,t.getMontant());}

        }

    @Override
    public Boolean risqueTransaction(transaction t) {
        int i=0,i1=0,i2=0,i3=0,i4=0,i5=0,j1=0,j2=0;
        String ch,sh1,sh2;
        float res1,res2;
        risqueTransaction rt=new risqueTransaction();
        compte cmp=serviceCompte.findCompte(t.getRibEmetteur());
        Optional<personnePhysique> pers=PService.findPersonnePhysique(cmp.getContactCompte().getCodeClient());
        Optional<personneMorale> persM=MService.findPersonneMorale(cmp.getContactCompte().getCodeClient());
        boolean trouve=false,trouve1=false,trouve2=false,trouve3=false,trouve4=false,trouve5=false,t1=false,t2=false,res=false,bl=false;
        res1 =blackList.Blacklist(t.getNomEmetteur());
        if (res1>75)
        {
            res=true;
            rt.setPersonneE(true);
        }
        res2 =blackList.Blacklist(t.getNomDestinataire());
        if (res2>75)
        {
            res=true;
            rt.setPersonneD(true);
        }
        //pays destinataire :
        ch=t.getPaysDestinataire();
        if(ch!=null)
        {//PAYS APPARTIENT AU LISTE NOIRE
        List<pays> l=servicePays.findAllListeNoire();
        while((i<l.size())&&(trouve==false))
        {if(l.get(i).getPays().equalsIgnoreCase(ch))
        {trouve=true;}
        else {i++;}}
        //PAYS APPARTIENT AU LISTE GRISE
        List<pays> l1=servicePays.findAllListeGrise();
        while((i1<l1.size())&&(trouve1==false))
        {if(l1.get(i1).getPays().equalsIgnoreCase(ch))
        {trouve1=true;}
        else {i1++;}}
        //PAYS AVEC UN TAUX D ECONOMIE PARALLELE ELEVE
            i3=0;
        List<pays> l3=servicePays.findAllListeEconomieInformelle();
        while((i3<l3.size())&&(trouve3==false))
        {if(l3.get(i3).getPays().equalsIgnoreCase(ch))
        {trouve3=true;}
        else {i3++;}}
        //PAYS AVEC UN TAUX DE CORRUPTION ELEVE
            i4=0;
        List<listeCorruption> l4=servicePays.findAllListeCorruption();
        while((i4<l4.size())&&(trouve4==false))
        {if(l4.get(i4).getPays().equalsIgnoreCase(ch))
        {trouve4=true;}
        else {i4++;}}
         //PAYS PRODUCTEURS DU DROGUE
        List<pays> l5=servicePays.findAllListePaysProducteursDrogue();
        i5=0;
        while((i5<l5.size())&&(trouve5==false))
        {if(l5.get(i5).getPays().equalsIgnoreCase(ch))
        {trouve5=true;}
        else {i5++;}}
        //PAYS DU GUERRE
            i2=0;
        List<pays> l2=servicePays.findAllListePaysGeurre();
        while((i2<l2.size())&&(trouve2==false))
        {if(l2.get(i2).getPays().equalsIgnoreCase(ch))
        {trouve2=true;}
        else {i2++;}}
        // pays destinataire appartient au liste grise ou liste noire
        if((trouve==true)||(trouve3==true))
        { res=true;
        rt.setPaysBlackList(true);
        }
        //pays destinataire appartient au liste grise ou liste de corruption avec un secteur ou proffesion ayant un taux de risque
            if((trouve5==true)||(trouve2==true))
            {  Optional<contact> p= findCompte(t.getCompteTransaction().getNumCompte());
            if(p.isPresent())
            { sh1=p.get().getSecteurTravail();
              //sh2=p.get().getProfession();
                //liste des secteur à risque :
                List<secteur> l6=risque.findAllListeSecteur();
                while((j1<l6.size())&&(t1==false))
                {if(l6.get(j1).getNom().equalsIgnoreCase(sh1))
                {t1=true;}
                else {j1++;}}
                /* liste des profession à risque :
                List<secteur> l7=risque.findAllListeProfession();
                while((j2<l7.size())&&(t2==false))
                {if(l7.get(j2).getNom().equalsIgnoreCase(sh2))
                {t2=true;}
                else {j2++;}} */
                if(t1)
                { res=true;
                rt.setPaysSecteurSuspect(true);}}}}
        //le monatant ne doit pas depasser 5000
        if(!persM.isEmpty())
        { if(20000<t.getMontant())   {res=true; rt.setMontantMaximal(true);}}
        if(!pers.isEmpty())
        { if(5000<t.getMontant())   {res=true; rt.setMontantMaximal(true);}}
        List <transaction>client=listTransactions(t.getRibEmetteur());
        if(!client.isEmpty())
        {
            LocalDate date1 =java.time.LocalDate.now();
            LocalDate date2 =t.getDateTransaction();
            long daysBetween = DAYS.between(date2,date1);
            if(daysBetween>3){res=true; rt.setCompteInactif(true);}
        }
        RService.saveRisque(rt);

        return res; }

    @Override
    public Optional<contact> findCompte(long codeTransaction) {
        //String code= String.valueOf(codeTransaction);
        Optional<compte> c=serviceCompte.findCompteCode(codeTransaction);
        Optional<contact> p=serviceContact.findPersonne(c.get().getContactCompte().getCodeClient());
        return p;
    }

    @Override
    public List<transaction> listeTransactionsDouteuses(){
        List<transaction> l= repository.findAll();
        List<transaction> l1= new ArrayList<transaction>() ;
        for(int i=0 ; i<l.size();i++)
        {
            if(l.get(i).getStatutTransaction().equalsIgnoreCase("En attente de validation "))
            {
                l1.add(l.get(i));
            }
        }
        return l1;
    }

    @Override
    public List<transaction> listeTransactionsRefuse(String ch) {
        List<transaction> l=repository.findAll();
        List<transaction> l1 = new ArrayList<>();
        for(int i=0;i<l.size();i++)
        {
            if((l.get(i).getRibEmetteur().equalsIgnoreCase(ch))&&(l.get(i).getStatutTransaction().equalsIgnoreCase("Refusée")))
            {
                l1.add(l.get(i));
            }
        }
        return l1;
    }

    @Override
    public List<transaction> listeTransactionsDouteusesValides(String ch) {
        List<transaction> l=repository.findAll();
        List<transaction> l1 = new ArrayList<>();
        for(int i=0;i<l.size();i++)
        {
            if((l.get(i).getRibEmetteur().equalsIgnoreCase(ch))&&(l.get(i).getStatutTransaction().equalsIgnoreCase("Douteuse Validée")))
            {
                l1.add(l.get(i));
            }
        }
        return l1;

    }

    @Override
    public List<transaction> listeTransactionsValides(String ch) {
        List<transaction> l=repository.findAll();
        List<transaction> l1 = new ArrayList<>();
        for(int i=0;i<l.size();i++)
        {
            if((l.get(i).getRibEmetteur().equalsIgnoreCase(ch))&&(l.get(i).getStatutTransaction().equalsIgnoreCase("Validée")))
            {
                l1.add(l.get(i));
            }
        }
        return l1;
    }
    @Override
    public String findContactCompte (Long code){
        Optional<transaction> t = repository.findById(code);
        Long codeCompte=t.get().getCompteTransaction().getNumCompte();
        Optional<compte> c=serviceCompte.findCompteCode(codeCompte);
        String emailPersonne=c.get().getContactCompte().getEmail();
        return emailPersonne;
    }

    @Override
    public List<stat> nbreTransactions(Long code) {
        List<transaction> l=repository.findAll();
        List<stat>l1=new ArrayList<stat>();
        int c=0,c1=0,c2=0;
        for(int i=0;i<l.size();i++)
        {
            if(l.get(i).getCompteTransaction().getNumCompte()==code)
            {   if(l.get(i).getTypeTransaction().equalsIgnoreCase("versement")){
                   c++; }
                if(l.get(i).getTypeTransaction().equalsIgnoreCase("prelevement")){
                    c1++; }
                if(l.get(i).getTypeTransaction().equalsIgnoreCase("virement")){
                    c2++;
                } } }
        stat e=new stat();
        e.setName("versement");
        e.setValeur(c);
        stat e1=new stat();
        e1.setName("prelevement");
        e1.setValeur(c1);
        stat e2=new stat();
        e2.setName("virement");
        e2.setValeur(c2);
        l1.add(e); l1.add(e1); l1.add(e2);
        return l1;
    }

    @Override
    public List<stat> nbreTransactionsD() {
        List<transaction> l=repository.findAll();
        List<stat>l1=new ArrayList<stat>();
        int c=0,c1=0,c2=0,c3;
        for(int i=0;i<l.size();i++)
        {

            if(l.get(i).getTypeTransaction().equalsIgnoreCase("versement")){
                c++; }
            if(l.get(i).getTypeTransaction().equalsIgnoreCase("prelevement")){
                c1++; }
            if(l.get(i).getTypeTransaction().equalsIgnoreCase("virement")){
                c2++;
            } }
    stat e=new stat();
        e.setName("versement");
        e.setValeur(c);
    stat e1=new stat();
        e1.setName("prelevement");
        e1.setValeur(c1);
    stat e2=new stat();
        e2.setName("virement");
        e2.setValeur(c2);
        l1.add(e); l1.add(e1); l1.add(e2);
        return l1;
    }

    @Override
    public List<stat> nbreTransactionstype() {
        List<transaction> l=repository.findAll();
        List<stat>l1=new ArrayList<stat>();
        int c=0,c1=0,c2=0,c3=0;
        for(int i=0;i<l.size();i++)
        {

            if(l.get(i).getStatutTransaction().equalsIgnoreCase("Refusée")){
                c++; }
            if(l.get(i).getStatutTransaction().equalsIgnoreCase("Validée")){
                c1++; }
            if(l.get(i).getStatutTransaction().equalsIgnoreCase("Douteuse Validée")){
                c2++; }
            if(l.get(i).getStatutTransaction().equalsIgnoreCase("En attente de validation ")){
                c3++; }
        }
        stat e=new stat();
        e.setName("Refusée");
        e.setValeur(c);
        stat e1=new stat();
        e1.setName("Validée");
        e1.setValeur(c1);

        stat e2=new stat();
        e2.setName("Douteuse Validée");
        e2.setValeur(c2);
        stat e3=new stat();
        e3.setName("En attente de validation ");
        e3.setValeur(c3);

        l1.add(e); l1.add(e1); l1.add(e2); l1.add(e3);
        return l1;
    }

    @Override
    public List<stat> nbreTransactionsDN() {
        List<transaction> l=repository.findAll();
        List<stat>l1=new ArrayList<stat>();
        int c=0,c1=0,c2=0;
        for(int i=0;i<l.size();i++)
        {
            if(l.get(i).getStatutTransaction().equalsIgnoreCase("En attente de validation ")){ c++; }
        }
        stat e=new stat();
        e.setName("En attente");
        e.setValeur(c);
        stat e1=new stat();
        e1.setName("Nombre Total");
        e1.setValeur(l.size());
        l1.add(e); l1.add(e1);
        return l1;
    }

}
