package com.Carthago.conformite.compte.services;

import com.Carthago.conformite.compte.entities.compteEpargne;
import com.Carthago.conformite.compte.entities.transaction;

import java.util.List;
import java.util.Optional;

public interface compteEpargneService {


    /**
     * Cette Methode permet d'enregistrer CompteCourant.
     */
    void saveCompteEpargneClientMoral(compteEpargne P, long id  );
    /**
     * Cette Methode permet d'enregistrer CompteCourant.
     */
    void saveCompteEpargneClientPhysique(compteEpargne P, long id  );


    /**
     * Cette Methode permet d'enregistrer les modifications exercées sur CompteEpargne.
     */
    void updateCompteEpargne(long codeCompteEpargne,compteEpargne P);


    /**
     * Cette Methode permet de lister tous les CompteEpargne.
     */
    List<compteEpargne> listCompteEpargne();


    /**
     * Cette Methode permet de supprimer CompteEpargne.
     */
    void removeCompteEpargne(long codeCompteEpargne);


    /**
     * Cette Methode permet de trouver CompteEpargne par son codeCompteEpargne.
     */
    compteEpargne findCompteEpargne(String codeCompteEpargne);

    /**
     * methode de credit
     */
    void crediterEpargne(compteEpargne c,  double montant );
    /**
     * methide de débit
     */
    void debiterEpargne(compteEpargne c , double montant);
}
