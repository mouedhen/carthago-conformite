package com.Carthago.conformite.clients.repositories;

import com.Carthago.conformite.clients.entities.persNat;
import org.springframework.data.jpa.repository.JpaRepository;

public interface persNatRepository extends JpaRepository<persNat,Long> {
}
