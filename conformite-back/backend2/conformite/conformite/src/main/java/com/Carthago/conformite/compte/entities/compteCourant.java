package com.Carthago.conformite.compte.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
public class compteCourant extends compte{

private float autorisationDecouvert=1000;

//declaration getters and setters

    public float getAutorisationDecouvert() {
        return autorisationDecouvert;
    }

    public void setAutorisationDecouvert(float autorisationDecouvert) {
        this.autorisationDecouvert = autorisationDecouvert;
    }
}
