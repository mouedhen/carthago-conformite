package com.Carthago.conformite.compte.controllers;

import com.Carthago.conformite.Configuration.emailService;
import com.Carthago.conformite.clients.entities.contact;
import com.Carthago.conformite.clients.entities.stat;
import com.Carthago.conformite.compte.entities.compte;
import com.Carthago.conformite.compte.entities.transaction;
import com.Carthago.conformite.compte.repositories.transactionRepository;
import com.Carthago.conformite.compte.services.compteCourantService;
import com.Carthago.conformite.compte.services.compteEpargneService;
import com.Carthago.conformite.compte.services.transactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value="/transaction",method= {RequestMethod.GET})
public class transactionController {
    @Autowired
    transactionService service ;

    @Autowired
    emailService emailService ;

    @Autowired
    transactionRepository repository;
    @Autowired
    compteCourantService courantService;

    @Autowired
    compteEpargneService epargneService;

    @PostMapping("/sendEmail/{id}")
    public void sendEmail(@PathVariable Long id)
    {
        String email=service.findContactCompte(id);

        emailService.sendMail(email,"Transaction refusée","On a le regret de vous informer que votre transaction est refusée pour des raisons de sécurité financiére. Pour plus de détails veuillez contacter votre chef d'agence. Bonne Journée.");
    }

    /**
     * Cette methode permet la creation d'une transaction
     */
    @PostMapping("/Create")
    public String createRevenu
    (@Validated @RequestBody transaction t)
    {
        service.saveTransaction(t);
        return " cree";
    }
    /**
     * Cette methode de lister les revenus
     */
    @GetMapping("/GetAll/{id}")
    public List<transaction> getTransaction(@PathVariable String id)
    {
        return service.listTransactions(id);
    }

    /**
     * Cette methode de lister les revenus
     */
    @GetMapping("/GetAllDD/{id}")
    public List<transaction> getAllTransactionDD(@PathVariable String id)
    {
        return service.listeTransactionsDouteusesDD(id);
    }

    /**
     * Cette methode de lister les revenus
     */
    @GetMapping("/GetAllV/{id}")
    public List<transaction> getAllTransactionV(@PathVariable String id)
    {
        return service.listeTransactionsValides(id);
    }

    /**
     * Cette methode de lister les revenus
     */
    @GetMapping("/GetAllDV/{id}")
    public List<transaction> getAllTransactionDV(@PathVariable String id)
    {
        return service.listeTransactionsDouteusesValides(id);
    }

    /**
     * Cette methode de lister les revenus
     */
    @GetMapping("/GetAllR/{id}")
    public List<transaction> getAllTransactionR(@PathVariable String id)
    {
        return service.listeTransactionsRefuse(id);
    }

    /**
     * Cette methode de lister les types de transactions transactions
     */
    @GetMapping("/GetAllAnalyse/{id}")
    public List<stat> getAllTransactionAnalyse(@PathVariable String id)
    {
        List<stat> p = new ArrayList<stat>();
        int c=0,c1=0,c2=0,c3=0;
       c= service.listeTransactionsRefuse(id).size();
       c1=service.listeTransactionsDouteusesValides(id).size();
       c2=service.listeTransactionsDouteusesDD(id).size();
       c3=service.listeTransactionsValides(id).size();
        stat e=new stat();stat e1=new stat();stat e2=new stat();stat e3=new stat();
        e.setValeur(c);e.setName("Refuseé");
        e1.setValeur(c1);e1.setName("Douteuse Validéé");
        e2.setValeur(c2);e2.setName("Douteuse");
        e3.setValeur(c3);e3.setName("Validée");
        p.add(e);p.add(e1);p.add(e2);p.add(e3);
        return p;
    }

    /**
     * Cette methode de lister les types de transactions transactions
     */
    @GetMapping("/GetAllAnalyseDate/{id}")
    public List<stat> getAllTransactionAnalyse1(@PathVariable String id)
    {    List<transaction> l=repository.findAll();
         List<stat> p = new ArrayList<stat>();
        int c1=0,c2=0,c3=0,c4=0,c5=0,c6=0,c7=0,c8=0,c9=0,c10=0,c11=0,c12=0,month,annee;
        for(int i=0;i<l.size();i++)
        {
            if(l.get(i).getRibEmetteur().equalsIgnoreCase(id))
            {
               month= l.get(i).getDateTransaction().getMonth().getValue();
               annee=l.get(i).getDateTransaction().getYear();
               if(annee==java.time.LocalDate.now().getYear()){
                   if(month==1){c1++;}
                   if(month==2){c2++;}
                   if(month==3){c3++;}
                   if(month==4){c4++;}
                   if(month==5){c5++;}
                   if(month==6){c6++;}
                   if(month==7){c7++;}
                   if(month==8){c8++;}
                   if(month==9){c9++;}
                   if(month==10){c10++;}
                   if(month==11){c11++;}
                   if(month==12){c12++;}
               }
            }
        }
        stat e8=new stat();stat e9=new stat();stat e10=new stat();stat e11=new stat();
        stat e4=new stat();stat e5=new stat();stat e6=new stat();stat e7=new stat();
        stat e=new stat();stat e1=new stat();stat e2=new stat();stat e3=new stat();
        e.setValeur(c1);e.setName("Janvier");
        e1.setValeur(c2);e1.setName("Fevrier");
        e2.setValeur(c3);e2.setName("Mars");
        e3.setValeur(c4);e3.setName("Avril");
        e4.setValeur(c5);e4.setName("Mai");
        e5.setValeur(c6);e5.setName("Juin");
        e6.setValeur(c7);e6.setName("Juillet");
        e7.setValeur(c8);e7.setName("Aout");
        e8.setValeur(c9);e8.setName("Septembre");
        e9.setValeur(c10);e9.setName("Octobre");
        e10.setValeur(c11);e10.setName("Novembre");
        e11.setValeur(c12);e11.setName("Decembre");
        p.add(e);p.add(e1);p.add(e2);p.add(e3);
        p.add(e4);p.add(e5);p.add(e6);p.add(e7);
        p.add(e8);p.add(e9);p.add(e10);p.add(e11);
        return p;
    }

    /**
     * Cette methode permet de trouver revenu par son id
     */
    @GetMapping("/Get/{id}")
    public transaction findTransaction(@PathVariable String id)
    {
        return service.findTransaction(id);
    }
    /**
     * Cette methode permet de modifier revenu
     */
    @PutMapping("/Update/{id}")
    public String UpdateTransaction
    (@Validated @RequestBody transaction t,@PathVariable Long id)
    {
        service.updateTransaction(id,t);
        return "La mise a jour a ete faite avec succees";
    }
    /**
     * Cette methode permet de modifier traiter les transactions
     */
    @PutMapping("/Traiter/{id}")
    public String traiterTransaction
    (@Validated @RequestBody transaction t,@PathVariable Long id)
    {
        service.traiterTransaction(id,t);
        return "La mise a jour a ete faite avec succees"+t.getMontant()+"yes";
    }
    /**
     * Cette methode permet la suppression revenu par son id
     */
    @DeleteMapping(value="/Delete/{id}")
    public String deleteRevenu(@PathVariable String id)
    {
        service.removeTransaction (Long.parseLong(id));
        return "supprimee avec succee";
    }

    /**
     * Cette methode permet la creation d'une transaction
     */
    @PostMapping("/Versement")
    public String createVersement
    (@Validated @RequestBody transaction t)
    {
        service.versement(t);
        return " jawk behy";
    }


    @GetMapping("/GetAll")
    public List<transaction> getAllTransaction()
    {
        return service.listDesTransactions();
    }

    @PostMapping(value="/Virement")
    public Optional<compte> virement (@Validated @RequestBody transaction t )
    {


        return service.virement(t);
    }
    /**
     * prelevement
     */
    @PostMapping(value="/Prelevement")
    public String prelevement (@Validated @RequestBody transaction t )
    {
        service.prelevement(t);
        return "done";
    }
    @Autowired
    transactionService PService;

    /**
     * Cette methode de lister les parentes
     * @return
     */
    @GetMapping("/GetCompte/{id}")
    public Optional<contact> getAllParente  (@PathVariable long id)
    {
        return PService.findCompte(id);
    }

    /**
     * Cette methode de lister les parentes
     * @return
     */
    @GetMapping("/GetT/{id}")
    public Optional<transaction> gett  (@PathVariable long id)
    {
        return repository.findById(id);
    }

    /**
     * Cette methode de lister les nombres
     * @return
     */
    @GetMapping("/GetNbre/{id}")
    public List<stat> getnbre  (@PathVariable long id)
    {
        return PService.nbreTransactions(id);
    }

    /**
     * Cette méthode permet de lister les transactions douteuses
     */
    @GetMapping("/GetTransactionDouteuses")
    public List<transaction> listeTransactionsDouteuses(){
        return PService.listeTransactionsDouteuses();
    }

    @GetMapping("/GetCodeClient/{id}")
    public String findContactCompte (@PathVariable  Long id)
    {
        return PService.findContactCompte(id);
    }

    @GetMapping("/GetAllD")
    public List<stat> getAllTransactiond()
    {
        return service.nbreTransactionsD();
    }

    @GetMapping("/GetAllType")
    public List<stat> getAllT()
    {
        return service.nbreTransactionstype();
    }

    @GetMapping("/GetAllDN")
    public List<stat> getAllDN()
    {
        return service.nbreTransactionsDN();
    }



    /**
     * Cette methode de lister les types de transactions transactions
     */
        @GetMapping("/GetAllTranMois")
    public int getAllTran1()
    {    List<transaction> l=repository.findAll();
        int c1=0,month,annee;
        for(int i=0;i<l.size();i++)
        {
                month= l.get(i).getDateTransaction().getMonth().getValue();
                annee=l.get(i).getDateTransaction().getYear();
                if((annee==java.time.LocalDate.now().getYear())&&(month==java.time.LocalDate.now().getMonthValue())){
                   c1=c1+1;
            }
        } return c1;}

    /**
     * Cette methode de lister les types de transactions transactions
     */
    @GetMapping("/GetAllMars")
    public List<stat> getAllMars()
    {    List<transaction> l=repository.findAll();
        List<stat> p = new ArrayList<stat>();
        int c1=0,c2=0,c3=0,c4=0,month,annee;
        for(int i=0;i<l.size();i++)
        {
            if(l.get(i).getStatutTransaction().equalsIgnoreCase("Validée"))
            {
                month= l.get(i).getDateTransaction().getMonth().getValue();
                annee=l.get(i).getDateTransaction().getYear();
                if((annee==java.time.LocalDate.now().getYear())&&(month==3)){
                   c1=c1+1;
                }
            }
            if(l.get(i).getStatutTransaction().equalsIgnoreCase("Douteuse Validée"))
            {
                month= l.get(i).getDateTransaction().getMonth().getValue();
                annee=l.get(i).getDateTransaction().getYear();
                if((annee==java.time.LocalDate.now().getYear())&&(month==3)){
                    c2=c2+1;
                }
            }
            if(l.get(i).getStatutTransaction().equalsIgnoreCase("En attente de validation "))
            {
                month= l.get(i).getDateTransaction().getMonth().getValue();
                annee=l.get(i).getDateTransaction().getYear();
                if((annee==java.time.LocalDate.now().getYear())&&(month==3)){
                    c3=c3+1;
                }
            }
            if(l.get(i).getStatutTransaction().equalsIgnoreCase("Refusée"))
            {
                month= l.get(i).getDateTransaction().getMonth().getValue();
                annee=l.get(i).getDateTransaction().getYear();
                if((annee==java.time.LocalDate.now().getYear())&&(month==3)){
                    c4=c4+1;
                }
            }
        }

        stat e=new stat();stat e1=new stat();stat e2=new stat();stat e3=new stat();
        e.setValeur(c1);e.setName("Validée");
        e1.setValeur(c2);e1.setName("Douteuse Validée");
        e2.setValeur(c3);e2.setName("En attente");
        e3.setValeur(c4);e3.setName("Refusée");

        p.add(e);p.add(e1);p.add(e2);p.add(e3);

        return p;
    }
    /**
     * Cette methode de lister les types de transactions transactions
     */
    @GetMapping("/GetAllJanvier")
    public List<stat> getAlljanvier()
    {    List<transaction> l=repository.findAll();
        List<stat> p = new ArrayList<stat>();
        int c1=0,c2=0,c3=0,c4=0,month,annee;
        for(int i=0;i<l.size();i++)
        {
            if(l.get(i).getStatutTransaction().equalsIgnoreCase("Validée"))
            {
                month= l.get(i).getDateTransaction().getMonth().getValue();
                annee=l.get(i).getDateTransaction().getYear();
                if((annee==java.time.LocalDate.now().getYear())&&(month==1)){
                    c1=c1+1;
                }
            }
            if(l.get(i).getStatutTransaction().equalsIgnoreCase("Douteuse Validée"))
            {
                month= l.get(i).getDateTransaction().getMonth().getValue();
                annee=l.get(i).getDateTransaction().getYear();
                if((annee==java.time.LocalDate.now().getYear())&&(month==1)){
                    c2=c2+1;
                }
            }
            if(l.get(i).getStatutTransaction().equalsIgnoreCase("En attente de validation "))
            {
                month= l.get(i).getDateTransaction().getMonth().getValue();
                annee=l.get(i).getDateTransaction().getYear();
                if((annee==java.time.LocalDate.now().getYear())&&(month==1)){
                    c3=c3+1;
                }
            }
            if(l.get(i).getStatutTransaction().equalsIgnoreCase("Refusée"))
            {
                month= l.get(i).getDateTransaction().getMonth().getValue();
                annee=l.get(i).getDateTransaction().getYear();
                if((annee==java.time.LocalDate.now().getYear())&&(month==1)){
                    c4=c4+1;
                }
            }
        }

        stat e=new stat();stat e1=new stat();stat e2=new stat();stat e3=new stat();
        e.setValeur(c1);e.setName("Validée");
        e1.setValeur(c2);e1.setName("Douteuse Validée");
        e2.setValeur(c3);e2.setName("En attente");
        e3.setValeur(c4);e3.setName("Refusée");

        p.add(e);p.add(e1);p.add(e2);p.add(e3);

        return p;
    }
    /**
     * Cette methode de lister les types de transactions transactions
     */
    @GetMapping("/GetAllfeverier")
    public List<stat> getAllfevrier()
    {    List<transaction> l=repository.findAll();
        List<stat> p = new ArrayList<stat>();
        int c1=0,c2=0,c3=0,c4=0,month,annee;
        for(int i=0;i<l.size();i++)
        {
            if(l.get(i).getStatutTransaction().equalsIgnoreCase("Validée"))
            {
                month= l.get(i).getDateTransaction().getMonth().getValue();
                annee=l.get(i).getDateTransaction().getYear();
                if((annee==java.time.LocalDate.now().getYear())&&(month==2)){
                    c1=c1+1;
                }
            }
            if(l.get(i).getStatutTransaction().equalsIgnoreCase("Douteuse Validée"))
            {
                month= l.get(i).getDateTransaction().getMonth().getValue();
                annee=l.get(i).getDateTransaction().getYear();
                if((annee==java.time.LocalDate.now().getYear())&&(month==2)){
                    c2=c2+1;
                }
            }
            if(l.get(i).getStatutTransaction().equalsIgnoreCase("En attente de validation "))
            {
                month= l.get(i).getDateTransaction().getMonth().getValue();
                annee=l.get(i).getDateTransaction().getYear();
                if((annee==java.time.LocalDate.now().getYear())&&(month==2)){
                    c3=c3+1;
                }
            }
            if(l.get(i).getStatutTransaction().equalsIgnoreCase("Refusée"))
            {
                month= l.get(i).getDateTransaction().getMonth().getValue();
                annee=l.get(i).getDateTransaction().getYear();
                if((annee==java.time.LocalDate.now().getYear())&&(month==2)){
                    c4=c4+1;
                }
            }
        }

        stat e=new stat();stat e1=new stat();stat e2=new stat();stat e3=new stat();
        e.setValeur(c1);e.setName("Validée");
        e1.setValeur(c2);e1.setName("Douteuse Validée");
        e2.setValeur(c3);e2.setName("En attente");
        e3.setValeur(c4);e3.setName("Refusée");

        p.add(e);p.add(e1);p.add(e2);p.add(e3);

        return p;
    }
    /**
     * Cette methode de lister les types de transactions transactions
     */
    @GetMapping("/GetAllAvril")
    public List<stat> getAllAvril()
    {    List<transaction> l=repository.findAll();
        List<stat> p = new ArrayList<stat>();
        int c1=0,c2=0,c3=0,c4=0,month,annee;
        for(int i=0;i<l.size();i++)
        {
            if(l.get(i).getStatutTransaction().equalsIgnoreCase("Validée"))
            {
                month= l.get(i).getDateTransaction().getMonth().getValue();
                annee=l.get(i).getDateTransaction().getYear();
                if((annee==java.time.LocalDate.now().getYear())&&(month==4)){
                    c1=c1+1;
                }
            }
            if(l.get(i).getStatutTransaction().equalsIgnoreCase("Douteuse Validée"))
            {
                month= l.get(i).getDateTransaction().getMonth().getValue();
                annee=l.get(i).getDateTransaction().getYear();
                if((annee==java.time.LocalDate.now().getYear())&&(month==4)){
                    c2=c2+1;
                }
            }
            if(l.get(i).getStatutTransaction().equalsIgnoreCase("En attente de validation "))
            {
                month= l.get(i).getDateTransaction().getMonth().getValue();
                annee=l.get(i).getDateTransaction().getYear();
                if((annee==java.time.LocalDate.now().getYear())&&(month==4)){
                    c3=c3+1;
                }
            }
            if(l.get(i).getStatutTransaction().equalsIgnoreCase("Refusée"))
            {
                month= l.get(i).getDateTransaction().getMonth().getValue();
                annee=l.get(i).getDateTransaction().getYear();
                if((annee==java.time.LocalDate.now().getYear())&&(month==4)){
                    c4=c4+1;
                }
            }
        }

        stat e=new stat();stat e1=new stat();stat e2=new stat();stat e3=new stat();
        e.setValeur(c1);e.setName("Validée");
        e1.setValeur(c2);e1.setName("Douteuse Validée");
        e2.setValeur(c3);e2.setName("En attente");
        e3.setValeur(c4);e3.setName("Refusée");

        p.add(e);p.add(e1);p.add(e2);p.add(e3);

        return p;
    }
    /**
     * Cette methode de lister les types de transactions transactions
     */
    @GetMapping("/GetAllJuin")
    public List<stat> getAlljuin()
    {    List<transaction> l=repository.findAll();
        List<stat> p = new ArrayList<stat>();
        int c1=0,c2=0,c3=0,c4=0,month,annee;
        for(int i=0;i<l.size();i++)
        {
            if(l.get(i).getStatutTransaction().equalsIgnoreCase("Validée"))
            {
                month= l.get(i).getDateTransaction().getMonth().getValue();
                annee=l.get(i).getDateTransaction().getYear();
                if((annee==java.time.LocalDate.now().getYear())&&(month==6)){
                    c1=c1+1;
                }
            }
            if(l.get(i).getStatutTransaction().equalsIgnoreCase("Douteuse Validée"))
            {
                month= l.get(i).getDateTransaction().getMonth().getValue();
                annee=l.get(i).getDateTransaction().getYear();
                if((annee==java.time.LocalDate.now().getYear())&&(month==6)){
                    c2=c2+1;
                }
            }
            if(l.get(i).getStatutTransaction().equalsIgnoreCase("En attente de validation "))
            {
                month= l.get(i).getDateTransaction().getMonth().getValue();
                annee=l.get(i).getDateTransaction().getYear();
                if((annee==java.time.LocalDate.now().getYear())&&(month==6)){
                    c3=c3+1;
                }
            }
            if(l.get(i).getStatutTransaction().equalsIgnoreCase("Refusée"))
            {
                month= l.get(i).getDateTransaction().getMonth().getValue();
                annee=l.get(i).getDateTransaction().getYear();
                if((annee==java.time.LocalDate.now().getYear())&&(month==6)){
                    c4=c4+1;
                }
            }
        }

        stat e=new stat();stat e1=new stat();stat e2=new stat();stat e3=new stat();
        e.setValeur(c1);e.setName("Validée");
        e1.setValeur(c2);e1.setName("Douteuse Validée");
        e2.setValeur(c3);e2.setName("En attente");
        e3.setValeur(c4);e3.setName("Refusée");

        p.add(e);p.add(e1);p.add(e2);p.add(e3);

        return p;
    }
    /**
     * Cette methode de lister les types de transactions transactions
     */
    @GetMapping("/GetAllMai")
    public List<stat> getAllMai()
    {    List<transaction> l=repository.findAll();
        List<stat> p = new ArrayList<stat>();
        int c1=0,c2=0,c3=0,c4=0,month,annee;
        for(int i=0;i<l.size();i++)
        {
            if(l.get(i).getStatutTransaction().equalsIgnoreCase("Validée"))
            {
                month= l.get(i).getDateTransaction().getMonth().getValue();
                annee=l.get(i).getDateTransaction().getYear();
                if((annee==java.time.LocalDate.now().getYear())&&(month==5)){
                    c1=c1+1;
                }
            }
            if(l.get(i).getStatutTransaction().equalsIgnoreCase("Douteuse Validée"))
            {
                month= l.get(i).getDateTransaction().getMonth().getValue();
                annee=l.get(i).getDateTransaction().getYear();
                if((annee==java.time.LocalDate.now().getYear())&&(month==5)){
                    c2=c2+1;
                }
            }
            if(l.get(i).getStatutTransaction().equalsIgnoreCase("En attente de validation "))
            {
                month= l.get(i).getDateTransaction().getMonth().getValue();
                annee=l.get(i).getDateTransaction().getYear();
                if((annee==java.time.LocalDate.now().getYear())&&(month==5)){
                    c3=c3+1;
                }
            }
            if(l.get(i).getStatutTransaction().equalsIgnoreCase("Refusée"))
            {
                month= l.get(i).getDateTransaction().getMonth().getValue();
                annee=l.get(i).getDateTransaction().getYear();
                if((annee==java.time.LocalDate.now().getYear())&&(month==5)){
                    c4=c4+1;
                }
            }
        }

        stat e=new stat();stat e1=new stat();stat e2=new stat();stat e3=new stat();
        e.setValeur(c1);e.setName("Validée");
        e1.setValeur(c2);e1.setName("Douteuse Validée");
        e2.setValeur(c3);e2.setName("En attente");
        e3.setValeur(c4);e3.setName("Refusée");

        p.add(e);p.add(e1);p.add(e2);p.add(e3);

        return p;
    }
    /**
     * Cette methode de lister les types de transactions transactions
     */
    @GetMapping("/GetAllJuillet")
    public List<stat> getAlljuillet()
    {    List<transaction> l=repository.findAll();
        List<stat> p = new ArrayList<stat>();
        int c1=0,c2=0,c3=0,c4=0,month,annee;
        for(int i=0;i<l.size();i++)
        {
            if(l.get(i).getStatutTransaction().equalsIgnoreCase("Validée"))
            {
                month= l.get(i).getDateTransaction().getMonth().getValue();
                annee=l.get(i).getDateTransaction().getYear();
                if((annee==java.time.LocalDate.now().getYear())&&(month==7)){
                    c1=c1+1;
                }
            }
            if(l.get(i).getStatutTransaction().equalsIgnoreCase("Douteuse Validée"))
            {
                month= l.get(i).getDateTransaction().getMonth().getValue();
                annee=l.get(i).getDateTransaction().getYear();
                if((annee==java.time.LocalDate.now().getYear())&&(month==7)){
                    c2=c2+1;
                }
            }
            if(l.get(i).getStatutTransaction().equalsIgnoreCase("En attente de validation "))
            {
                month= l.get(i).getDateTransaction().getMonth().getValue();
                annee=l.get(i).getDateTransaction().getYear();
                if((annee==java.time.LocalDate.now().getYear())&&(month==7)){
                    c3=c3+1;
                }
            }
            if(l.get(i).getStatutTransaction().equalsIgnoreCase("Refusée"))
            {
                month= l.get(i).getDateTransaction().getMonth().getValue();
                annee=l.get(i).getDateTransaction().getYear();
                if((annee==java.time.LocalDate.now().getYear())&&(month==7)){
                    c4=c4+1;
                }
            }
        }

        stat e=new stat();stat e1=new stat();stat e2=new stat();stat e3=new stat();
        e.setValeur(c1);e.setName("Validée");
        e1.setValeur(c2);e1.setName("Douteuse Validée");
        e2.setValeur(c3);e2.setName("En attente");
        e3.setValeur(c4);e3.setName("Refusée");

        p.add(e);p.add(e1);p.add(e2);p.add(e3);

        return p;
    }


    /**
     * Cette methode de lister les types de transactions transactions
     */
    @GetMapping("/GetAllCompte")
    public List<stat> getAllM()
    {    List<transaction> l=repository.findAll();
        List<stat> p = new ArrayList<stat>();
        int c1=0,c2=0,c3=0,c4=0,month,annee;
        c1=courantService.listCompteCourant().size();
        c2=epargneService.listCompteEpargne().size();
        stat e=new stat();stat e1=new stat();
        e.setValeur(c1);e.setName("Courant");
        e1.setValeur(c2);e1.setName("Epargne");


        p.add(e);p.add(e1);

        return p;
    }
}
