package com.Carthago.conformite.risque.repositories;

import com.Carthago.conformite.risque.entities.risque;
import com.Carthago.conformite.risque.entities.risquePersonneMorale;
import org.springframework.data.jpa.repository.JpaRepository;

public interface risquePersonneMoraleRepository extends JpaRepository<risquePersonneMorale,Long> {
}
