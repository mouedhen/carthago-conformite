package com.Carthago.conformite.clients.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.sql.Date;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity

public class personnePhysique extends contact {

    //declaration des variables
    private String nom=""; //nom personne physique
    private String prenom=""; //prenom personne physique
    private Date dateNaissance; //date naissance de la personne physique
    private Boolean usResident=false; //true si la personne physique reside de facon permanente
    private String numPassport=""; //numero du passport
    public  Long numCin; //numero de carte d'identite de la personne physique
    private String carteSejour=""; //numero carte sejour
    private String etatCivil=""; //etat civil
    private String sexe=""; //sexe
    private String capaciteJuridique=""; //capacite juridique
    private String catSocioprofessionnelle=""; // categorie socioprofessionnelle
    private String profession=""; //profession de la personne physique
    private String categorieEmployeur="";  //Catégorie Employeur
    private String contratTravail=""; //Contrat
    private String paysTravail=""; //pays du travail
    private String paysNaissance=""; //pays de la naissance

    private Date dateExtraitRne;  //date extrait RNE
    private String regimeFiscal=""; //regime fiscal

    private String codeDouane=""; //code Douane
    private String affSociale=""; //affiliation sociale
    private Long numAff;//numero affiliation
    private Boolean parentePep=false; //si quelqu'un de sa famille est PE
    private Boolean statutPep=false; //s'il est politiquement expose
    private Boolean carteVerte=false; //si la personne detient une carte verte
    private Boolean statutFatca=false; //si la personne doit payer des taxes à US
    private String risque="";//le niveau du risque de chaque client
    private Boolean intermediaire=false;//prend true si le compte est ouvert
    //par intermediaire

    //getters and setters

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public Date getDateNaissance() {
        return dateNaissance;
    }

    public void setDateNaissance(Date dateNaissance) {
        this.dateNaissance = dateNaissance;
    }

    public Boolean getUsResident() {
        return usResident;
    }

    public void setUsResident(Boolean usResident) {
        this.usResident = usResident;
    }

    public String getNumPassport() {
        return numPassport;
    }

    public void setNumPassport(String numPassport) {
        this.numPassport = numPassport;
    }

    public Long getNumCin() {
        return numCin;
    }

    public void setNumCin(Long numCin) {
        this.numCin = numCin;
    }

    public String getCarteSejour() {
        return carteSejour;
    }

    public void setCarteSejour(String carteSejour) {
        this.carteSejour = carteSejour;
    }

    public String getEtatCivil() {
        return etatCivil;
    }

    public void setEtatCivil(String etatCivil) {
        this.etatCivil = etatCivil;
    }

    public String getSexe() {
        return sexe;
    }

    public void setSexe(String sexe) {
        this.sexe = sexe;
    }

    public String getCapaciteJuridique() {
        return capaciteJuridique;
    }

    public void setCapaciteJuridique(String capaciteJuridique) {
        this.capaciteJuridique = capaciteJuridique;
    }

    public String getCatSocioprofessionnelle() {
        return catSocioprofessionnelle;
    }

    public void setCatSocioprofessionnelle(String catSocioprofessionnelle) {
        this.catSocioprofessionnelle = catSocioprofessionnelle;
    }

    public String getProfession() {
        return profession;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }

    public String getCategorieEmployeur() {
        return categorieEmployeur;
    }

    public void setCategorieEmployeur(String categorieEmployeur) {
        this.categorieEmployeur = categorieEmployeur;
    }


    public String getContratTravail() {
        return contratTravail;
    }

    public void setContratTravail(String contratTravail) {
        this.contratTravail = contratTravail;
    }

    public String getPaysTravail() {
        return paysTravail;
    }

    public void setPaysTravail(String paysTravail) {
        this.paysTravail = paysTravail;
    }

    public String getPaysNaissance() {
        return paysNaissance;
    }

    public void setPaysNaissance(String paysNaissance) {
        this.paysNaissance = paysNaissance;
    }



    public Date getDateExtraitRne() {
        return dateExtraitRne;
    }

    public void setDateExtraitRne(Date dateExtraitRne) {
        this.dateExtraitRne = dateExtraitRne;
    }

    public String getRegimeFiscal() {
        return regimeFiscal;
    }

    public void setRegimeFiscal(String regimeFiscal) {
        this.regimeFiscal = regimeFiscal;
    }



    public String getCodeDouane() {
        return codeDouane;
    }

    public void setCodeDouane(String codeDouane) {
        this.codeDouane = codeDouane;
    }

    public String getAffSociale() {
        return affSociale;
    }

    public void setAffSociale(String affSociale) {
        this.affSociale = affSociale;
    }

    public Long getNumAff() {
        return numAff;
    }

    public void setNumAff(Long numAff) {
        this.numAff = numAff;
    }

    public Boolean getParentePep() {
        return parentePep;
    }

    public void setParentePep(Boolean parentePep) {
        this.parentePep = parentePep;
    }

    public Boolean getStatutPep() {
        return statutPep;
    }

    public void setStatutPep(Boolean statutPep) {
        this.statutPep = statutPep;
    }

    public Boolean getCarteVerte() {
        return carteVerte;
    }

    public void setCarteVerte(Boolean carteVerte) {
        this.carteVerte = carteVerte;
    }

    public Boolean getStatutFatca() {
        return statutFatca;
    }

    public void setStatutFatca(Boolean statutFatca) {
        this.statutFatca = statutFatca;
    }

    public List<revenu> getListRevenu() {
        return listRevenu;
    }

    public void setListRevenu(List<revenu> listRevenu) {
        this.listRevenu = listRevenu;
    }


    public List<parente> getListPersPar() {
        return listPersPar;
    }

    public void setListPersPar(List<parente> listPersPar) {
        this.listPersPar = listPersPar;
    }

    public List<persNat> getListPersNat1() {
        return listPersNat1;
    }

    public void setListPersNat1(List<persNat> listPersNat1) {
        this.listPersNat1 = listPersNat1;
    }

    //constructeurs
    public personnePhysique(Long codeClient) {
        super(codeClient);
    }

    public personnePhysique(Long codeClient, String nom, String prenom) {
        super(codeClient);
        this.nom = nom;
        this.prenom = prenom;
    }

    //relation entre personne physique et revenu
    @OneToMany(mappedBy = "persPhy", cascade = CascadeType.ALL)
    private List<revenu> listRevenu;

    //Relation entre persNat et personnePhysique
    @OneToMany(mappedBy = "pers1",cascade = CascadeType.ALL)
    private List<persNat> listPersNat1;

    //Relation entre persPar et personne physique
    @OneToMany(mappedBy = "perPhy",cascade = CascadeType.ALL)
    private List<parente> listPersPar ;


}
