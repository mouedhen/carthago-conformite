package com.Carthago.conformite.risque.repositories;

import com.Carthago.conformite.risque.entities.modeleRisquePersonneMorale;
import org.springframework.data.jpa.repository.JpaRepository;

public interface modeleRisquePersonneMoraleRepository extends JpaRepository<modeleRisquePersonneMorale,Long> {
}
