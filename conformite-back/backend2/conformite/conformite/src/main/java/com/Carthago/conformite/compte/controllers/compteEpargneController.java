package com.Carthago.conformite.compte.controllers;

import com.Carthago.conformite.compte.entities.compteEpargne;
import com.Carthago.conformite.compte.services.compteEpargneService;
import com.Carthago.conformite.risque.services.RisquePersonneMoraleService;
import com.Carthago.conformite.risque.services.risqueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value="/compteEpargne",method= {RequestMethod.GET})
public class compteEpargneController {

    @Autowired
    compteEpargneService PService;

    @Autowired
    risqueService s;

    /**
     * Cette methode permet la creation de compte courant pour un client moral
     */
    @PostMapping("/CreateCompteClientMoral/{id}")
    public String createCompteCourantClienMoral
    (@Validated @RequestBody compteEpargne P, @PathVariable Long id)
    {
        PService.saveCompteEpargneClientMoral(P,id);
        return "cree";
    }
    /**
     * Cette methode permet la creation de compte courant pour un client moral
     */
    @PostMapping("/CreateCompteClientPhysique/{id}")
    public String createCompteCourantClientPhysique
    (@Validated @RequestBody compteEpargne P,@PathVariable Long id)
    {
        PService.saveCompteEpargneClientPhysique(P,id);
        return "cree";
    }


    /**
     * Cette methode de lister les compteEpargne
     */
    @GetMapping("/GetAll")
    public List<compteEpargne> getAllParente()
    {
        return PService.listCompteEpargne();

    }

    /**
     * Cette methode permet de trouver Parente par son id
     */
    @GetMapping("/Get/{id}")
    public compteEpargne findParente
    (@PathVariable String id)
    {

        return PService.findCompteEpargne(id);
    }

    /**
     * Cette methode permet de modifier Parente
     */
    @PutMapping("/Update/{id}")
    public String UpdateParente
    (@Validated @RequestBody compteEpargne P,@PathVariable Long id)
    {
        PService.updateCompteEpargne(id,P);
        return "La mise a jour a ete faite avec succees";
    }


    /**
     * Cette methode permet la suppression compteCourant par son id
     */
    @DeleteMapping(value="/Delete/{id}")
    public String deleteParente(@PathVariable String id)
    {
        PService.removeCompteEpargne (Long.parseLong(id));
        return "compteCourant supprimee avec succee";
    }


}
