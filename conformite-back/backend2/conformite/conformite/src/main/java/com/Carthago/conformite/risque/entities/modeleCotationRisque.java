package com.Carthago.conformite.risque.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
public class modeleCotationRisque {
    //declaration des variables
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long num; //NUMERO DE MODELe
    private int noteListeGrise;
    private int noteListeNoire;
    private int notePaysCorruption;
    private int notePaysEconomieParallele;
    private int notePaysGuerre;
    private int notePaysDrogues;
    private int notePepPersonnePhysique;
    private int notePepPersonneMorale;
    //PersonneMorale
    private int noteNature;
    private int noteParticipationPub;
    private int noteOffshore;
    private int notePaysOrigineFonds;
    private int noteOrigineFonds ;
    private int noteMarcheBoursier;
    private int noteAssOffshore;
    private int ong;
    private int noteChiffreAffaire;
    private int noteBlackListePP;
    //PersonnePhysique
    private int residence;
    private int intermediaire;
    private int noteBlackListePM;
    //Paramétrage niveau risque
    private int noteRisqueFaible;
    private int noteRisqueMoyennementFaible;
    private int noteRisqueMoyennementEleve;
    private int noteRisqueEleve ;
    //Paramétrage secteur
    private int noteSecteur;
    private int noteSecteur1 ;
    private int noteSecteur2 ;
    private int noteSecteur3;
    //Paramétrage profession
    private int noteProfession;
    private int noteProfession1 ;
    private int noteProfession2 ;
    private int noteProfession3;
}
