package com.Carthago.conformite.fatca.repositories;

import com.Carthago.conformite.fatca.entities.justificatif;
import org.springframework.data.jpa.repository.JpaRepository;

public interface justificatifRepository extends JpaRepository<justificatif,Long> {
}
