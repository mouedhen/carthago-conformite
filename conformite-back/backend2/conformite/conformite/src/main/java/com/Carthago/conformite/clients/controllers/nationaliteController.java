package com.Carthago.conformite.clients.controllers;

import com.Carthago.conformite.clients.entities.nationalite;
import com.Carthago.conformite.clients.services.nationaliteService;
import com.Carthago.conformite.clients.services.persNatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value="/nationalite",method= {RequestMethod.GET})

public class nationaliteController {

    @Autowired
    nationaliteService NService;

    @Autowired
    persNatService p ;
    /**
     * Cette methode permet de trouver une nationalite par son id
     */
    @GetMapping("/Get/{id}")
    public List<nationalite> findNationalite
    (@PathVariable Long id)
    {
        List<Long> l=p.listPNCode(id);
        List<nationalite> l1=NService.listNationalite();
        List<nationalite> l2=new ArrayList<nationalite>();
        for(int i=0;i<l.size();i++)
        {
            for(int j=0;j<l1.size();j++)
            {
                if(l.get(i)==l1.get(j).getCodeNationalite()){
                    l2.add(l1.get(j));}
            }
        }

        return l2;
    }

    /**
     * Cette methode permet la creation d'une nationalite
     */
    @PostMapping("/Create")
    public String createNationalite
    (@Validated @RequestBody nationalite N)
    {
        NService.saveNationalite(N);
        return "Nationalite cree";
    }


    /**
     * Cette methode de lister les nationalites
     */
    @GetMapping("/GetAll")
    public List<nationalite> getAllNationalite()
    {
        return NService.listNationalite();

    }



    /**
     * Cette methode permet de modifier une nationalite
     */
    @PutMapping("/Update/{id}")
    public String UpdateNationalite
    (@Validated @RequestBody nationalite N, @PathVariable Long id)
    {
        NService.updateNationalite(id,N);
        return "La mise a jour a ete faite avec succees";
    }


    /**
     * Cette methode permet la suppression d'une nationalite par son id
     */
    @DeleteMapping(value="/Delete/{id}")
    public String deleteNationalite(@PathVariable String id)
    {
        NService.removeNationalite (Long.parseLong(id));
        return "Nationalite  supprimee avec succee";
    }




}
