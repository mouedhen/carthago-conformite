package com.Carthago.conformite.clients.controllers;

import com.Carthago.conformite.clients.entities.personneMorale;
import com.Carthago.conformite.clients.entities.personnePhysique;
import com.Carthago.conformite.clients.repositories.personneMoraleRepository;
import com.Carthago.conformite.clients.services.*;
import com.Carthago.conformite.fatca.entities.fatca;
import com.Carthago.conformite.fatca.services.fatcaService;
import com.Carthago.conformite.risque.entities.modeleCotationRisque;
import com.Carthago.conformite.risque.entities.modeleRisquePersonneMorale;
import com.Carthago.conformite.risque.entities.risquePersonneMorale;
import com.Carthago.conformite.risque.services.RisquePersonneMoraleService;
import com.Carthago.conformite.risque.services.modeleCotationRisqueService;
import com.Carthago.conformite.risque.services.modeleRisquePersonneMoraleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
@RestController
@RequestMapping(value="/personneMorale",method= {RequestMethod.GET})
public class personneMoraleController {
    @Autowired
    personneMoraleService PService;
    @Autowired
    fatcaService fService;
    @Autowired
    RisquePersonneMoraleService RisqueService;
    @Autowired
    modeleRisquePersonneMoraleService mservice;
    @Autowired
    modeleCotationRisqueService modeleService;
    @Autowired
    RepPersService repService ;
    @Autowired
    representantLegalService serviceRepresentant;
    @Autowired
    persBeService beService ;
    @Autowired
    beneficiaireEffectifService serviceBeneficiaire;
    @Autowired
    personneMoraleRepository repository ;
    /**
     * Cette methode permet la creation de personneMorale
     */
    @PostMapping("/Create")
    public String createPersonneMorale
    (@Validated @RequestBody personneMorale P)
    {

        PService.savePersonneMorale(P);
        return "PersonneMorale cree";
    }
    /**
     * Cette methode de lister les personneMorale
     */
    @GetMapping("/GetAll")
    public List<personneMorale> getAllPersonneMorale()
    {
        return PService.listPersonneMorale();
        /*List<personneMorale> l=PService.listPersonneMorale();
        List<personneMorale> p = new ArrayList<personneMorale>();
        personneMorale pers;
        for(int i=0;i<l.size();i++)
        {
            pers=new personneMorale();
            pers.setCodeClient(l.get(i).getCodeClient());
            pers.setStatutFatca(l.get(i).getStatutFatca());
            pers.setRevue((l.get(i).getRevue()));
            pers.setNiveauRisque((l.get(i).getNiveauRisque()));
            pers.setFormeJuridique((l.get(i).getFormeJuridique()));
            pers.setNature((l.get(i).getNature()));
            p.add(pers);
        }
        return p;*/
    }
    /**
     * Cette methode permet de trouver PersonneMoralepar son id
     */
    @GetMapping("/Get/{id}")
    public Optional<personneMorale>findPersonneMorale
    (@PathVariable Long id)
    { /*
    Optional<personneMorale> p= PService.findPersonneMorale(id);
       Optional<personneMorale> pers= Optional.of(new personneMorale());
        pers.get().setCodeClient(p.get().getCodeClient());
        pers.get().setStatutFatca(p.get().getStatutFatca());
        pers.get().setRevue((p.get().getRevue()));
        pers.get().setNiveauRisque((p.get().getNiveauRisque()));
        pers.get().setFormeJuridique((p.get().getFormeJuridique()));
        pers.get().setNature((p.get().getNature()));
        return pers;*/
        return  PService.findPersonneMorale(id);
    }


    /**
     * Cette methode permet de modifier PersonneMorale
     */
    @PutMapping("/Update/{id}")
    public int UpdatePersonneMorale
    (@PathVariable long id,@Validated @RequestBody  personneMorale P)
    {   int n=0 ;

        Optional<modeleCotationRisque> modele = modeleService.findModeleCotationRisque((long) 1);
        Optional<personneMorale> oldPersonneMorale=PService.findPersonneMorale(id);
        P.setBeneficiaireEffectifList(oldPersonneMorale.get().getBeneficiaireEffectifList());
        P.setRepresentants(oldPersonneMorale.get().getRepresentants());
        P.setNumeroRne(id);

        //detection statut fatca
        fatca f = new fatca();
        f.setCodeFatca(id);
        if((P.getGiin()==true )
                ||(P.getUs()==true)
                || (P.isCodeTin()==true)
                ||(P.getPaysConstruction().equalsIgnoreCase("Etat_Unis"))
                ||(P.getPaysResidence().equalsIgnoreCase("Etat_Unis"))||(P.getActionnaireUs()==true))
        {
            P.setStatutFatca(true);
            f.setStatut(true);
            fService.saveFatca(f);
        }
        else {
            P.setStatutFatca(false);
            fService.saveFatca(f);
        }
   /* if(P.getStatutPersonne()!=null)
    {if(P.getStatutPersonne().equalsIgnoreCase("Client"))
    {   calculerRisque(P,id);
        PService.updatePersonneMorale(id,P);
    }}*/
        //Calcul du risque
        n=RisqueService.risqueNature(P);
        n=n+RisqueService.risqueGeographique(id,P);
        risquePersonneMorale r=new risquePersonneMorale();
        r.setCodeRisque(id);
        if(n<=modele.get().getNoteRisqueFaible())
        {
            r.setStatut("Faible");
            P.setNiveauRisque("Faible");
            P.setRevue("Chaque 2 ans");
            P.setStatutPersonne("En attente de validation");
        }
        else if((n>modele.get().getNoteRisqueFaible()) &&  (n<=modele.get().getNoteRisqueMoyennementFaible())){
            r.setStatut("Moyennement Faible");
            P.setNiveauRisque("Moyennement Faible");
            P.setRevue("Chaque  1 ans");
            P.setStatutPersonne("En attente de validation");
        }
        else if ((n>modele.get().getNoteRisqueMoyennementFaible()) &&  (n<=modele.get().getNoteRisqueMoyennementEleve()))
        {
            r.setStatut("Moyennement Elevé");
            P.setNiveauRisque("Moyennement Elevé");
            P.setRevue("Chaque  7 mois");
            P.setStatutPersonne("En attente de validation");
        }
        else
        {
            r.setStatut("Elevé");
            P.setNiveauRisque("Elevé");
            P.setRevue("Chaque  3 mois");
            P.setStatutPersonne("En attente de validation");
        }
        Optional<modeleRisquePersonneMorale> m =mservice.find(id);
        if(m.isPresent())
        {m.get().setValeur(n);
            mservice.update(m.get(),id);}
        r.setValeur(n);
        RisqueService.saveRisque(r);
        PService.updatePersonneMorale(id,P);
        return n;
    }

    @GetMapping("/Exists/{id}")
    public Boolean personneMoraleExists (@PathVariable Long id){
        return repository.existsByCodeClient(id);
    }

    /**
     * Calcul risque apres mise à jour
     */
    @PutMapping("/UpdateRisque/{id}")
    public int calculerRisque  (@PathVariable long id,@Validated @RequestBody  personneMorale P)
    {  int n=0 ;
        Optional<modeleCotationRisque> modele = modeleService.findModeleCotationRisque((long) 1);
        Optional<personneMorale> oldPersonneMorale=PService.findPersonneMorale(id);
        P.setBeneficiaireEffectifList(oldPersonneMorale.get().getBeneficiaireEffectifList());
        //detection statut fatca
        fatca f = new fatca();
        f.setCodeFatca(id);
        if((P.getGiin()==true )
                ||(P.getUs()==true)
                || (P.isCodeTin()==true)
                ||(P.getPaysConstruction().equalsIgnoreCase("Etat_Unis"))
                ||(P.getPaysResidence().equalsIgnoreCase("Etat_Unis"))||(P.getActionnaireUs()==true))
        {
            P.setStatutFatca(true);
            f.setStatut(true);
            fService.saveFatca(f);
        }
        else {
            P.setStatutFatca(false);
            fService.saveFatca(f);
        }
        //Calcul du risque
        n=RisqueService.risqueNature(P);
        n=n+RisqueService.risqueGeographique(id,P);
        risquePersonneMorale r=new risquePersonneMorale();
        r.setCodeRisque(id);
        if(n<=modele.get().getNoteRisqueFaible())
        {
            r.setStatut("Faible");
            P.setNiveauRisque("Faible");
        }
        else if((n>modele.get().getNoteRisqueFaible()) &&  (n<=modele.get().getNoteRisqueMoyennementFaible())){
            r.setStatut("Moyennement Faible");
            P.setNiveauRisque("Moyennement Faible");
        }
        else if ((n>modele.get().getNoteRisqueMoyennementFaible()) &&  (n<=modele.get().getNoteRisqueMoyennementEleve()))
        {
            r.setStatut("Moyennement Elevé");
            P.setNiveauRisque("Moyennement Elevé");
        }
        else
        {
            r.setStatut("Elevé");
            P.setNiveauRisque("Elevé");
        }
        Optional<modeleRisquePersonneMorale> m =mservice.find(id);
        if(m.isPresent())
        {m.get().setValeur(n);
            mservice.update(m.get(),id);}
        r.setValeur(n);
        RisqueService.saveRisque(r);
        PService.updatePersonneMorale(id,P);
        return n;
    }
    /**
     * Cette methode permet la suppression PersonneMorale par son id
     */
    @DeleteMapping(value="/Delete/{id}")
    public String deletePersonneMorale(@PathVariable String id)
    {
        ArrayList<Long> l =repService.listRepPersCode(Long.parseLong(id));
        for(int i=0;i<l.size();i++)
        {serviceRepresentant.removeRepresentantLegal(l.get(i));}
        ArrayList<Long> l1 =beService.listPersBeCode(Long.parseLong(id));
        for(int i=0;i<l1.size();i++)
        {serviceBeneficiaire.removeBeneficiaireEffectif(l1.get(i));}
        PService.removePersonneMorale (Long.parseLong(id));
        return "PersNat supprimee avec succee";
    }
    @GetMapping(value="/Fatca")
    public List<personneMorale> findPersonnes()
    {
        List<personneMorale> l =PService.listPersonneMorale();
        List<personneMorale> l1 = new ArrayList<>();
        for(int i=0;i<l.size();i++)
        {
            if ((l.get(i).getStatutFatca().equals(true))&&(l.get(i).getStatutPersonne().equalsIgnoreCase("Client")))
            {
                l1.add(l.get(i));
            }
        }
        return l1;
    }
    @GetMapping("/RisqueEleve")
    List<personneMorale> getClientsRisqueEleve()
    {List<personneMorale> l=PService.listPersonneMorale();
        List<personneMorale> p = new ArrayList<personneMorale>();
        for(int i=0;i<l.size();i++)
        { if((l.get(i).getNiveauRisque().equalsIgnoreCase("Elevé"))&&(l.get(i).getStatutPersonne().equalsIgnoreCase("Client")))
            p.add(l.get(i));
        }
        return p;
    }
    @GetMapping("/RisqueMoyennementEleve")
    List<personneMorale> getClientsRisqueMoyennementElevé()
    {List<personneMorale> l=PService.listPersonneMorale();
        List<personneMorale> p = new ArrayList<personneMorale>();
        for(int i=0;i<l.size();i++)
        { if((l.get(i).getNiveauRisque().equalsIgnoreCase("Moyennement Elevé"))&&(l.get(i).getStatutPersonne().equalsIgnoreCase("Client")))
            p.add(l.get(i));
        }
        return p;
    }
    @GetMapping("/RisqueFaible")
    List<personneMorale> getClientsRisqueFaible()
    {List<personneMorale> l=PService.listPersonneMorale();
        List<personneMorale> p = new ArrayList<personneMorale>();
        for(int i=0;i<l.size();i++)
        { if((l.get(i).getNiveauRisque().equalsIgnoreCase("Faible"))&&(l.get(i).getStatutPersonne().equalsIgnoreCase("Client")))
            p.add(l.get(i));
        }
        return p;
    }

    @GetMapping("/RisqueFaiblementEleve")
    List<personneMorale> getClientsRisqueFaiblementEleve()
    {List<personneMorale> l=PService.listPersonneMorale();
        List<personneMorale> p = new ArrayList<personneMorale>();
        for(int i=0;i<l.size();i++)
        { if((l.get(i).getNiveauRisque().equalsIgnoreCase("Moyennement Faible"))&&(l.get(i).getStatutPersonne().equalsIgnoreCase("Client")))
            p.add(l.get(i));
        }
        return p;
    }
    /**
     * affiche les comptes à traiter par le conseiller client
     */
    @GetMapping(value="/DossiersRisqueFaible")
    public List<personneMorale> getDossiersRisqueFaible ()
    {
        List<personneMorale> l=PService.listPersonneMorale();
        List<personneMorale> p = new ArrayList<personneMorale>();
        for(int i=0;i<l.size();i++)
        {if(l.get(i).getStatutPersonne()!=null)
        { if((l.get(i).getNiveauRisque().equalsIgnoreCase("Faible"))&& (l.get(i).getStatutPersonne().equalsIgnoreCase("En attente de validation")))
            p.add(l.get(i)); }
        }
        return p;
    }

    /**
     * affiche les comptes à traiter par le responsable Conformite
     */
    @GetMapping(value="/DossiersRisqueMoyennementFaibleouMoyennementEleve")
    public List<personneMorale> getDossiersRisqueMFME ()
    {
        List<personneMorale> l=PService.listPersonneMorale();
        List<personneMorale> p = new ArrayList<personneMorale>();
        for(int i=0;i<l.size();i++)
        {if(l.get(i).getStatutPersonne()!=null)
        { if(((l.get(i).getNiveauRisque().equalsIgnoreCase("Moyennement Elevé"))||(l.get(i).getNiveauRisque().equalsIgnoreCase("Moyennement Faible")))&&(l.get(i).getStatutPersonne().equalsIgnoreCase("En attente de validation")))
            p.add(l.get(i)); }
        }
        return p;
    }

    /**
     * affiche les comptes à traiter par le chef d'agence
     */
    @GetMapping(value="/DossiersRisqueEleve")
    public List<personneMorale> getDossiersRisqueE()
    {
        List<personneMorale> l=PService.listPersonneMorale();
        List<personneMorale> p = new ArrayList<personneMorale>();
        for(int i=0;i<l.size();i++)
        {if(l.get(i).getStatutPersonne()!=null)
        { if((l.get(i).getNiveauRisque().equalsIgnoreCase("Elevé"))&&(l.get(i).getStatutPersonne().equalsIgnoreCase("En attente de validation")))
            p.add(l.get(i)); }
        }
        return p;
    }
    /**
     * affiche les comptes à traiter par le chef d'agence
     */
    @GetMapping(value="/Dossiers")
    public List<personneMorale> getDossiers()
    {
        List<personneMorale> l=PService.listPersonneMorale();
        List<personneMorale> p = new ArrayList<personneMorale>();
        for(int i=0;i<l.size();i++)
        {if(l.get(i).getStatutPersonne()!=null)
        { if((l.get(i).getStatutPersonne().equalsIgnoreCase("En attente de validation")))
            p.add(l.get(i)); }
        }
        return p;
    }

}
