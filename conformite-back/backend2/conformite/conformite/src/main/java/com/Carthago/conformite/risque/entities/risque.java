package com.Carthago.conformite.risque.entities;

import com.Carthago.conformite.clients.entities.contact;
import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
public class risque {
    @Id
    private  Long codeRisque;
    private  String statut;
    private int valeur;


    //GETTERS AND SETTERS

    public Long getCodeRisque() {
        return codeRisque;
    }

    public void setCodeRisque(Long codeRisque) {
        this.codeRisque = codeRisque;
    }

    public String getStatut() {
        return statut;
    }

    public void setStatut(String statut) {
        this.statut = statut;
    }

    public int getValeur() {
        return valeur;
    }

    public void setValeur(int valeur) {
        this.valeur = valeur;
    }

    //Relations
    @OneToOne
    @JoinColumn(name = "codeClient2")
    @JsonBackReference
    private contact contactRisque;
}
