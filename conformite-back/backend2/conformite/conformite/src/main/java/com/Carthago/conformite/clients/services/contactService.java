package com.Carthago.conformite.clients.services;
import com.Carthago.conformite.clients.entities.beneficiaireEffectif;
import com.Carthago.conformite.clients.entities.contact;
import com.Carthago.conformite.clients.entities.personnePhysique;

import java.util.List;
import java.util.Optional;

public interface contactService {
    /**
     * lister les contacts
     * @return
     */
    List<contact> listClients();
    /**
     * Cette Methode permet de supprimer un contact.
     */
    void removeContact(long codeClient);


    Optional <contact> findPersonne(long codeClient);
}