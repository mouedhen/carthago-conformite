package com.Carthago.conformite.clients.controllers;


import com.Carthago.conformite.clients.entities.RepPers;
import com.Carthago.conformite.clients.entities.personneMorale;
import com.Carthago.conformite.clients.services.RepPersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value="/RepPers",method= {RequestMethod.GET})
public class RepPersController {

    @Autowired
    RepPersService RPService ;

    @GetMapping("/GetAll")
    public List<RepPers> getAllRepPers()
    {
        return RPService.listRepPers();

    }

    @GetMapping("/GetAllRepPers/{id}")
    public List<RepPers> getAllRepPersByCodeClient(@PathVariable Long id)
    {
        return RPService.listRepPersCodeClient(id);
    }

    @GetMapping("/GetCode/{id}")
    public ArrayList<Long> listRepPersCode(@PathVariable  Long id)
    {
        return RPService.listRepPersCode(id);
    }

    @GetMapping("/Get/{id}")
    public RepPers findByCodeClient(@PathVariable Long codeClient)
    {
        return RPService.findByCodeClient(codeClient);
    }

}
